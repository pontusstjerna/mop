#define SIMULATOR
 
#define GPIO_D 0x40020C00
#define GPIO_MODER (unsigned long*) GPIO_D
#define GPIO_IPR_LOW (unsigned char*) GPIO_D + 0x10
#define GPIO_IPR_HIGH (unsigned char*) GPIO_D + 0x11
#define GPIO_OPR_LOW (unsigned char*) GPIO_D + 0x14
#define GPIO_OPR_HIGH (unsigned char*) GPIO_D + 0x15

#define SysTick 0xE000E010
#define STK_CTRL (volatile unsigned long*) SysTick
#define STK_LOAD (volatile unsigned long*) SysTick + 0x4
#define STK_VAL (volatile unsigned long*) SysTick + 0x8
#define STK_CALIB (volatile unsigned long*) SysTick + 0xC
 
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
void delay_250ns(void);
void delay_mikro(unsigned int us);
void delay_milli(unsigned int ms);

void startup ( void )
{
asm volatile(
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void init_app(void){
	//Set all pins to outputs
	*(GPIO_MODER) = 0x55555555;
}

void cheapDelay(unsigned int delay){
	int i;
	for(i = 0; i < delay; i++);
}

void main(void)
{
	init_app();
	
	unsigned char bits = 0x1;
	while(1){
		*(GPIO_OPR_LOW) = bits;
		delay_milli(500);
		if(bits >= 0x80){
			bits = 0x01;
		}else{
			bits = bits<<1;
		}
	}
}

void delay_milli(unsigned int ms){
	#ifdef SIMULATOR
		ms = 5; //Some weird bug here with division
	#endif
	
	int i;
	for(i = 0; i < 1000; i++){
		delay_mikro(ms);
	}
}

void delay_mikro(unsigned int us){
	unsigned int i;
	
	for(i = us; i > 0; i--){
		delay_250ns();
		delay_250ns();
		delay_250ns();
		delay_250ns();
	}
}

void delay_250ns(void){
	*(STK_CTRL) = 0;
	*(STK_LOAD) = 168/4 - 1;
	*(STK_VAL) = 0;
	*(STK_CTRL) = 0x5; //ends with 0101
	while((*(STK_CTRL) & 0x10000) == 0); //wait for bit 16 to be 1
	*(STK_CTRL) = 0;
	
	//Kontrollregister:
	/*Bit 16: 1 om klar
	 * Bit 2: 1 efter RESET, använd systemklocka om 1 annars systemklocka/8
	 * Bit 1: Aktivera avbrott
	 * Bit 0: ENABLE
	 * DET ÄR DISPLAYEN SOM HAR ALL MAGI :o
	 */
}


