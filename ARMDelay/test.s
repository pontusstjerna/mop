   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"blinkey.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay/blinkey.c"
   1:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define SIMULATOR
   2:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c ****  
   3:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_D 0x40020C00
   4:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_MODER (unsigned long*) GPIO_D
   5:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_IPR_LOW (unsigned char*) GPIO_D + 0x10
   6:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_IPR_HIGH (unsigned char*) GPIO_D + 0x11
   7:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_OPR_LOW (unsigned char*) GPIO_D + 0x14
   8:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define GPIO_OPR_HIGH (unsigned char*) GPIO_D + 0x15
   9:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  10:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define SysTick 0xE000E010
  11:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define STK_CTRL (volatile unsigned long*) SysTick
  12:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define STK_LOAD (volatile unsigned long*) SysTick + 0x4
  13:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define STK_VAL (volatile unsigned long*) SysTick + 0x8
  14:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** #define STK_CALIB (volatile unsigned long*) SysTick + 0xC
  15:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c ****  
  16:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  17:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_250ns(void);
  18:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_mikro(unsigned int us);
  19:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_milli(unsigned int ms);
  20:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  21:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void startup ( void )
  22:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** {
  28              		.loc 1 22 0
  29              		.cfi_startproc
  23:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** asm volatile(
  30              		.loc 1 23 0
  31              		.syntax divided
  32              	@ 23 "C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay/blinkey.c" 1
  33 0000 0248     		 LDR R0,=0x2001C000
  34 0002 8546     	 MOV SP,R0
  35 0004 FFF7FEFF 	 BL main
  36 0008 FEE7     	.L1: B .L1
  37              	
  38              	@ 0 "" 2
  24:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  25:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	" MOV SP,R0\n"
  26:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	" BL main\n"				/* call main */
  27:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	".L1: B .L1\n"				/* never return */
  28:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	) ;
  29:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
  39              		.loc 1 29 0
  40              		.thumb
  41              		.syntax unified
  42 000a C046     		nop
  43              		.cfi_endproc
  44              	.LFE0:
  46              		.text
  47              		.align	2
  48              		.global	init_app
  49              		.code	16
  50              		.thumb_func
  52              	init_app:
  53              	.LFB1:
  30:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  31:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void init_app(void){
  54              		.loc 1 31 0
  55              		.cfi_startproc
  56 0000 80B5     		push	{r7, lr}
  57              		.cfi_def_cfa_offset 8
  58              		.cfi_offset 7, -8
  59              		.cfi_offset 14, -4
  60 0002 00AF     		add	r7, sp, #0
  61              		.cfi_def_cfa_register 7
  32:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	//Set all pins to outputs
  33:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(GPIO_MODER) = 0x55555555;
  62              		.loc 1 33 0
  63 0004 024B     		ldr	r3, .L3
  64 0006 034A     		ldr	r2, .L3+4
  65 0008 1A60     		str	r2, [r3]
  34:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
  66              		.loc 1 34 0
  67 000a C046     		nop
  68 000c BD46     		mov	sp, r7
  69              		@ sp needed
  70 000e 80BD     		pop	{r7, pc}
  71              	.L4:
  72              		.align	2
  73              	.L3:
  74 0010 000C0240 		.word	1073875968
  75 0014 55555555 		.word	1431655765
  76              		.cfi_endproc
  77              	.LFE1:
  79              		.align	2
  80              		.global	cheapDelay
  81              		.code	16
  82              		.thumb_func
  84              	cheapDelay:
  85              	.LFB2:
  35:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  36:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void cheapDelay(unsigned int delay){
  86              		.loc 1 36 0
  87              		.cfi_startproc
  88 0018 80B5     		push	{r7, lr}
  89              		.cfi_def_cfa_offset 8
  90              		.cfi_offset 7, -8
  91              		.cfi_offset 14, -4
  92 001a 84B0     		sub	sp, sp, #16
  93              		.cfi_def_cfa_offset 24
  94 001c 00AF     		add	r7, sp, #0
  95              		.cfi_def_cfa_register 7
  96 001e 7860     		str	r0, [r7, #4]
  37:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	int i;
  38:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	for(i = 0; i < delay; i++);
  97              		.loc 1 38 0
  98 0020 0023     		movs	r3, #0
  99 0022 FB60     		str	r3, [r7, #12]
 100 0024 02E0     		b	.L6
 101              	.L7:
 102              		.loc 1 38 0 is_stmt 0 discriminator 3
 103 0026 FB68     		ldr	r3, [r7, #12]
 104 0028 0133     		adds	r3, r3, #1
 105 002a FB60     		str	r3, [r7, #12]
 106              	.L6:
 107              		.loc 1 38 0 discriminator 1
 108 002c FA68     		ldr	r2, [r7, #12]
 109 002e 7B68     		ldr	r3, [r7, #4]
 110 0030 9A42     		cmp	r2, r3
 111 0032 F8D3     		bcc	.L7
  39:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
 112              		.loc 1 39 0 is_stmt 1
 113 0034 C046     		nop
 114 0036 BD46     		mov	sp, r7
 115 0038 04B0     		add	sp, sp, #16
 116              		@ sp needed
 117 003a 80BD     		pop	{r7, pc}
 118              		.cfi_endproc
 119              	.LFE2:
 121              		.align	2
 122              		.global	main
 123              		.code	16
 124              		.thumb_func
 126              	main:
 127              	.LFB3:
  40:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  41:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void main(void)
  42:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** {
 128              		.loc 1 42 0
 129              		.cfi_startproc
 130 003c 80B5     		push	{r7, lr}
 131              		.cfi_def_cfa_offset 8
 132              		.cfi_offset 7, -8
 133              		.cfi_offset 14, -4
 134 003e 82B0     		sub	sp, sp, #8
 135              		.cfi_def_cfa_offset 16
 136 0040 00AF     		add	r7, sp, #0
 137              		.cfi_def_cfa_register 7
  43:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	init_app();
 138              		.loc 1 43 0
 139 0042 FFF7FEFF 		bl	init_app
  44:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	
  45:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	unsigned char bits = 0x1;
 140              		.loc 1 45 0
 141 0046 FB1D     		adds	r3, r7, #7
 142 0048 0122     		movs	r2, #1
 143 004a 1A70     		strb	r2, [r3]
 144              	.L11:
  46:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	while(1){
  47:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		*(GPIO_OPR_LOW) = bits;
 145              		.loc 1 47 0
 146 004c 0B4A     		ldr	r2, .L12
 147 004e FB1D     		adds	r3, r7, #7
 148 0050 1B78     		ldrb	r3, [r3]
 149 0052 1370     		strb	r3, [r2]
  48:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_milli(500);
 150              		.loc 1 48 0
 151 0054 FA23     		movs	r3, #250
 152 0056 5B00     		lsls	r3, r3, #1
 153 0058 1800     		movs	r0, r3
 154 005a FFF7FEFF 		bl	delay_milli
  49:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		if(bits >= 0x80){
 155              		.loc 1 49 0
 156 005e FB1D     		adds	r3, r7, #7
 157 0060 1B78     		ldrb	r3, [r3]
 158 0062 5BB2     		sxtb	r3, r3
 159 0064 002B     		cmp	r3, #0
 160 0066 03DA     		bge	.L9
  50:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 			bits = 0x01;
 161              		.loc 1 50 0
 162 0068 FB1D     		adds	r3, r7, #7
 163 006a 0122     		movs	r2, #1
 164 006c 1A70     		strb	r2, [r3]
 165 006e EDE7     		b	.L11
 166              	.L9:
  51:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		}else{
  52:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 			bits = bits<<1;
 167              		.loc 1 52 0
 168 0070 FA1D     		adds	r2, r7, #7
 169 0072 FB1D     		adds	r3, r7, #7
 170 0074 1B78     		ldrb	r3, [r3]
 171 0076 DB18     		adds	r3, r3, r3
 172 0078 1370     		strb	r3, [r2]
  53:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		}
  54:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	}
 173              		.loc 1 54 0
 174 007a E7E7     		b	.L11
 175              	.L13:
 176              		.align	2
 177              	.L12:
 178 007c 140C0240 		.word	1073875988
 179              		.cfi_endproc
 180              	.LFE3:
 182              		.align	2
 183              		.global	delay_milli
 184              		.code	16
 185              		.thumb_func
 187              	delay_milli:
 188              	.LFB4:
  55:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
  56:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  57:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_milli(unsigned int ms){
 189              		.loc 1 57 0
 190              		.cfi_startproc
 191 0080 80B5     		push	{r7, lr}
 192              		.cfi_def_cfa_offset 8
 193              		.cfi_offset 7, -8
 194              		.cfi_offset 14, -4
 195 0082 84B0     		sub	sp, sp, #16
 196              		.cfi_def_cfa_offset 24
 197 0084 00AF     		add	r7, sp, #0
 198              		.cfi_def_cfa_register 7
 199 0086 7860     		str	r0, [r7, #4]
  58:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	#ifdef SIMULATOR
  59:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		ms = 5; //Some weird bug here with division
 200              		.loc 1 59 0
 201 0088 0523     		movs	r3, #5
 202 008a 7B60     		str	r3, [r7, #4]
  60:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	#endif
  61:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	
  62:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	int i;
  63:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	for(i = 0; i < 1000; i++){
 203              		.loc 1 63 0
 204 008c 0023     		movs	r3, #0
 205 008e FB60     		str	r3, [r7, #12]
 206 0090 06E0     		b	.L15
 207              	.L16:
  64:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_mikro(ms);
 208              		.loc 1 64 0 discriminator 3
 209 0092 7B68     		ldr	r3, [r7, #4]
 210 0094 1800     		movs	r0, r3
 211 0096 FFF7FEFF 		bl	delay_mikro
  63:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_mikro(ms);
 212              		.loc 1 63 0 discriminator 3
 213 009a FB68     		ldr	r3, [r7, #12]
 214 009c 0133     		adds	r3, r3, #1
 215 009e FB60     		str	r3, [r7, #12]
 216              	.L15:
  63:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_mikro(ms);
 217              		.loc 1 63 0 is_stmt 0 discriminator 1
 218 00a0 FB68     		ldr	r3, [r7, #12]
 219 00a2 034A     		ldr	r2, .L17
 220 00a4 9342     		cmp	r3, r2
 221 00a6 F4DD     		ble	.L16
  65:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	}
  66:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
 222              		.loc 1 66 0 is_stmt 1
 223 00a8 C046     		nop
 224 00aa BD46     		mov	sp, r7
 225 00ac 04B0     		add	sp, sp, #16
 226              		@ sp needed
 227 00ae 80BD     		pop	{r7, pc}
 228              	.L18:
 229              		.align	2
 230              	.L17:
 231 00b0 E7030000 		.word	999
 232              		.cfi_endproc
 233              	.LFE4:
 235              		.align	2
 236              		.global	delay_mikro
 237              		.code	16
 238              		.thumb_func
 240              	delay_mikro:
 241              	.LFB5:
  67:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  68:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_mikro(unsigned int us){
 242              		.loc 1 68 0
 243              		.cfi_startproc
 244 00b4 80B5     		push	{r7, lr}
 245              		.cfi_def_cfa_offset 8
 246              		.cfi_offset 7, -8
 247              		.cfi_offset 14, -4
 248 00b6 84B0     		sub	sp, sp, #16
 249              		.cfi_def_cfa_offset 24
 250 00b8 00AF     		add	r7, sp, #0
 251              		.cfi_def_cfa_register 7
 252 00ba 7860     		str	r0, [r7, #4]
  69:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	unsigned int i;
  70:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	
  71:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	for(i = us; i > 0; i--){
 253              		.loc 1 71 0
 254 00bc 7B68     		ldr	r3, [r7, #4]
 255 00be FB60     		str	r3, [r7, #12]
 256 00c0 0AE0     		b	.L20
 257              	.L21:
  72:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 258              		.loc 1 72 0 discriminator 3
 259 00c2 FFF7FEFF 		bl	delay_250ns
  73:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 260              		.loc 1 73 0 discriminator 3
 261 00c6 FFF7FEFF 		bl	delay_250ns
  74:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 262              		.loc 1 74 0 discriminator 3
 263 00ca FFF7FEFF 		bl	delay_250ns
  75:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 264              		.loc 1 75 0 discriminator 3
 265 00ce FFF7FEFF 		bl	delay_250ns
  71:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 266              		.loc 1 71 0 discriminator 3
 267 00d2 FB68     		ldr	r3, [r7, #12]
 268 00d4 013B     		subs	r3, r3, #1
 269 00d6 FB60     		str	r3, [r7, #12]
 270              	.L20:
  71:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 		delay_250ns();
 271              		.loc 1 71 0 is_stmt 0 discriminator 1
 272 00d8 FB68     		ldr	r3, [r7, #12]
 273 00da 002B     		cmp	r3, #0
 274 00dc F1D1     		bne	.L21
  76:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	}
  77:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
 275              		.loc 1 77 0 is_stmt 1
 276 00de C046     		nop
 277 00e0 BD46     		mov	sp, r7
 278 00e2 04B0     		add	sp, sp, #16
 279              		@ sp needed
 280 00e4 80BD     		pop	{r7, pc}
 281              		.cfi_endproc
 282              	.LFE5:
 284 00e6 C046     		.align	2
 285              		.global	delay_250ns
 286              		.code	16
 287              		.thumb_func
 289              	delay_250ns:
 290              	.LFB6:
  78:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 
  79:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** void delay_250ns(void){
 291              		.loc 1 79 0
 292              		.cfi_startproc
 293 00e8 80B5     		push	{r7, lr}
 294              		.cfi_def_cfa_offset 8
 295              		.cfi_offset 7, -8
 296              		.cfi_offset 14, -4
 297 00ea 00AF     		add	r7, sp, #0
 298              		.cfi_def_cfa_register 7
  80:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(STK_CTRL) = 0;
 299              		.loc 1 80 0
 300 00ec 0C4B     		ldr	r3, .L24
 301 00ee 0022     		movs	r2, #0
 302 00f0 1A60     		str	r2, [r3]
  81:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(STK_LOAD) = 168/4 - 1;
 303              		.loc 1 81 0
 304 00f2 0C4B     		ldr	r3, .L24+4
 305 00f4 2922     		movs	r2, #41
 306 00f6 1A60     		str	r2, [r3]
  82:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(STK_VAL) = 0;
 307              		.loc 1 82 0
 308 00f8 0B4B     		ldr	r3, .L24+8
 309 00fa 0022     		movs	r2, #0
 310 00fc 1A60     		str	r2, [r3]
  83:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(STK_CTRL) = 0x5; //ends with 0101
 311              		.loc 1 83 0
 312 00fe 084B     		ldr	r3, .L24
 313 0100 0522     		movs	r2, #5
 314 0102 1A60     		str	r2, [r3]
  84:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	while((*(STK_CTRL) & 0x10000) == 0); //wait for bit 16 to be 1
 315              		.loc 1 84 0
 316 0104 C046     		nop
 317              	.L23:
 318              		.loc 1 84 0 is_stmt 0 discriminator 1
 319 0106 064B     		ldr	r3, .L24
 320 0108 1A68     		ldr	r2, [r3]
 321 010a 8023     		movs	r3, #128
 322 010c 5B02     		lsls	r3, r3, #9
 323 010e 1340     		ands	r3, r2
 324 0110 F9D0     		beq	.L23
  85:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	*(STK_CTRL) = 0;
 325              		.loc 1 85 0 is_stmt 1
 326 0112 034B     		ldr	r3, .L24
 327 0114 0022     		movs	r2, #0
 328 0116 1A60     		str	r2, [r3]
  86:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	
  87:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	//Kontrollregister:
  88:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	/*Bit 16: 1 om klar
  89:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	 * Bit 2: 1 efter RESET, använd systemklocka om 1 annars systemklocka/8
  90:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	 * Bit 1: Aktivera avbrott
  91:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	 * Bit 0: ENABLE
  92:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	 * DET ÄR DISPLAYEN SOM HAR ALL MAGI :o
  93:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** 	 */
  94:C:/Users/Pontus/OneDrive/Programmering/C/MOP/ARMDelay\blinkey.c **** }
 329              		.loc 1 94 0
 330 0118 C046     		nop
 331 011a BD46     		mov	sp, r7
 332              		@ sp needed
 333 011c 80BD     		pop	{r7, pc}
 334              	.L25:
 335 011e C046     		.align	2
 336              	.L24:
 337 0120 10E000E0 		.word	-536813552
 338 0124 20E000E0 		.word	-536813536
 339 0128 30E000E0 		.word	-536813520
 340              		.cfi_endproc
 341              	.LFE6:
 343              	.Letext0:
