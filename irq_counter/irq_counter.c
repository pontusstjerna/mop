#define SYSCFG_MEMRMP (unsigned volatile long*) 0x40013800
#define SYSCFG_PMC (unsigned volatile long*) 0x40013804
#define SYSCFG_EXTICR1 (unsigned volatile long*) 0x40013808
#define SYSCFG_EXTICR2 (unsigned volatile long*) 0x40013810
#define SYSCFG_EXTICR3 (unsigned volatile long*) 0x40013812
#define SYSCFG_EXTICR4 (unsigned volatile long*) 0x40013814

#define EXTI_IMR (unsigned volatile long*) 0x040013C00
#define EXTI_EMR (unsigned volatile long*) 0x040013C04
#define EXTI_RTSR (unsigned volatile long*) 0x040013C08
#define EXTI_FTSR (unsigned volatile long*) 0x040013C0C
#define EXTI_SWIER (unsigned volatile long*) 0x040013C10
#define EXTI_PR (unsigned volatile long*) 0x040013C14

typedef unsigned volatile char vbyte;

//Port D
#define GPIO_MODER (unsigned volatile long*) 0x40020C00
#define GPIO_OTYPER (unsigned volatile short*) 0x40020C04
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40020C08
#define GPIO_PUPDR (unsigned volatile long*) 0x40020C0C
#define GPIO_IDR_LOW (vbyte*) 0x40020C10
#define GPIO_IDR_HIGH (vbyte*) 0x40020C11
#define GPIO_ODR_LOW (vbyte*) 0x40020C14
#define GPIO_ODR_HIGH (vbyte*) 0x40020C15

#define NVIC_ISER0 (unsigned volatile long*) 0xE000E100
#define RCC_APB2ENR (unsigned int *) 0x40023844

char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};

void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void startup ( void )
{
asm volatile(
	" LDR R0, =0x40023830\n"
	" MOV R1, #0x18\n"
	" STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void application_init(void);
void disable_interrupt(void);
void irq_clock_handler(void);
void irq0_handler(void);
void irq1_handler(void);
void out7seg(unsigned char);

int count = 0;

void main(void)
{
	application_init();
	while(1)
	{
		//*(GPIO_ODR_LOW) = count;
		out7seg(count + 48);
	}
}

void application_init()
{
	*(GPIO_MODER) = 0x00005555;
	*((unsigned long *)0x40023844) |= 0x4000;
	
	/**(SYSCFG_EXTICR1) &= ~0x0FFF;
	*(SYSCFG_EXTICR1) |= 0x4000;
	* 
	*/
	
	*(SYSCFG_EXTICR1) &= ~0x0FFF;
	*(SYSCFG_EXTICR1) |= 0x0444;
	*(EXTI_IMR) |= 0x7; //01011
	*(EXTI_FTSR) |= 0x7;
	
	*((void (**) (void)) 0x2001C058) = irq0_handler;
	*((void (**) (void)) 0x2001C05C) = irq1_handler;
	*((void (**) (void)) 0x2001C060) = irq_clock_handler;
	//*(NVIC_ISER0) |= (1<<9);
	*(NVIC_ISER0) |= (7 << 6);
	//0000 0111 0000
 
}



void irq0_handler(void)
{
	if((*(EXTI_PR) & 1))
	{
		count++;
		*(EXTI_PR) |= 0x1;
	}
}

void irq1_handler(void)
{
	if(*(EXTI_PR) & 2)
	{
		count = 0;
		*(EXTI_PR) |= 0x2;
	}
	
}

void irq_clock_handler(void)
{
	if(*(EXTI_PR) & 4)
	{
		if(count != 3)
		{
			count = 3;
		}
		else{
			count = 10;
		}
		*(EXTI_PR) |= 4;
	}
}

void out7seg(unsigned char c){
	unsigned char bits;
	
	/*
	 *  _0_
	 *5|_6|1
	 *4|__|2
	 *  3 .7
	 */
	
	switch(c){
		case 0xFF: bits = 0x00; break;
		case '1': bits = 0x06; break;
		case '2': bits = 0x5B; break;
		case '3': bits = 0x4F; break;
		case '4': bits = 0x66; break;
		case '5': bits = 0x6D; break;
		case '6': bits = 0x7D; break;
		case '7': bits = 0x07; break;
		case '8': bits = 0x7F; break;
		case '9': bits = 0x6F; break;
		case '0': bits = 0x3F; break;
		case 'A': bits = 0x77; break;
		case 'B': bits = 0x7C; break;
		case 'C': bits = 0x39; break;
		case 'D': bits = 0x5E; break;
		case '*': bits = 0x49; break;
		default: bits = 0x79; break;
		
		//
	}
	
	*(GPIO_ODR_LOW) = bits;
}