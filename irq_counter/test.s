   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"irq_counter.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.global	keyValues
  20              		.data
  21              		.align	2
  24              	keyValues:
  25 0000 31       		.byte	49
  26 0001 32       		.byte	50
  27 0002 33       		.byte	51
  28 0003 41       		.byte	65
  29 0004 34       		.byte	52
  30 0005 35       		.byte	53
  31 0006 36       		.byte	54
  32 0007 42       		.byte	66
  33 0008 37       		.byte	55
  34 0009 38       		.byte	56
  35 000a 39       		.byte	57
  36 000b 43       		.byte	67
  37 000c 2A       		.byte	42
  38 000d 30       		.byte	48
  39 000e 23       		.byte	35
  40 000f 44       		.byte	68
  41              		.section	.start_section,"ax",%progbits
  42              		.align	2
  43              		.global	startup
  44              		.code	16
  45              		.thumb_func
  47              	startup:
  48              	.LFB0:
  49              		.file 1 "C:/Users/alebabu/Documents/mop/irq_counter/irq_counter.c"
   1:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_MEMRMP (unsigned volatile long*) 0x40013800
   2:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_PMC (unsigned volatile long*) 0x40013804
   3:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_EXTICR1 (unsigned volatile long*) 0x40013808
   4:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_EXTICR2 (unsigned volatile long*) 0x40013810
   5:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_EXTICR3 (unsigned volatile long*) 0x40013812
   6:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define SYSCFG_EXTICR4 (unsigned volatile long*) 0x40013814
   7:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
   8:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_IMR (unsigned volatile long*) 0x040013C00
   9:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_EMR (unsigned volatile long*) 0x040013C04
  10:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_RTSR (unsigned volatile long*) 0x040013C08
  11:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_FTSR (unsigned volatile long*) 0x040013C0C
  12:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_SWIER (unsigned volatile long*) 0x040013C10
  13:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define EXTI_PR (unsigned volatile long*) 0x040013C14
  14:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  15:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** typedef unsigned volatile char vbyte;
  16:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  17:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_MODER (unsigned volatile long*) 0x40020C00
  18:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_OTYPER (unsigned volatile short*) 0x40020C04
  19:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_OSPEEDR (unsigned volatile long*) 0x40020C08
  20:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_PUPDR (unsigned volatile long*) 0x40020C0C
  21:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_IDR_LOW (vbyte*) 0x40020C10
  22:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_IDR_HIGH (vbyte*) 0x40020C11
  23:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_ODR_LOW (vbyte*) 0x40020C14
  24:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define GPIO_ODR_HIGH (vbyte*) 0x40020C15
  25:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  26:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define NVIC_ISER0 (unsigned volatile long*) 0xE000E100
  27:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** #define RCC_APB2ENR (unsigned int *) 0x40023844
  28:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  29:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};
  30:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  31:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  32:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  33:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void startup ( void )
  34:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
  50              		.loc 1 34 0
  51              		.cfi_startproc
  35:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** asm volatile(
  52              		.loc 1 35 0
  53              		.syntax divided
  54              	@ 35 "C:/Users/alebabu/Documents/mop/irq_counter/irq_counter.c" 1
  55 0000 0448     		 LDR R0, =0x40023830
  56 0002 1821     	 MOV R1, #0x18
  57 0004 0160     	 STR R1, [R0]
  58 0006 0448     	 LDR R0,=0x2001C000
  59 0008 8546     	 MOV SP,R0
  60 000a FFF7FEFF 	 BL main
  61 000e FEE7     	.L1: B .L1
  62              	
  63              	@ 0 "" 2
  36:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" LDR R0, =0x40023830\n"
  37:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" MOV R1, #0x18\n"
  38:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" STR R1, [R0]\n"
  39:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  40:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" MOV SP,R0\n"
  41:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	" BL main\n"				/* call main */
  42:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	".L1: B .L1\n"				/* never return */
  43:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	) ;
  44:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
  64              		.loc 1 44 0
  65              		.thumb
  66              		.syntax unified
  67 0010 C046     		nop
  68              		.cfi_endproc
  69              	.LFE0:
  71              		.global	count
  72 0012 0000     		.bss
  73              		.align	2
  76              	count:
  77 0000 00000000 		.space	4
  78              		.text
  79              		.align	2
  80              		.global	main
  81              		.code	16
  82              		.thumb_func
  84              	main:
  85              	.LFB1:
  45:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  46:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void application_init(void);
  47:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void disable_interrupt(void);
  48:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq_clock_handler(void);
  49:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq0_handler(void);
  50:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq1_handler(void);
  51:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void out7seg(unsigned char);
  52:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  53:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** int count = 0;
  54:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  55:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void main(void)
  56:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
  86              		.loc 1 56 0
  87              		.cfi_startproc
  88 0000 80B5     		push	{r7, lr}
  89              		.cfi_def_cfa_offset 8
  90              		.cfi_offset 7, -8
  91              		.cfi_offset 14, -4
  92 0002 00AF     		add	r7, sp, #0
  93              		.cfi_def_cfa_register 7
  57:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	application_init();
  94              		.loc 1 57 0
  95 0004 FFF7FEFF 		bl	application_init
  96              	.L3:
  58:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	while(1)
  59:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	{
  60:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		//*(GPIO_ODR_LOW) = count;
  61:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		out7seg(count + 48);
  97              		.loc 1 61 0 discriminator 1
  98 0008 044B     		ldr	r3, .L4
  99 000a 1B68     		ldr	r3, [r3]
 100 000c DBB2     		uxtb	r3, r3
 101 000e 3033     		adds	r3, r3, #48
 102 0010 DBB2     		uxtb	r3, r3
 103 0012 1800     		movs	r0, r3
 104 0014 FFF7FEFF 		bl	out7seg
  62:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	}
 105              		.loc 1 62 0 discriminator 1
 106 0018 F6E7     		b	.L3
 107              	.L5:
 108 001a C046     		.align	2
 109              	.L4:
 110 001c 00000000 		.word	count
 111              		.cfi_endproc
 112              	.LFE1:
 114              		.align	2
 115              		.global	application_init
 116              		.code	16
 117              		.thumb_func
 119              	application_init:
 120              	.LFB2:
  63:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
  64:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  65:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void application_init()
  66:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
 121              		.loc 1 66 0
 122              		.cfi_startproc
 123 0020 80B5     		push	{r7, lr}
 124              		.cfi_def_cfa_offset 8
 125              		.cfi_offset 7, -8
 126              		.cfi_offset 14, -4
 127 0022 00AF     		add	r7, sp, #0
 128              		.cfi_def_cfa_register 7
  67:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(GPIO_MODER) = 0x00005555;
 129              		.loc 1 67 0
 130 0024 1A4B     		ldr	r3, .L7
 131 0026 1B4A     		ldr	r2, .L7+4
 132 0028 1A60     		str	r2, [r3]
  68:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*((unsigned long *)0x40023844) |= 0x4000;
 133              		.loc 1 68 0
 134 002a 1B4B     		ldr	r3, .L7+8
 135 002c 1A4A     		ldr	r2, .L7+8
 136 002e 1268     		ldr	r2, [r2]
 137 0030 8021     		movs	r1, #128
 138 0032 C901     		lsls	r1, r1, #7
 139 0034 0A43     		orrs	r2, r1
 140 0036 1A60     		str	r2, [r3]
  69:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
  70:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	/**(SYSCFG_EXTICR1) &= ~0x0FFF;
  71:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(SYSCFG_EXTICR1) |= 0x4000;
  72:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	* 
  73:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*/
  74:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
  75:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(SYSCFG_EXTICR1) &= ~0x0FFF;
 141              		.loc 1 75 0
 142 0038 184B     		ldr	r3, .L7+12
 143 003a 184A     		ldr	r2, .L7+12
 144 003c 1268     		ldr	r2, [r2]
 145 003e 120B     		lsrs	r2, r2, #12
 146 0040 1203     		lsls	r2, r2, #12
 147 0042 1A60     		str	r2, [r3]
  76:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(SYSCFG_EXTICR1) |= 0x0444;
 148              		.loc 1 76 0
 149 0044 154B     		ldr	r3, .L7+12
 150 0046 154A     		ldr	r2, .L7+12
 151 0048 1268     		ldr	r2, [r2]
 152 004a 1549     		ldr	r1, .L7+16
 153 004c 0A43     		orrs	r2, r1
 154 004e 1A60     		str	r2, [r3]
  77:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(EXTI_IMR) |= 0x7; //01011
 155              		.loc 1 77 0
 156 0050 144B     		ldr	r3, .L7+20
 157 0052 144A     		ldr	r2, .L7+20
 158 0054 1268     		ldr	r2, [r2]
 159 0056 0721     		movs	r1, #7
 160 0058 0A43     		orrs	r2, r1
 161 005a 1A60     		str	r2, [r3]
  78:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(EXTI_FTSR) |= 0x7;
 162              		.loc 1 78 0
 163 005c 124B     		ldr	r3, .L7+24
 164 005e 124A     		ldr	r2, .L7+24
 165 0060 1268     		ldr	r2, [r2]
 166 0062 0721     		movs	r1, #7
 167 0064 0A43     		orrs	r2, r1
 168 0066 1A60     		str	r2, [r3]
  79:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
  80:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*((void (**) (void)) 0x2001C058) = irq0_handler;
 169              		.loc 1 80 0
 170 0068 104B     		ldr	r3, .L7+28
 171 006a 114A     		ldr	r2, .L7+32
 172 006c 1A60     		str	r2, [r3]
  81:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*((void (**) (void)) 0x2001C05C) = irq1_handler;
 173              		.loc 1 81 0
 174 006e 114B     		ldr	r3, .L7+36
 175 0070 114A     		ldr	r2, .L7+40
 176 0072 1A60     		str	r2, [r3]
  82:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*((void (**) (void)) 0x2001C060) = irq_clock_handler;
 177              		.loc 1 82 0
 178 0074 114B     		ldr	r3, .L7+44
 179 0076 124A     		ldr	r2, .L7+48
 180 0078 1A60     		str	r2, [r3]
  83:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	//*(NVIC_ISER0) |= (1<<9);
  84:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(NVIC_ISER0) |= (7 << 6);
 181              		.loc 1 84 0
 182 007a 124B     		ldr	r3, .L7+52
 183 007c 114A     		ldr	r2, .L7+52
 184 007e 1268     		ldr	r2, [r2]
 185 0080 E021     		movs	r1, #224
 186 0082 4900     		lsls	r1, r1, #1
 187 0084 0A43     		orrs	r2, r1
 188 0086 1A60     		str	r2, [r3]
  85:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	//0000 0111 0000
  86:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c ****  
  87:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
 189              		.loc 1 87 0
 190 0088 C046     		nop
 191 008a BD46     		mov	sp, r7
 192              		@ sp needed
 193 008c 80BD     		pop	{r7, pc}
 194              	.L8:
 195 008e C046     		.align	2
 196              	.L7:
 197 0090 000C0240 		.word	1073875968
 198 0094 55550000 		.word	21845
 199 0098 44380240 		.word	1073887300
 200 009c 08380140 		.word	1073821704
 201 00a0 44040000 		.word	1092
 202 00a4 003C0140 		.word	1073822720
 203 00a8 0C3C0140 		.word	1073822732
 204 00ac 58C00120 		.word	536985688
 205 00b0 00000000 		.word	irq0_handler
 206 00b4 5CC00120 		.word	536985692
 207 00b8 00000000 		.word	irq1_handler
 208 00bc 60C00120 		.word	536985696
 209 00c0 00000000 		.word	irq_clock_handler
 210 00c4 00E100E0 		.word	-536813312
 211              		.cfi_endproc
 212              	.LFE2:
 214              		.align	2
 215              		.global	irq0_handler
 216              		.code	16
 217              		.thumb_func
 219              	irq0_handler:
 220              	.LFB3:
  88:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  89:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  90:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
  91:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq0_handler(void)
  92:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
 221              		.loc 1 92 0
 222              		.cfi_startproc
 223 00c8 80B5     		push	{r7, lr}
 224              		.cfi_def_cfa_offset 8
 225              		.cfi_offset 7, -8
 226              		.cfi_offset 14, -4
 227 00ca 00AF     		add	r7, sp, #0
 228              		.cfi_def_cfa_register 7
  93:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	if((*(EXTI_PR) & 1))
 229              		.loc 1 93 0
 230 00cc 094B     		ldr	r3, .L12
 231 00ce 1B68     		ldr	r3, [r3]
 232 00d0 0122     		movs	r2, #1
 233 00d2 1340     		ands	r3, r2
 234 00d4 0AD0     		beq	.L11
  94:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	{
  95:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		count++;
 235              		.loc 1 95 0
 236 00d6 084B     		ldr	r3, .L12+4
 237 00d8 1B68     		ldr	r3, [r3]
 238 00da 5A1C     		adds	r2, r3, #1
 239 00dc 064B     		ldr	r3, .L12+4
 240 00de 1A60     		str	r2, [r3]
  96:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		*(EXTI_PR) |= 0x1;
 241              		.loc 1 96 0
 242 00e0 044B     		ldr	r3, .L12
 243 00e2 044A     		ldr	r2, .L12
 244 00e4 1268     		ldr	r2, [r2]
 245 00e6 0121     		movs	r1, #1
 246 00e8 0A43     		orrs	r2, r1
 247 00ea 1A60     		str	r2, [r3]
 248              	.L11:
  97:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	}
  98:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
 249              		.loc 1 98 0
 250 00ec C046     		nop
 251 00ee BD46     		mov	sp, r7
 252              		@ sp needed
 253 00f0 80BD     		pop	{r7, pc}
 254              	.L13:
 255 00f2 C046     		.align	2
 256              	.L12:
 257 00f4 143C0140 		.word	1073822740
 258 00f8 00000000 		.word	count
 259              		.cfi_endproc
 260              	.LFE3:
 262              		.align	2
 263              		.global	irq1_handler
 264              		.code	16
 265              		.thumb_func
 267              	irq1_handler:
 268              	.LFB4:
  99:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
 100:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq1_handler(void)
 101:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
 269              		.loc 1 101 0
 270              		.cfi_startproc
 271 00fc 80B5     		push	{r7, lr}
 272              		.cfi_def_cfa_offset 8
 273              		.cfi_offset 7, -8
 274              		.cfi_offset 14, -4
 275 00fe 00AF     		add	r7, sp, #0
 276              		.cfi_def_cfa_register 7
 102:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	if(*(EXTI_PR) & 2)
 277              		.loc 1 102 0
 278 0100 084B     		ldr	r3, .L17
 279 0102 1B68     		ldr	r3, [r3]
 280 0104 0222     		movs	r2, #2
 281 0106 1340     		ands	r3, r2
 282 0108 08D0     		beq	.L16
 103:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	{
 104:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		count = 0;
 283              		.loc 1 104 0
 284 010a 074B     		ldr	r3, .L17+4
 285 010c 0022     		movs	r2, #0
 286 010e 1A60     		str	r2, [r3]
 105:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		*(EXTI_PR) |= 0x2;
 287              		.loc 1 105 0
 288 0110 044B     		ldr	r3, .L17
 289 0112 044A     		ldr	r2, .L17
 290 0114 1268     		ldr	r2, [r2]
 291 0116 0221     		movs	r1, #2
 292 0118 0A43     		orrs	r2, r1
 293 011a 1A60     		str	r2, [r3]
 294              	.L16:
 106:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	}
 107:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
 295              		.loc 1 107 0
 296 011c C046     		nop
 297 011e BD46     		mov	sp, r7
 298              		@ sp needed
 299 0120 80BD     		pop	{r7, pc}
 300              	.L18:
 301 0122 C046     		.align	2
 302              	.L17:
 303 0124 143C0140 		.word	1073822740
 304 0128 00000000 		.word	count
 305              		.cfi_endproc
 306              	.LFE4:
 308              		.align	2
 309              		.global	irq_clock_handler
 310              		.code	16
 311              		.thumb_func
 313              	irq_clock_handler:
 314              	.LFB5:
 108:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
 109:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void irq_clock_handler(void)
 110:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** {
 315              		.loc 1 110 0
 316              		.cfi_startproc
 317 012c 80B5     		push	{r7, lr}
 318              		.cfi_def_cfa_offset 8
 319              		.cfi_offset 7, -8
 320              		.cfi_offset 14, -4
 321 012e 00AF     		add	r7, sp, #0
 322              		.cfi_def_cfa_register 7
 111:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	if(*(EXTI_PR) & 4)
 323              		.loc 1 111 0
 324 0130 0C4B     		ldr	r3, .L24
 325 0132 1B68     		ldr	r3, [r3]
 326 0134 0422     		movs	r2, #4
 327 0136 1340     		ands	r3, r2
 328 0138 10D0     		beq	.L23
 112:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	{
 113:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		if(count != 3)
 329              		.loc 1 113 0
 330 013a 0B4B     		ldr	r3, .L24+4
 331 013c 1B68     		ldr	r3, [r3]
 332 013e 032B     		cmp	r3, #3
 333 0140 03D0     		beq	.L21
 114:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		{
 115:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 			count = 3;
 334              		.loc 1 115 0
 335 0142 094B     		ldr	r3, .L24+4
 336 0144 0322     		movs	r2, #3
 337 0146 1A60     		str	r2, [r3]
 338 0148 02E0     		b	.L22
 339              	.L21:
 116:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		}
 117:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		else{
 118:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 			count = 10;
 340              		.loc 1 118 0
 341 014a 074B     		ldr	r3, .L24+4
 342 014c 0A22     		movs	r2, #10
 343 014e 1A60     		str	r2, [r3]
 344              	.L22:
 119:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		}
 120:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		*(EXTI_PR) |= 4;
 345              		.loc 1 120 0
 346 0150 044B     		ldr	r3, .L24
 347 0152 044A     		ldr	r2, .L24
 348 0154 1268     		ldr	r2, [r2]
 349 0156 0421     		movs	r1, #4
 350 0158 0A43     		orrs	r2, r1
 351 015a 1A60     		str	r2, [r3]
 352              	.L23:
 121:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	}
 122:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }
 353              		.loc 1 122 0
 354 015c C046     		nop
 355 015e BD46     		mov	sp, r7
 356              		@ sp needed
 357 0160 80BD     		pop	{r7, pc}
 358              	.L25:
 359 0162 C046     		.align	2
 360              	.L24:
 361 0164 143C0140 		.word	1073822740
 362 0168 00000000 		.word	count
 363              		.cfi_endproc
 364              	.LFE5:
 366              		.align	2
 367              		.global	out7seg
 368              		.code	16
 369              		.thumb_func
 371              	out7seg:
 372              	.LFB6:
 123:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 
 124:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** void out7seg(unsigned char c){
 373              		.loc 1 124 0
 374              		.cfi_startproc
 375 016c 80B5     		push	{r7, lr}
 376              		.cfi_def_cfa_offset 8
 377              		.cfi_offset 7, -8
 378              		.cfi_offset 14, -4
 379 016e 84B0     		sub	sp, sp, #16
 380              		.cfi_def_cfa_offset 24
 381 0170 00AF     		add	r7, sp, #0
 382              		.cfi_def_cfa_register 7
 383 0172 0200     		movs	r2, r0
 384 0174 FB1D     		adds	r3, r7, #7
 385 0176 1A70     		strb	r2, [r3]
 125:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	unsigned char bits;
 126:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
 127:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	/*
 128:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	 *  _0_
 129:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	 *5|_6|1
 130:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	 *4|__|2
 131:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	 *  3 .7
 132:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	 */
 133:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
 134:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	switch(c){
 386              		.loc 1 134 0
 387 0178 FB1D     		adds	r3, r7, #7
 388 017a 1B78     		ldrb	r3, [r3]
 389 017c 362B     		cmp	r3, #54
 390 017e 3CD0     		beq	.L28
 391 0180 0DDC     		bgt	.L29
 392 0182 322B     		cmp	r3, #50
 393 0184 25D0     		beq	.L30
 394 0186 06DC     		bgt	.L31
 395 0188 302B     		cmp	r3, #48
 396 018a 4AD0     		beq	.L32
 397 018c 1CDC     		bgt	.L33
 398 018e 2A2B     		cmp	r3, #42
 399 0190 00D1     		bne	.LCB301
 400 0192 5FE0     		b	.L34	@long jump
 401              	.LCB301:
 402 0194 63E0     		b	.L27
 403              	.L31:
 404 0196 342B     		cmp	r3, #52
 405 0198 25D0     		beq	.L35
 406 019a 29DC     		bgt	.L36
 407 019c 1EE0     		b	.L48
 408              	.L29:
 409 019e 412B     		cmp	r3, #65
 410 01a0 44D0     		beq	.L38
 411 01a2 05DC     		bgt	.L39
 412 01a4 382B     		cmp	r3, #56
 413 01a6 32D0     		beq	.L40
 414 01a8 2CDB     		blt	.L41
 415 01aa 392B     		cmp	r3, #57
 416 01ac 34D0     		beq	.L42
 417 01ae 56E0     		b	.L27
 418              	.L39:
 419 01b0 432B     		cmp	r3, #67
 420 01b2 45D0     		beq	.L43
 421 01b4 3FDB     		blt	.L44
 422 01b6 442B     		cmp	r3, #68
 423 01b8 47D0     		beq	.L45
 424 01ba FF2B     		cmp	r3, #255
 425 01bc 4FD1     		bne	.L27
 135:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case 0xFF: bits = 0x00; break;
 426              		.loc 1 135 0
 427 01be 0F23     		movs	r3, #15
 428 01c0 FB18     		adds	r3, r7, r3
 429 01c2 0022     		movs	r2, #0
 430 01c4 1A70     		strb	r2, [r3]
 431 01c6 4FE0     		b	.L47
 432              	.L33:
 136:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '1': bits = 0x06; break;
 433              		.loc 1 136 0
 434 01c8 0F23     		movs	r3, #15
 435 01ca FB18     		adds	r3, r7, r3
 436 01cc 0622     		movs	r2, #6
 437 01ce 1A70     		strb	r2, [r3]
 438 01d0 4AE0     		b	.L47
 439              	.L30:
 137:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '2': bits = 0x5B; break;
 440              		.loc 1 137 0
 441 01d2 0F23     		movs	r3, #15
 442 01d4 FB18     		adds	r3, r7, r3
 443 01d6 5B22     		movs	r2, #91
 444 01d8 1A70     		strb	r2, [r3]
 445 01da 45E0     		b	.L47
 446              	.L48:
 138:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '3': bits = 0x4F; break;
 447              		.loc 1 138 0
 448 01dc 0F23     		movs	r3, #15
 449 01de FB18     		adds	r3, r7, r3
 450 01e0 4F22     		movs	r2, #79
 451 01e2 1A70     		strb	r2, [r3]
 452 01e4 40E0     		b	.L47
 453              	.L35:
 139:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '4': bits = 0x66; break;
 454              		.loc 1 139 0
 455 01e6 0F23     		movs	r3, #15
 456 01e8 FB18     		adds	r3, r7, r3
 457 01ea 6622     		movs	r2, #102
 458 01ec 1A70     		strb	r2, [r3]
 459 01ee 3BE0     		b	.L47
 460              	.L36:
 140:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '5': bits = 0x6D; break;
 461              		.loc 1 140 0
 462 01f0 0F23     		movs	r3, #15
 463 01f2 FB18     		adds	r3, r7, r3
 464 01f4 6D22     		movs	r2, #109
 465 01f6 1A70     		strb	r2, [r3]
 466 01f8 36E0     		b	.L47
 467              	.L28:
 141:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '6': bits = 0x7D; break;
 468              		.loc 1 141 0
 469 01fa 0F23     		movs	r3, #15
 470 01fc FB18     		adds	r3, r7, r3
 471 01fe 7D22     		movs	r2, #125
 472 0200 1A70     		strb	r2, [r3]
 473 0202 31E0     		b	.L47
 474              	.L41:
 142:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '7': bits = 0x07; break;
 475              		.loc 1 142 0
 476 0204 0F23     		movs	r3, #15
 477 0206 FB18     		adds	r3, r7, r3
 478 0208 0722     		movs	r2, #7
 479 020a 1A70     		strb	r2, [r3]
 480 020c 2CE0     		b	.L47
 481              	.L40:
 143:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '8': bits = 0x7F; break;
 482              		.loc 1 143 0
 483 020e 0F23     		movs	r3, #15
 484 0210 FB18     		adds	r3, r7, r3
 485 0212 7F22     		movs	r2, #127
 486 0214 1A70     		strb	r2, [r3]
 487 0216 27E0     		b	.L47
 488              	.L42:
 144:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '9': bits = 0x6F; break;
 489              		.loc 1 144 0
 490 0218 0F23     		movs	r3, #15
 491 021a FB18     		adds	r3, r7, r3
 492 021c 6F22     		movs	r2, #111
 493 021e 1A70     		strb	r2, [r3]
 494 0220 22E0     		b	.L47
 495              	.L32:
 145:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '0': bits = 0x3F; break;
 496              		.loc 1 145 0
 497 0222 0F23     		movs	r3, #15
 498 0224 FB18     		adds	r3, r7, r3
 499 0226 3F22     		movs	r2, #63
 500 0228 1A70     		strb	r2, [r3]
 501 022a 1DE0     		b	.L47
 502              	.L38:
 146:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case 'A': bits = 0x77; break;
 503              		.loc 1 146 0
 504 022c 0F23     		movs	r3, #15
 505 022e FB18     		adds	r3, r7, r3
 506 0230 7722     		movs	r2, #119
 507 0232 1A70     		strb	r2, [r3]
 508 0234 18E0     		b	.L47
 509              	.L44:
 147:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case 'B': bits = 0x7C; break;
 510              		.loc 1 147 0
 511 0236 0F23     		movs	r3, #15
 512 0238 FB18     		adds	r3, r7, r3
 513 023a 7C22     		movs	r2, #124
 514 023c 1A70     		strb	r2, [r3]
 515 023e 13E0     		b	.L47
 516              	.L43:
 148:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case 'C': bits = 0x39; break;
 517              		.loc 1 148 0
 518 0240 0F23     		movs	r3, #15
 519 0242 FB18     		adds	r3, r7, r3
 520 0244 3922     		movs	r2, #57
 521 0246 1A70     		strb	r2, [r3]
 522 0248 0EE0     		b	.L47
 523              	.L45:
 149:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case 'D': bits = 0x5E; break;
 524              		.loc 1 149 0
 525 024a 0F23     		movs	r3, #15
 526 024c FB18     		adds	r3, r7, r3
 527 024e 5E22     		movs	r2, #94
 528 0250 1A70     		strb	r2, [r3]
 529 0252 09E0     		b	.L47
 530              	.L34:
 150:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		case '*': bits = 0x49; break;
 531              		.loc 1 150 0
 532 0254 0F23     		movs	r3, #15
 533 0256 FB18     		adds	r3, r7, r3
 534 0258 4922     		movs	r2, #73
 535 025a 1A70     		strb	r2, [r3]
 536 025c 04E0     		b	.L47
 537              	.L27:
 151:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		default: bits = 0x79; break;
 538              		.loc 1 151 0
 539 025e 0F23     		movs	r3, #15
 540 0260 FB18     		adds	r3, r7, r3
 541 0262 7922     		movs	r2, #121
 542 0264 1A70     		strb	r2, [r3]
 543 0266 C046     		nop
 544              	.L47:
 152:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		
 153:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 		//
 154:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	}
 155:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	
 156:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** 	*(GPIO_ODR_LOW) = bits;
 545              		.loc 1 156 0
 546 0268 044A     		ldr	r2, .L49
 547 026a 0F23     		movs	r3, #15
 548 026c FB18     		adds	r3, r7, r3
 549 026e 1B78     		ldrb	r3, [r3]
 550 0270 1370     		strb	r3, [r2]
 157:C:/Users/alebabu/Documents/mop/irq_counter\irq_counter.c **** }...
 551              		.loc 1 157 0
 552 0272 C046     		nop
 553 0274 BD46     		mov	sp, r7
 554 0276 04B0     		add	sp, sp, #16
 555              		@ sp needed
 556 0278 80BD     		pop	{r7, pc}
 557              	.L50:
 558 027a C046     		.align	2
 559              	.L49:
 560 027c 140C0240 		.word	1073875988
 561              		.cfi_endproc
 562              	.LFE6:
 564              	.Letext0:
