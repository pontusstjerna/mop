   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"asciidisplay.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc6
   1:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
   2:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
   3:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_MODER (unsigned volatile long*) 0x40021000
   4:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
   5:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
   6:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
   7:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_IDR_LOW (unsigned volatile char*) 0x40021010
   8:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_IDR_HIGH (unsigned volatile char*) 0x40021011
   9:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_ODR_LOW (unsigned volatile char*) 0x40021014
  10:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define GPIO_ODR_HIGH (unsigned volatile char*) 0x40021015
  11:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  12:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  13:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define B_E 0x40
  14:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define B_SELECT 0x4
  15:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define B_RW 0x2
  16:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define B_RS 0x1
  17:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  18:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define STK_CTRL (unsigned volatile long*) 0xE000E010
  19:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define STK_LOAD (unsigned volatile long*) 0xE000E014
  20:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define STK_VAL (unsigned volatile long*) 0xE000E018
  21:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** #define STK_CALIB (unsigned volatile long*) 0xE000E01C
  22:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  23:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  24:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  25:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_250ns(void);
  26:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_mikro(unsigned int us);
  27:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_milli(unsigned int ms);
  28:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_ctrl_bit_set(unsigned char x);
  29:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_ctrl_bit_clear(unsigned char x);
  30:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_controller(unsigned char);
  31:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_controller(void);
  32:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_cmd(unsigned char command);
  33:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_data(unsigned char data);
  34:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_status(void);
  35:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_data(void);
  36:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_command(unsigned char command, unsigned int delayUs);
  37:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_gotoPos(int x, int y);
  38:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_char(unsigned char c);
  39:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  40:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void startup ( void )
  41:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** {
  28              		.loc 1 41 0
  29              		.cfi_startproc
  42:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** asm volatile(
  30              		.loc 1 42 0
  31              		.syntax divided
  32              	@ 42 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b9
  33 0000 0448     		LDR R0, =0x40023830
  34 0002 1821     	MOV R1, #0x18
  35 0004 0160     	STR R1, [R0]
  36 0006 0448     	 LDR R0,=0x2001C000
  37 0008 8546     	 MOV SP,R0
  38 000a FFF7FEFF 	 BL main
  39 000e FEE7     	.L1: B .L1
  40              	
  41              	@ 0 "" 2
  43:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	"LDR R0, =0x40023830\n"
  44:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	"MOV R1, #0x18\n"
  45:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	"STR R1, [R0]\n"
  46:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  47:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	" MOV SP,R0\n"
  48:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	" BL main\n"				/* call main */
  49:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	".L1: B .L1\n"				/* never return */
  50:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	) ;
  51:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
  42              		.loc 1 51 0
  43              		.thumb
  44              		.syntax unified
  45 0010 C046     		nop
  46              		.cfi_endproc
  47              	.LFE0:
  49 0012 0000     		.text
  50              		.align	2
  51              		.global	init_app
  52              		.code	16
  53              		.thumb_func
  55              	init_app:
  56              	.LFB1:
  52:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  53:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void init_app(void){
  57              		.loc 1 53 0
  58              		.cfi_startproc
  59 0000 80B5     		push	{r7, lr}
  60              		.cfi_def_cfa_offset 8
  61              		.cfi_offset 7, -8
  62              		.cfi_offset 14, -4
  63 0002 00AF     		add	r7, sp, #0
  64              		.cfi_def_cfa_register 7
  54:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Set all pins to outputs
  55:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_MODER) = 0x55555555;
  65              		.loc 1 55 0
  66 0004 024B     		ldr	r3, .L3
  67 0006 034A     		ldr	r2, .L3+4
  68 0008 1A60     		str	r2, [r3]
  56:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
  69              		.loc 1 56 0
  70 000a C046     		nop
  71 000c BD46     		mov	sp, r7
  72              		@ sp needed
  73 000e 80BD     		pop	{r7, pc}
  74              	.L4:
  75              		.align	2
  76              	.L3:
  77 0010 00100240 		.word	1073876992
  78 0014 55555555 		.word	1431655765
  79              		.cfi_endproc
  80              	.LFE1:
  82              		.align	2
  83              		.global	ascii_init
  84              		.code	16
  85              		.thumb_func
  87              	ascii_init:
  88              	.LFB2:
  57:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  58:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_init(void){
  89              		.loc 1 58 0
  90              		.cfi_startproc
  91 0018 80B5     		push	{r7, lr}
  92              		.cfi_def_cfa_offset 8
  93              		.cfi_offset 7, -8
  94              		.cfi_offset 14, -4
  95 001a 00AF     		add	r7, sp, #0
  96              		.cfi_def_cfa_register 7
  59:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Set rows and size
  60:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_command(0x38, 39);
  97              		.loc 1 60 0
  98 001c 2721     		movs	r1, #39
  99 001e 3820     		movs	r0, #56
 100 0020 FFF7FEFF 		bl	ascii_command
  61:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  62:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Start display, start and set marker constant
  63:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_command(0x0E, 39);
 101              		.loc 1 63 0
 102 0024 2721     		movs	r1, #39
 103 0026 0E20     		movs	r0, #14
 104 0028 FFF7FEFF 		bl	ascii_command
  64:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  65:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Clear display
  66:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_command(0x01, 1530);
 105              		.loc 1 66 0
 106 002c 054B     		ldr	r3, .L6
 107 002e 1900     		movs	r1, r3
 108 0030 0120     		movs	r0, #1
 109 0032 FFF7FEFF 		bl	ascii_command
  67:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  68:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Set increment and no shift
  69:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_command(0x03, 39);
 110              		.loc 1 69 0
 111 0036 2721     		movs	r1, #39
 112 0038 0320     		movs	r0, #3
 113 003a FFF7FEFF 		bl	ascii_command
  70:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 114              		.loc 1 70 0
 115 003e C046     		nop
 116 0040 BD46     		mov	sp, r7
 117              		@ sp needed
 118 0042 80BD     		pop	{r7, pc}
 119              	.L7:
 120              		.align	2
 121              	.L6:
 122 0044 FA050000 		.word	1530
 123              		.cfi_endproc
 124              	.LFE2:
 126              		.section	.rodata
 127              		.align	2
 128              	.LC0:
 129 0000 49206E65 		.ascii	"I need some\000"
 129      65642073 
 129      6F6D6500 
 130              		.align	2
 131              	.LC2:
 132 000c 676F6464 		.ascii	"goddamn tea!\000"
 132      616D6E20 
 132      74656121 
 132      00
 133              		.text
 134              		.align	2
 135              		.global	main
 136              		.code	16
 137              		.thumb_func
 139              	main:
 140              	.LFB3:
  71:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  72:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** int main(void)
  73:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** {
 141              		.loc 1 73 0
 142              		.cfi_startproc
 143 0048 90B5     		push	{r4, r7, lr}
 144              		.cfi_def_cfa_offset 12
 145              		.cfi_offset 4, -12
 146              		.cfi_offset 7, -8
 147              		.cfi_offset 14, -4
 148 004a 89B0     		sub	sp, sp, #36
 149              		.cfi_def_cfa_offset 48
 150 004c 00AF     		add	r7, sp, #0
 151              		.cfi_def_cfa_register 7
  74:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	char* s;
  75:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	char test1[] = "I need some";
 152              		.loc 1 75 0
 153 004e 1023     		movs	r3, #16
 154 0050 FB18     		adds	r3, r7, r3
 155 0052 1B4A     		ldr	r2, .L14
 156 0054 13CA     		ldmia	r2!, {r0, r1, r4}
 157 0056 13C3     		stmia	r3!, {r0, r1, r4}
  76:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	char test2[] = "goddamn tea!";
 158              		.loc 1 76 0
 159 0058 3B00     		movs	r3, r7
 160 005a 1A4A     		ldr	r2, .L14+4
 161 005c 13CA     		ldmia	r2!, {r0, r1, r4}
 162 005e 13C3     		stmia	r3!, {r0, r1, r4}
 163 0060 1278     		ldrb	r2, [r2]
 164 0062 1A70     		strb	r2, [r3]
  77:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  78:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	init_app();
 165              		.loc 1 78 0
 166 0064 FFF7FEFF 		bl	init_app
  79:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_init();
 167              		.loc 1 79 0
 168 0068 FFF7FEFF 		bl	ascii_init
  80:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_gotoPos(1,1);
 169              		.loc 1 80 0
 170 006c 0121     		movs	r1, #1
 171 006e 0120     		movs	r0, #1
 172 0070 FFF7FEFF 		bl	ascii_gotoPos
  81:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	s = test1;
 173              		.loc 1 81 0
 174 0074 1023     		movs	r3, #16
 175 0076 FB18     		adds	r3, r7, r3
 176 0078 FB61     		str	r3, [r7, #28]
  82:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  83:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//ascii_write_char('a');
  84:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while(*s){
 177              		.loc 1 84 0
 178 007a 06E0     		b	.L9
 179              	.L10:
  85:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		ascii_write_char(*s++);
 180              		.loc 1 85 0
 181 007c FB69     		ldr	r3, [r7, #28]
 182 007e 5A1C     		adds	r2, r3, #1
 183 0080 FA61     		str	r2, [r7, #28]
 184 0082 1B78     		ldrb	r3, [r3]
 185 0084 1800     		movs	r0, r3
 186 0086 FFF7FEFF 		bl	ascii_write_char
 187              	.L9:
  84:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		ascii_write_char(*s++);
 188              		.loc 1 84 0
 189 008a FB69     		ldr	r3, [r7, #28]
 190 008c 1B78     		ldrb	r3, [r3]
 191 008e 002B     		cmp	r3, #0
 192 0090 F4D1     		bne	.L10
  86:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
  87:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_gotoPos(1,2);
 193              		.loc 1 87 0
 194 0092 0221     		movs	r1, #2
 195 0094 0120     		movs	r0, #1
 196 0096 FFF7FEFF 		bl	ascii_gotoPos
  88:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	s = test2;
 197              		.loc 1 88 0
 198 009a 3B00     		movs	r3, r7
 199 009c FB61     		str	r3, [r7, #28]
  89:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while(*s){
 200              		.loc 1 89 0
 201 009e 06E0     		b	.L11
 202              	.L12:
  90:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		ascii_write_char(*s++);
 203              		.loc 1 90 0
 204 00a0 FB69     		ldr	r3, [r7, #28]
 205 00a2 5A1C     		adds	r2, r3, #1
 206 00a4 FA61     		str	r2, [r7, #28]
 207 00a6 1B78     		ldrb	r3, [r3]
 208 00a8 1800     		movs	r0, r3
 209 00aa FFF7FEFF 		bl	ascii_write_char
 210              	.L11:
  89:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while(*s){
 211              		.loc 1 89 0
 212 00ae FB69     		ldr	r3, [r7, #28]
 213 00b0 1B78     		ldrb	r3, [r3]
 214 00b2 002B     		cmp	r3, #0
 215 00b4 F4D1     		bne	.L12
  91:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
  92:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
  93:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	return 0;
 216              		.loc 1 93 0
 217 00b6 0023     		movs	r3, #0
  94:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 218              		.loc 1 94 0
 219 00b8 1800     		movs	r0, r3
 220 00ba BD46     		mov	sp, r7
 221 00bc 09B0     		add	sp, sp, #36
 222              		@ sp needed
 223 00be 90BD     		pop	{r4, r7, pc}
 224              	.L15:
 225              		.align	2
 226              	.L14:
 227 00c0 00000000 		.word	.LC0
 228 00c4 0C000000 		.word	.LC2
 229              		.cfi_endproc
 230              	.LFE3:
 232              		.align	2
 233              		.global	ascii_ctrl_bit_set
 234              		.code	16
 235              		.thumb_func
 237              	ascii_ctrl_bit_set:
 238              	.LFB4:
  95:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
  96:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** //Set set bits in x to 1
  97:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_ctrl_bit_set(unsigned char x){ 
 239              		.loc 1 97 0
 240              		.cfi_startproc
 241 00c8 80B5     		push	{r7, lr}
 242              		.cfi_def_cfa_offset 8
 243              		.cfi_offset 7, -8
 244              		.cfi_offset 14, -4
 245 00ca 84B0     		sub	sp, sp, #16
 246              		.cfi_def_cfa_offset 24
 247 00cc 00AF     		add	r7, sp, #0
 248              		.cfi_def_cfa_register 7
 249 00ce 0200     		movs	r2, r0
 250 00d0 FB1D     		adds	r3, r7, #7
 251 00d2 1A70     		strb	r2, [r3]
  98:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char c = *(GPIO_ODR_LOW);
 252              		.loc 1 98 0
 253 00d4 0C4A     		ldr	r2, .L17
 254 00d6 0F23     		movs	r3, #15
 255 00d8 FB18     		adds	r3, r7, r3
 256 00da 1278     		ldrb	r2, [r2]
 257 00dc 1A70     		strb	r2, [r3]
  99:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	c = c | (B_SELECT | x);
 258              		.loc 1 99 0
 259 00de FA1D     		adds	r2, r7, #7
 260 00e0 0F23     		movs	r3, #15
 261 00e2 FB18     		adds	r3, r7, r3
 262 00e4 1278     		ldrb	r2, [r2]
 263 00e6 1B78     		ldrb	r3, [r3]
 264 00e8 1343     		orrs	r3, r2
 265 00ea DAB2     		uxtb	r2, r3
 266 00ec 0F23     		movs	r3, #15
 267 00ee FB18     		adds	r3, r7, r3
 268 00f0 0421     		movs	r1, #4
 269 00f2 0A43     		orrs	r2, r1
 270 00f4 1A70     		strb	r2, [r3]
 100:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_ODR_LOW) = c;
 271              		.loc 1 100 0
 272 00f6 044A     		ldr	r2, .L17
 273 00f8 0F23     		movs	r3, #15
 274 00fa FB18     		adds	r3, r7, r3
 275 00fc 1B78     		ldrb	r3, [r3]
 276 00fe 1370     		strb	r3, [r2]
 101:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 277              		.loc 1 101 0
 278 0100 C046     		nop
 279 0102 BD46     		mov	sp, r7
 280 0104 04B0     		add	sp, sp, #16
 281              		@ sp needed
 282 0106 80BD     		pop	{r7, pc}
 283              	.L18:
 284              		.align	2
 285              	.L17:
 286 0108 14100240 		.word	1073877012
 287              		.cfi_endproc
 288              	.LFE4:
 290              		.align	2
 291              		.global	ascii_ctrl_bit_clear
 292              		.code	16
 293              		.thumb_func
 295              	ascii_ctrl_bit_clear:
 296              	.LFB5:
 102:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 103:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** //Set set bits in x to 0
 104:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_ctrl_bit_clear(unsigned char x){
 297              		.loc 1 104 0
 298              		.cfi_startproc
 299 010c 80B5     		push	{r7, lr}
 300              		.cfi_def_cfa_offset 8
 301              		.cfi_offset 7, -8
 302              		.cfi_offset 14, -4
 303 010e 84B0     		sub	sp, sp, #16
 304              		.cfi_def_cfa_offset 24
 305 0110 00AF     		add	r7, sp, #0
 306              		.cfi_def_cfa_register 7
 307 0112 0200     		movs	r2, r0
 308 0114 FB1D     		adds	r3, r7, #7
 309 0116 1A70     		strb	r2, [r3]
 105:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char c;
 106:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	c = *(GPIO_ODR_LOW);
 310              		.loc 1 106 0
 311 0118 0F4A     		ldr	r2, .L20
 312 011a 0F23     		movs	r3, #15
 313 011c FB18     		adds	r3, r7, r3
 314 011e 1278     		ldrb	r2, [r2]
 315 0120 1A70     		strb	r2, [r3]
 107:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	c = B_SELECT | (c & ~x);
 316              		.loc 1 107 0
 317 0122 FB1D     		adds	r3, r7, #7
 318 0124 1B78     		ldrb	r3, [r3]
 319 0126 DB43     		mvns	r3, r3
 320 0128 D9B2     		uxtb	r1, r3
 321 012a 0F23     		movs	r3, #15
 322 012c FB18     		adds	r3, r7, r3
 323 012e 1A78     		ldrb	r2, [r3]
 324 0130 0B1C     		adds	r3, r1, #0
 325 0132 1340     		ands	r3, r2
 326 0134 DBB2     		uxtb	r3, r3
 327 0136 1A1C     		adds	r2, r3, #0
 328 0138 0423     		movs	r3, #4
 329 013a 1343     		orrs	r3, r2
 330 013c DAB2     		uxtb	r2, r3
 331 013e 0F23     		movs	r3, #15
 332 0140 FB18     		adds	r3, r7, r3
 333 0142 1A70     		strb	r2, [r3]
 108:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_ODR_LOW) = c;
 334              		.loc 1 108 0
 335 0144 044A     		ldr	r2, .L20
 336 0146 0F23     		movs	r3, #15
 337 0148 FB18     		adds	r3, r7, r3
 338 014a 1B78     		ldrb	r3, [r3]
 339 014c 1370     		strb	r3, [r2]
 109:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 340              		.loc 1 109 0
 341 014e C046     		nop
 342 0150 BD46     		mov	sp, r7
 343 0152 04B0     		add	sp, sp, #16
 344              		@ sp needed
 345 0154 80BD     		pop	{r7, pc}
 346              	.L21:
 347 0156 C046     		.align	2
 348              	.L20:
 349 0158 14100240 		.word	1073877012
 350              		.cfi_endproc
 351              	.LFE5:
 353              		.align	2
 354              		.global	ascii_write_controller
 355              		.code	16
 356              		.thumb_func
 358              	ascii_write_controller:
 359              	.LFB6:
 110:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 111:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_controller(unsigned char byte){
 360              		.loc 1 111 0
 361              		.cfi_startproc
 362 015c 80B5     		push	{r7, lr}
 363              		.cfi_def_cfa_offset 8
 364              		.cfi_offset 7, -8
 365              		.cfi_offset 14, -4
 366 015e 82B0     		sub	sp, sp, #8
 367              		.cfi_def_cfa_offset 16
 368 0160 00AF     		add	r7, sp, #0
 369              		.cfi_def_cfa_register 7
 370 0162 0200     		movs	r2, r0
 371 0164 FB1D     		adds	r3, r7, #7
 372 0166 1A70     		strb	r2, [r3]
 112:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_E);
 373              		.loc 1 112 0
 374 0168 4020     		movs	r0, #64
 375 016a FFF7FEFF 		bl	ascii_ctrl_bit_set
 113:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_ODR_HIGH) = byte;
 376              		.loc 1 113 0
 377 016e 064A     		ldr	r2, .L23
 378 0170 FB1D     		adds	r3, r7, #7
 379 0172 1B78     		ldrb	r3, [r3]
 380 0174 1370     		strb	r3, [r2]
 114:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_250ns();
 381              		.loc 1 114 0
 382 0176 FFF7FEFF 		bl	delay_250ns
 115:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_E);
 383              		.loc 1 115 0
 384 017a 4020     		movs	r0, #64
 385 017c FFF7FEFF 		bl	ascii_ctrl_bit_clear
 116:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 386              		.loc 1 116 0
 387 0180 C046     		nop
 388 0182 BD46     		mov	sp, r7
 389 0184 02B0     		add	sp, sp, #8
 390              		@ sp needed
 391 0186 80BD     		pop	{r7, pc}
 392              	.L24:
 393              		.align	2
 394              	.L23:
 395 0188 15100240 		.word	1073877013
 396              		.cfi_endproc
 397              	.LFE6:
 399              		.align	2
 400              		.global	ascii_read_controller
 401              		.code	16
 402              		.thumb_func
 404              	ascii_read_controller:
 405              	.LFB7:
 117:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 118:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_controller(void){
 406              		.loc 1 118 0
 407              		.cfi_startproc
 408 018c 80B5     		push	{r7, lr}
 409              		.cfi_def_cfa_offset 8
 410              		.cfi_offset 7, -8
 411              		.cfi_offset 14, -4
 412 018e 82B0     		sub	sp, sp, #8
 413              		.cfi_def_cfa_offset 16
 414 0190 00AF     		add	r7, sp, #0
 415              		.cfi_def_cfa_register 7
 119:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_E);
 416              		.loc 1 119 0
 417 0192 4020     		movs	r0, #64
 418 0194 FFF7FEFF 		bl	ascii_ctrl_bit_set
 120:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_250ns();
 419              		.loc 1 120 0
 420 0198 FFF7FEFF 		bl	delay_250ns
 121:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_250ns();
 421              		.loc 1 121 0
 422 019c FFF7FEFF 		bl	delay_250ns
 122:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char byte = *(GPIO_IDR_HIGH);
 423              		.loc 1 122 0
 424 01a0 064A     		ldr	r2, .L27
 425 01a2 FB1D     		adds	r3, r7, #7
 426 01a4 1278     		ldrb	r2, [r2]
 427 01a6 1A70     		strb	r2, [r3]
 123:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_E);
 428              		.loc 1 123 0
 429 01a8 4020     		movs	r0, #64
 430 01aa FFF7FEFF 		bl	ascii_ctrl_bit_clear
 124:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	return byte;
 431              		.loc 1 124 0
 432 01ae FB1D     		adds	r3, r7, #7
 433 01b0 1B78     		ldrb	r3, [r3]
 125:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 434              		.loc 1 125 0
 435 01b2 1800     		movs	r0, r3
 436 01b4 BD46     		mov	sp, r7
 437 01b6 02B0     		add	sp, sp, #8
 438              		@ sp needed
 439 01b8 80BD     		pop	{r7, pc}
 440              	.L28:
 441 01ba C046     		.align	2
 442              	.L27:
 443 01bc 11100240 		.word	1073877009
 444              		.cfi_endproc
 445              	.LFE7:
 447              		.align	2
 448              		.global	ascii_write_cmd
 449              		.code	16
 450              		.thumb_func
 452              	ascii_write_cmd:
 453              	.LFB8:
 126:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 127:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_cmd(unsigned char command){
 454              		.loc 1 127 0
 455              		.cfi_startproc
 456 01c0 80B5     		push	{r7, lr}
 457              		.cfi_def_cfa_offset 8
 458              		.cfi_offset 7, -8
 459              		.cfi_offset 14, -4
 460 01c2 82B0     		sub	sp, sp, #8
 461              		.cfi_def_cfa_offset 16
 462 01c4 00AF     		add	r7, sp, #0
 463              		.cfi_def_cfa_register 7
 464 01c6 0200     		movs	r2, r0
 465 01c8 FB1D     		adds	r3, r7, #7
 466 01ca 1A70     		strb	r2, [r3]
 128:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_RS);
 467              		.loc 1 128 0
 468 01cc 0120     		movs	r0, #1
 469 01ce FFF7FEFF 		bl	ascii_ctrl_bit_clear
 129:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_RW);
 470              		.loc 1 129 0
 471 01d2 0220     		movs	r0, #2
 472 01d4 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 130:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_write_controller(command);
 473              		.loc 1 130 0
 474 01d8 FB1D     		adds	r3, r7, #7
 475 01da 1B78     		ldrb	r3, [r3]
 476 01dc 1800     		movs	r0, r3
 477 01de FFF7FEFF 		bl	ascii_write_controller
 131:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 478              		.loc 1 131 0
 479 01e2 C046     		nop
 480 01e4 BD46     		mov	sp, r7
 481 01e6 02B0     		add	sp, sp, #8
 482              		@ sp needed
 483 01e8 80BD     		pop	{r7, pc}
 484              		.cfi_endproc
 485              	.LFE8:
 487 01ea C046     		.align	2
 488              		.global	ascii_write_data
 489              		.code	16
 490              		.thumb_func
 492              	ascii_write_data:
 493              	.LFB9:
 132:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 133:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_data(unsigned char data){
 494              		.loc 1 133 0
 495              		.cfi_startproc
 496 01ec 80B5     		push	{r7, lr}
 497              		.cfi_def_cfa_offset 8
 498              		.cfi_offset 7, -8
 499              		.cfi_offset 14, -4
 500 01ee 82B0     		sub	sp, sp, #8
 501              		.cfi_def_cfa_offset 16
 502 01f0 00AF     		add	r7, sp, #0
 503              		.cfi_def_cfa_register 7
 504 01f2 0200     		movs	r2, r0
 505 01f4 FB1D     		adds	r3, r7, #7
 506 01f6 1A70     		strb	r2, [r3]
 134:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_RS);
 507              		.loc 1 134 0
 508 01f8 0120     		movs	r0, #1
 509 01fa FFF7FEFF 		bl	ascii_ctrl_bit_set
 135:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_RW);
 510              		.loc 1 135 0
 511 01fe 0220     		movs	r0, #2
 512 0200 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 136:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_write_controller(data);
 513              		.loc 1 136 0
 514 0204 FB1D     		adds	r3, r7, #7
 515 0206 1B78     		ldrb	r3, [r3]
 516 0208 1800     		movs	r0, r3
 517 020a FFF7FEFF 		bl	ascii_write_controller
 137:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 518              		.loc 1 137 0
 519 020e C046     		nop
 520 0210 BD46     		mov	sp, r7
 521 0212 02B0     		add	sp, sp, #8
 522              		@ sp needed
 523 0214 80BD     		pop	{r7, pc}
 524              		.cfi_endproc
 525              	.LFE9:
 527 0216 C046     		.align	2
 528              		.global	ascii_read_status
 529              		.code	16
 530              		.thumb_func
 532              	ascii_read_status:
 533              	.LFB10:
 138:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 139:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_status(void){
 534              		.loc 1 139 0
 535              		.cfi_startproc
 536 0218 90B5     		push	{r4, r7, lr}
 537              		.cfi_def_cfa_offset 12
 538              		.cfi_offset 4, -12
 539              		.cfi_offset 7, -8
 540              		.cfi_offset 14, -4
 541 021a 83B0     		sub	sp, sp, #12
 542              		.cfi_def_cfa_offset 24
 543 021c 00AF     		add	r7, sp, #0
 544              		.cfi_def_cfa_register 7
 140:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_MODER) = 0x00005555;
 545              		.loc 1 140 0
 546 021e 0D4B     		ldr	r3, .L33
 547 0220 0D4A     		ldr	r2, .L33+4
 548 0222 1A60     		str	r2, [r3]
 141:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_clear(B_RS);
 549              		.loc 1 141 0
 550 0224 0120     		movs	r0, #1
 551 0226 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 142:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_RW);
 552              		.loc 1 142 0
 553 022a 0220     		movs	r0, #2
 554 022c FFF7FEFF 		bl	ascii_ctrl_bit_set
 143:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char status = ascii_read_controller();
 555              		.loc 1 143 0
 556 0230 FC1D     		adds	r4, r7, #7
 557 0232 FFF7FEFF 		bl	ascii_read_controller
 558 0236 0300     		movs	r3, r0
 559 0238 2370     		strb	r3, [r4]
 144:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_MODER) = 0x55555555;
 560              		.loc 1 144 0
 561 023a 064B     		ldr	r3, .L33
 562 023c 074A     		ldr	r2, .L33+8
 563 023e 1A60     		str	r2, [r3]
 145:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_ODR_HIGH) = status;
 564              		.loc 1 145 0
 565 0240 074A     		ldr	r2, .L33+12
 566 0242 FB1D     		adds	r3, r7, #7
 567 0244 1B78     		ldrb	r3, [r3]
 568 0246 1370     		strb	r3, [r2]
 146:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	return status;
 569              		.loc 1 146 0
 570 0248 FB1D     		adds	r3, r7, #7
 571 024a 1B78     		ldrb	r3, [r3]
 147:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 572              		.loc 1 147 0
 573 024c 1800     		movs	r0, r3
 574 024e BD46     		mov	sp, r7
 575 0250 03B0     		add	sp, sp, #12
 576              		@ sp needed
 577 0252 90BD     		pop	{r4, r7, pc}
 578              	.L34:
 579              		.align	2
 580              	.L33:
 581 0254 00100240 		.word	1073876992
 582 0258 55550000 		.word	21845
 583 025c 55555555 		.word	1431655765
 584 0260 15100240 		.word	1073877013
 585              		.cfi_endproc
 586              	.LFE10:
 588              		.align	2
 589              		.global	ascii_read_data
 590              		.code	16
 591              		.thumb_func
 593              	ascii_read_data:
 594              	.LFB11:
 148:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 149:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** unsigned char ascii_read_data(void){
 595              		.loc 1 149 0
 596              		.cfi_startproc
 597 0264 90B5     		push	{r4, r7, lr}
 598              		.cfi_def_cfa_offset 12
 599              		.cfi_offset 4, -12
 600              		.cfi_offset 7, -8
 601              		.cfi_offset 14, -4
 602 0266 83B0     		sub	sp, sp, #12
 603              		.cfi_def_cfa_offset 24
 604 0268 00AF     		add	r7, sp, #0
 605              		.cfi_def_cfa_register 7
 150:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned int reg = *(GPIO_MODER);
 606              		.loc 1 150 0
 607 026a 0F4B     		ldr	r3, .L37
 608 026c 1B68     		ldr	r3, [r3]
 609 026e 7B60     		str	r3, [r7, #4]
 151:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_MODER) = 0x0000FFFF & reg;
 610              		.loc 1 151 0
 611 0270 0D4B     		ldr	r3, .L37
 612 0272 7A68     		ldr	r2, [r7, #4]
 613 0274 1204     		lsls	r2, r2, #16
 614 0276 120C     		lsrs	r2, r2, #16
 615 0278 1A60     		str	r2, [r3]
 152:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_RS);
 616              		.loc 1 152 0
 617 027a 0120     		movs	r0, #1
 618 027c FFF7FEFF 		bl	ascii_ctrl_bit_set
 153:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_ctrl_bit_set(B_RW);
 619              		.loc 1 153 0
 620 0280 0220     		movs	r0, #2
 621 0282 FFF7FEFF 		bl	ascii_ctrl_bit_set
 154:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char status = ascii_read_controller();
 622              		.loc 1 154 0
 623 0286 FC1C     		adds	r4, r7, #3
 624 0288 FFF7FEFF 		bl	ascii_read_controller
 625 028c 0300     		movs	r3, r0
 626 028e 2370     		strb	r3, [r4]
 155:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(GPIO_MODER) = 0x55550000 | reg;
 627              		.loc 1 155 0
 628 0290 054B     		ldr	r3, .L37
 629 0292 7A68     		ldr	r2, [r7, #4]
 630 0294 0549     		ldr	r1, .L37+4
 631 0296 0A43     		orrs	r2, r1
 632 0298 1A60     		str	r2, [r3]
 156:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	return status;
 633              		.loc 1 156 0
 634 029a FB1C     		adds	r3, r7, #3
 635 029c 1B78     		ldrb	r3, [r3]
 157:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 636              		.loc 1 157 0
 637 029e 1800     		movs	r0, r3
 638 02a0 BD46     		mov	sp, r7
 639 02a2 03B0     		add	sp, sp, #12
 640              		@ sp needed
 641 02a4 90BD     		pop	{r4, r7, pc}
 642              	.L38:
 643 02a6 C046     		.align	2
 644              	.L37:
 645 02a8 00100240 		.word	1073876992
 646 02ac 00005555 		.word	1431633920
 647              		.cfi_endproc
 648              	.LFE11:
 650              		.align	2
 651              		.global	ascii_command
 652              		.code	16
 653              		.thumb_func
 655              	ascii_command:
 656              	.LFB12:
 158:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 159:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_command(unsigned char command, unsigned int delayUs){
 657              		.loc 1 159 0
 658              		.cfi_startproc
 659 02b0 80B5     		push	{r7, lr}
 660              		.cfi_def_cfa_offset 8
 661              		.cfi_offset 7, -8
 662              		.cfi_offset 14, -4
 663 02b2 82B0     		sub	sp, sp, #8
 664              		.cfi_def_cfa_offset 16
 665 02b4 00AF     		add	r7, sp, #0
 666              		.cfi_def_cfa_register 7
 667 02b6 0200     		movs	r2, r0
 668 02b8 3960     		str	r1, [r7]
 669 02ba FB1D     		adds	r3, r7, #7
 670 02bc 1A70     		strb	r2, [r3]
 160:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
 671              		.loc 1 160 0
 672 02be C046     		nop
 673              	.L40:
 674              		.loc 1 160 0 is_stmt 0 discriminator 1
 675 02c0 FFF7FEFF 		bl	ascii_read_status
 676 02c4 0300     		movs	r3, r0
 677 02c6 DBB2     		uxtb	r3, r3
 678 02c8 5BB2     		sxtb	r3, r3
 679 02ca 002B     		cmp	r3, #0
 680 02cc F8DB     		blt	.L40
 161:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_mikro(8); //Wait 8 us
 681              		.loc 1 161 0 is_stmt 1
 682 02ce 0820     		movs	r0, #8
 683 02d0 FFF7FEFF 		bl	delay_mikro
 162:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_write_cmd(command); //Send command
 684              		.loc 1 162 0
 685 02d4 FB1D     		adds	r3, r7, #7
 686 02d6 1B78     		ldrb	r3, [r3]
 687 02d8 1800     		movs	r0, r3
 688 02da FFF7FEFF 		bl	ascii_write_cmd
 163:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_mikro(delayUs); //Wait specific time
 689              		.loc 1 163 0
 690 02de 3B68     		ldr	r3, [r7]
 691 02e0 1800     		movs	r0, r3
 692 02e2 FFF7FEFF 		bl	delay_mikro
 164:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 693              		.loc 1 164 0
 694 02e6 C046     		nop
 695 02e8 BD46     		mov	sp, r7
 696 02ea 02B0     		add	sp, sp, #8
 697              		@ sp needed
 698 02ec 80BD     		pop	{r7, pc}
 699              		.cfi_endproc
 700              	.LFE12:
 702 02ee C046     		.align	2
 703              		.global	ascii_gotoPos
 704              		.code	16
 705              		.thumb_func
 707              	ascii_gotoPos:
 708              	.LFB13:
 165:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 166:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_gotoPos(int x, int y){
 709              		.loc 1 166 0
 710              		.cfi_startproc
 711 02f0 80B5     		push	{r7, lr}
 712              		.cfi_def_cfa_offset 8
 713              		.cfi_offset 7, -8
 714              		.cfi_offset 14, -4
 715 02f2 84B0     		sub	sp, sp, #16
 716              		.cfi_def_cfa_offset 24
 717 02f4 00AF     		add	r7, sp, #0
 718              		.cfi_def_cfa_register 7
 719 02f6 7860     		str	r0, [r7, #4]
 720 02f8 3960     		str	r1, [r7]
 167:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	if(x < 1){
 721              		.loc 1 167 0
 722 02fa 7B68     		ldr	r3, [r7, #4]
 723 02fc 002B     		cmp	r3, #0
 724 02fe 02DC     		bgt	.L42
 168:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		x = 1;
 725              		.loc 1 168 0
 726 0300 0123     		movs	r3, #1
 727 0302 7B60     		str	r3, [r7, #4]
 728 0304 04E0     		b	.L43
 729              	.L42:
 169:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}else if(x > 20){
 730              		.loc 1 169 0
 731 0306 7B68     		ldr	r3, [r7, #4]
 732 0308 142B     		cmp	r3, #20
 733 030a 01DD     		ble	.L43
 170:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		x = 20;
 734              		.loc 1 170 0
 735 030c 1423     		movs	r3, #20
 736 030e 7B60     		str	r3, [r7, #4]
 737              	.L43:
 171:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
 172:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	if(y < 1){
 738              		.loc 1 172 0
 739 0310 3B68     		ldr	r3, [r7]
 740 0312 002B     		cmp	r3, #0
 741 0314 02DC     		bgt	.L44
 173:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		y = 1;
 742              		.loc 1 173 0
 743 0316 0123     		movs	r3, #1
 744 0318 3B60     		str	r3, [r7]
 745 031a 04E0     		b	.L45
 746              	.L44:
 174:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}else if(y > 2){
 747              		.loc 1 174 0
 748 031c 3B68     		ldr	r3, [r7]
 749 031e 022B     		cmp	r3, #2
 750 0320 01DD     		ble	.L45
 175:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		y = 2;
 751              		.loc 1 175 0
 752 0322 0223     		movs	r3, #2
 753 0324 3B60     		str	r3, [r7]
 754              	.L45:
 176:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
 177:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
 178:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned char bits = x-1;
 755              		.loc 1 178 0
 756 0326 7B68     		ldr	r3, [r7, #4]
 757 0328 DAB2     		uxtb	r2, r3
 758 032a 0F23     		movs	r3, #15
 759 032c FB18     		adds	r3, r7, r3
 760 032e 013A     		subs	r2, r2, #1
 761 0330 1A70     		strb	r2, [r3]
 179:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	if(y == 2){
 762              		.loc 1 179 0
 763 0332 3B68     		ldr	r3, [r7]
 764 0334 022B     		cmp	r3, #2
 765 0336 06D1     		bne	.L46
 180:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		bits += 0x40;
 766              		.loc 1 180 0
 767 0338 0F23     		movs	r3, #15
 768 033a FB18     		adds	r3, r7, r3
 769 033c 0F22     		movs	r2, #15
 770 033e BA18     		adds	r2, r7, r2
 771 0340 1278     		ldrb	r2, [r2]
 772 0342 4032     		adds	r2, r2, #64
 773 0344 1A70     		strb	r2, [r3]
 774              	.L46:
 181:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
 182:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
 183:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_write_cmd(bits | 0x80);
 775              		.loc 1 183 0
 776 0346 0F23     		movs	r3, #15
 777 0348 FB18     		adds	r3, r7, r3
 778 034a 1B78     		ldrb	r3, [r3]
 779 034c 8022     		movs	r2, #128
 780 034e 5242     		rsbs	r2, r2, #0
 781 0350 1343     		orrs	r3, r2
 782 0352 DBB2     		uxtb	r3, r3
 783 0354 1800     		movs	r0, r3
 784 0356 FFF7FEFF 		bl	ascii_write_cmd
 184:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 785              		.loc 1 184 0
 786 035a C046     		nop
 787 035c BD46     		mov	sp, r7
 788 035e 04B0     		add	sp, sp, #16
 789              		@ sp needed
 790 0360 80BD     		pop	{r7, pc}
 791              		.cfi_endproc
 792              	.LFE13:
 794 0362 C046     		.align	2
 795              		.global	ascii_write_char
 796              		.code	16
 797              		.thumb_func
 799              	ascii_write_char:
 800              	.LFB14:
 185:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 186:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void ascii_write_char(unsigned char c){
 801              		.loc 1 186 0
 802              		.cfi_startproc
 803 0364 80B5     		push	{r7, lr}
 804              		.cfi_def_cfa_offset 8
 805              		.cfi_offset 7, -8
 806              		.cfi_offset 14, -4
 807 0366 82B0     		sub	sp, sp, #8
 808              		.cfi_def_cfa_offset 16
 809 0368 00AF     		add	r7, sp, #0
 810              		.cfi_def_cfa_register 7
 811 036a 0200     		movs	r2, r0
 812 036c FB1D     		adds	r3, r7, #7
 813 036e 1A70     		strb	r2, [r3]
 187:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
 814              		.loc 1 187 0
 815 0370 C046     		nop
 816              	.L48:
 817              		.loc 1 187 0 is_stmt 0 discriminator 1
 818 0372 FFF7FEFF 		bl	ascii_read_status
 819 0376 0300     		movs	r3, r0
 820 0378 DBB2     		uxtb	r3, r3
 821 037a 5BB2     		sxtb	r3, r3
 822 037c 002B     		cmp	r3, #0
 823 037e F8DB     		blt	.L48
 188:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_mikro(8); 
 824              		.loc 1 188 0 is_stmt 1
 825 0380 0820     		movs	r0, #8
 826 0382 FFF7FEFF 		bl	delay_mikro
 189:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	ascii_write_data(c);
 827              		.loc 1 189 0
 828 0386 FB1D     		adds	r3, r7, #7
 829 0388 1B78     		ldrb	r3, [r3]
 830 038a 1800     		movs	r0, r3
 831 038c FFF7FEFF 		bl	ascii_write_data
 190:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	delay_mikro(43);
 832              		.loc 1 190 0
 833 0390 2B20     		movs	r0, #43
 834 0392 FFF7FEFF 		bl	delay_mikro
 191:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 835              		.loc 1 191 0
 836 0396 C046     		nop
 837 0398 BD46     		mov	sp, r7
 838 039a 02B0     		add	sp, sp, #8
 839              		@ sp needed
 840 039c 80BD     		pop	{r7, pc}
 841              		.cfi_endproc
 842              	.LFE14:
 844 039e C046     		.align	2
 845              		.global	delay_milli
 846              		.code	16
 847              		.thumb_func
 849              	delay_milli:
 850              	.LFB15:
 192:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 193:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_milli(unsigned int ms){
 851              		.loc 1 193 0
 852              		.cfi_startproc
 853 03a0 80B5     		push	{r7, lr}
 854              		.cfi_def_cfa_offset 8
 855              		.cfi_offset 7, -8
 856              		.cfi_offset 14, -4
 857 03a2 84B0     		sub	sp, sp, #16
 858              		.cfi_def_cfa_offset 24
 859 03a4 00AF     		add	r7, sp, #0
 860              		.cfi_def_cfa_register 7
 861 03a6 7860     		str	r0, [r7, #4]
 194:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	#ifdef SIMULATOR
 195:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		ms = 5; //Some weird bug here with division
 196:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	#endif
 197:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
 198:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	int i;
 199:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	for(i = 0; i < 1000; i++){
 862              		.loc 1 199 0
 863 03a8 0023     		movs	r3, #0
 864 03aa FB60     		str	r3, [r7, #12]
 865 03ac 06E0     		b	.L50
 866              	.L51:
 200:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_mikro(ms);
 867              		.loc 1 200 0 discriminator 3
 868 03ae 7B68     		ldr	r3, [r7, #4]
 869 03b0 1800     		movs	r0, r3
 870 03b2 FFF7FEFF 		bl	delay_mikro
 199:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_mikro(ms);
 871              		.loc 1 199 0 discriminator 3
 872 03b6 FB68     		ldr	r3, [r7, #12]
 873 03b8 0133     		adds	r3, r3, #1
 874 03ba FB60     		str	r3, [r7, #12]
 875              	.L50:
 199:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_mikro(ms);
 876              		.loc 1 199 0 is_stmt 0 discriminator 1
 877 03bc FB68     		ldr	r3, [r7, #12]
 878 03be 034A     		ldr	r2, .L52
 879 03c0 9342     		cmp	r3, r2
 880 03c2 F4DD     		ble	.L51
 201:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
 202:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 881              		.loc 1 202 0 is_stmt 1
 882 03c4 C046     		nop
 883 03c6 BD46     		mov	sp, r7
 884 03c8 04B0     		add	sp, sp, #16
 885              		@ sp needed
 886 03ca 80BD     		pop	{r7, pc}
 887              	.L53:
 888              		.align	2
 889              	.L52:
 890 03cc E7030000 		.word	999
 891              		.cfi_endproc
 892              	.LFE15:
 894              		.align	2
 895              		.global	delay_mikro
 896              		.code	16
 897              		.thumb_func
 899              	delay_mikro:
 900              	.LFB16:
 203:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 204:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_mikro(unsigned int us){
 901              		.loc 1 204 0
 902              		.cfi_startproc
 903 03d0 80B5     		push	{r7, lr}
 904              		.cfi_def_cfa_offset 8
 905              		.cfi_offset 7, -8
 906              		.cfi_offset 14, -4
 907 03d2 84B0     		sub	sp, sp, #16
 908              		.cfi_def_cfa_offset 24
 909 03d4 00AF     		add	r7, sp, #0
 910              		.cfi_def_cfa_register 7
 911 03d6 7860     		str	r0, [r7, #4]
 205:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	unsigned int i;
 206:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
 207:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	for(i = us; i > 0; i--){
 912              		.loc 1 207 0
 913 03d8 7B68     		ldr	r3, [r7, #4]
 914 03da FB60     		str	r3, [r7, #12]
 915 03dc 0AE0     		b	.L55
 916              	.L56:
 208:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 917              		.loc 1 208 0 discriminator 3
 918 03de FFF7FEFF 		bl	delay_250ns
 209:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 919              		.loc 1 209 0 discriminator 3
 920 03e2 FFF7FEFF 		bl	delay_250ns
 210:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 921              		.loc 1 210 0 discriminator 3
 922 03e6 FFF7FEFF 		bl	delay_250ns
 211:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 923              		.loc 1 211 0 discriminator 3
 924 03ea FFF7FEFF 		bl	delay_250ns
 207:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 925              		.loc 1 207 0 discriminator 3
 926 03ee FB68     		ldr	r3, [r7, #12]
 927 03f0 013B     		subs	r3, r3, #1
 928 03f2 FB60     		str	r3, [r7, #12]
 929              	.L55:
 207:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 		delay_250ns();
 930              		.loc 1 207 0 is_stmt 0 discriminator 1
 931 03f4 FB68     		ldr	r3, [r7, #12]
 932 03f6 002B     		cmp	r3, #0
 933 03f8 F1D1     		bne	.L56
 212:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	}
 213:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }
 934              		.loc 1 213 0 is_stmt 1
 935 03fa C046     		nop
 936 03fc BD46     		mov	sp, r7
 937 03fe 04B0     		add	sp, sp, #16
 938              		@ sp needed
 939 0400 80BD     		pop	{r7, pc}
 940              		.cfi_endproc
 941              	.LFE16:
 943 0402 C046     		.align	2
 944              		.global	delay_250ns
 945              		.code	16
 946              		.thumb_func
 948              	delay_250ns:
 949              	.LFB17:
 214:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 
 215:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** void delay_250ns(void){
 950              		.loc 1 215 0
 951              		.cfi_startproc
 952 0404 80B5     		push	{r7, lr}
 953              		.cfi_def_cfa_offset 8
 954              		.cfi_offset 7, -8
 955              		.cfi_offset 14, -4
 956 0406 00AF     		add	r7, sp, #0
 957              		.cfi_def_cfa_register 7
 216:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(STK_CTRL) = 0;
 958              		.loc 1 216 0
 959 0408 0C4B     		ldr	r3, .L59
 960 040a 0022     		movs	r2, #0
 961 040c 1A60     		str	r2, [r3]
 217:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(STK_LOAD) = 168/4 - 1;
 962              		.loc 1 217 0
 963 040e 0C4B     		ldr	r3, .L59+4
 964 0410 2922     		movs	r2, #41
 965 0412 1A60     		str	r2, [r3]
 218:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(STK_VAL) = 0;
 966              		.loc 1 218 0
 967 0414 0B4B     		ldr	r3, .L59+8
 968 0416 0022     		movs	r2, #0
 969 0418 1A60     		str	r2, [r3]
 219:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(STK_CTRL) = 0x5; //ends with 0101
 970              		.loc 1 219 0
 971 041a 084B     		ldr	r3, .L59
 972 041c 0522     		movs	r2, #5
 973 041e 1A60     		str	r2, [r3]
 220:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	while((*(STK_CTRL) & 0x10000) == 0); //wait for bit 16 to be 1
 974              		.loc 1 220 0
 975 0420 C046     		nop
 976              	.L58:
 977              		.loc 1 220 0 is_stmt 0 discriminator 1
 978 0422 064B     		ldr	r3, .L59
 979 0424 1A68     		ldr	r2, [r3]
 980 0426 8023     		movs	r3, #128
 981 0428 5B02     		lsls	r3, r3, #9
 982 042a 1340     		ands	r3, r2
 983 042c F9D0     		beq	.L58
 221:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	*(STK_CTRL) = 0;
 984              		.loc 1 221 0 is_stmt 1
 985 042e 034B     		ldr	r3, .L59
 986 0430 0022     		movs	r2, #0
 987 0432 1A60     		str	r2, [r3]
 222:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	
 223:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	//Kontrollregister:
 224:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	/*Bit 16: 1 om klar
 225:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	 * Bit 2: 1 efter RESET, använd systemklocka om 1 annars systemklocka/8
 226:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	 * Bit 1: Aktivera avbrott
 227:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	 * Bit 0: ENABLE
 228:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	 * DET ÄR DISPLAYEN SOM HAR ALL MAGI :o
 229:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** 	 */
 230:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/asciidisplay\asciidisplay.c **** }...
 988              		.loc 1 230 0
 989 0434 C046     		nop
 990 0436 BD46     		mov	sp, r7
 991              		@ sp needed
 992 0438 80BD     		pop	{r7, pc}
 993              	.L60:
 994 043a C046     		.align	2
 995              	.L59:
 996 043c 10E000E0 		.word	-536813552
 997 0440 14E000E0 		.word	-536813548
 998 0444 18E000E0 		.word	-536813544
 999              		.cfi_endproc
 1000              	.LFE17:
 1002              	.Letext0:
