

#define GPIO_MODER (unsigned volatile long*) 0x40021000
#define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
#define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
#define GPIO_IDR_LOW (unsigned volatile char*) 0x40021010
#define GPIO_IDR_HIGH (unsigned volatile char*) 0x40021011
#define GPIO_ODR_LOW (unsigned volatile char*) 0x40021014
#define GPIO_ODR_HIGH (unsigned volatile char*) 0x40021015


#define B_E 0x40
#define B_SELECT 0x4
#define B_RW 0x2
#define B_RS 0x1

#define STK_CTRL (unsigned volatile long*) 0xE000E010
#define STK_LOAD (unsigned volatile long*) 0xE000E014
#define STK_VAL (unsigned volatile long*) 0xE000E018
#define STK_CALIB (unsigned volatile long*) 0xE000E01C


void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
void delay_250ns(void);
void delay_mikro(unsigned int us);
void delay_milli(unsigned int ms);
void ascii_ctrl_bit_set(unsigned char x);
void ascii_ctrl_bit_clear(unsigned char x);
void ascii_write_controller(unsigned char);
unsigned char ascii_read_controller(void);
void ascii_write_cmd(unsigned char command);
void ascii_write_data(unsigned char data);
unsigned char ascii_read_status(void);
unsigned char ascii_read_data(void);
void ascii_command(unsigned char command, unsigned int delayUs);
void ascii_gotoPos(int x, int y);
void ascii_write_char(unsigned char c);

void startup ( void )
{
asm volatile(
	"LDR R0, =0x40023830\n"
	"MOV R1, #0x18\n"
	"STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void init_app(void){
	//Set all pins to outputs
	*(GPIO_MODER) = 0x55555555;
}

void ascii_init(void){
	//Set rows and size
	ascii_command(0x38, 39);
	
	//Start display, start and set marker constant
	ascii_command(0x0E, 39);
	
	//Clear display
	ascii_command(0x01, 1530);
	
	//Set increment and no shift
	ascii_command(0x03, 39);
}

int main(void)
{
	char* s;
	char test1[] = "I need some";
	char test2[] = "goddamn tea!";
	
	init_app();
	ascii_init();
	ascii_gotoPos(1,1);
	s = test1;
	
	//ascii_write_char('a');
	while(*s){
		ascii_write_char(*s++);
	}
	ascii_gotoPos(1,2);
	s = test2;
	while(*s){
		ascii_write_char(*s++);
	}
	
	return 0;
}

//Set set bits in x to 1
void ascii_ctrl_bit_set(unsigned char x){ 
	unsigned char c = *(GPIO_ODR_LOW);
	c = c | (B_SELECT | x);
	*(GPIO_ODR_LOW) = c;
}

//Set set bits in x to 0
void ascii_ctrl_bit_clear(unsigned char x){
	unsigned char c;
	c = *(GPIO_ODR_LOW);
	c = B_SELECT | (c & ~x);
	*(GPIO_ODR_LOW) = c;
}

void ascii_write_controller(unsigned char byte){
	ascii_ctrl_bit_set(B_E);
	*(GPIO_ODR_HIGH) = byte;
	delay_250ns();
	ascii_ctrl_bit_clear(B_E);
}

unsigned char ascii_read_controller(void){
	ascii_ctrl_bit_set(B_E);
	delay_250ns();
	delay_250ns();
	unsigned char byte = *(GPIO_IDR_HIGH);
	ascii_ctrl_bit_clear(B_E);
	return byte;
}

void ascii_write_cmd(unsigned char command){
	ascii_ctrl_bit_clear(B_RS);
	ascii_ctrl_bit_clear(B_RW);
	ascii_write_controller(command);
}

void ascii_write_data(unsigned char data){
	ascii_ctrl_bit_set(B_RS);
	ascii_ctrl_bit_clear(B_RW);
	ascii_write_controller(data);
}

unsigned char ascii_read_status(void){
	*(GPIO_MODER) = 0x00005555;
	ascii_ctrl_bit_clear(B_RS);
	ascii_ctrl_bit_set(B_RW);
	unsigned char status = ascii_read_controller();
	*(GPIO_MODER) = 0x55555555;
	*(GPIO_ODR_HIGH) = status;
	return status;
}

unsigned char ascii_read_data(void){
	unsigned int reg = *(GPIO_MODER);
	*(GPIO_MODER) = 0x0000FFFF & reg;
	ascii_ctrl_bit_set(B_RS);
	ascii_ctrl_bit_set(B_RW);
	unsigned char status = ascii_read_controller();
	*(GPIO_MODER) = 0x55550000 | reg;
	return status;
}

void ascii_command(unsigned char command, unsigned int delayUs){
	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
	delay_mikro(8); //Wait 8 us
	ascii_write_cmd(command); //Send command
	delay_mikro(delayUs); //Wait specific time
}

void ascii_gotoPos(int x, int y){
	if(x < 1){
		x = 1;
	}else if(x > 20){
		x = 20;
	}
	if(y < 1){
		y = 1;
	}else if(y > 2){
		y = 2;
	}
	
	unsigned char bits = x-1;
	if(y == 2){
		bits += 0x40;
	}
	
	ascii_write_cmd(bits | 0x80);
}

void ascii_write_char(unsigned char c){
	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
	delay_mikro(8); 
	ascii_write_data(c);
	delay_mikro(43);
}

void delay_milli(unsigned int ms){
	#ifdef SIMULATOR
		ms = 5; //Some weird bug here with division
	#endif
	
	int i;
	for(i = 0; i < 1000; i++){
		delay_mikro(ms);
	}
}

void delay_mikro(unsigned int us){
	unsigned int i;
	
	for(i = us; i > 0; i--){
		delay_250ns();
		delay_250ns();
		delay_250ns();
		delay_250ns();
	}
}

void delay_250ns(void){
	*(STK_CTRL) = 0;
	*(STK_LOAD) = 168/4 - 1;
	*(STK_VAL) = 0;
	*(STK_CTRL) = 0x5; //ends with 0101
	while((*(STK_CTRL) & 0x10000) == 0); //wait for bit 16 to be 1
	*(STK_CTRL) = 0;
	
	//Kontrollregister:
	/*Bit 16: 1 om klar
	 * Bit 2: 1 efter RESET, använd systemklocka om 1 annars systemklocka/8
	 * Bit 1: Aktivera avbrott
	 * Bit 0: ENABLE
	 * DET ÄR DISPLAYEN SOM HAR ALL MAGI :o
	 */
}