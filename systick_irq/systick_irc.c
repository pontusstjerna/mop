#include "boolean.h"

//#define SIMULATOR

#ifdef SIMULATOR
#define DELAY_COUNT 100
#else
#define DELAY_COUNT 100000
#endif

#define GPIO_MODER (unsigned volatile long*) 0x40021000
#define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
#define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
#define GPIO_IDR_LOW (vbyte*) 0x40021010
#define GPIO_IDR_HIGH (vbyte*) 0x40021011
#define GPIO_ODR_LOW (vbyte*) 0x40021014
#define GPIO_ODR_HIGH (vbyte*) 0x40021015

#define STK_CTRL (unsigned volatile long*) 0xE000E010
#define STK_LOAD (unsigned volatile long*) 0xE000E014
#define STK_VAL (unsigned volatile long*) 0xE000E018
#define STK_CALIB (unsigned volatile long*) 0xE000E01C
#define RCC_APB2ENR (unsigned int *) 0x40023844

typedef unsigned char byte;
typedef unsigned volatile char vbyte;

/*
static struct GPIO
{
	byte *gpio;
	unsigned volatile long *MODER = gpio;
	unsigned volatile short *OTYPER = GPIO + 4;
	unsigned volatile long *OSPEEDR = GPIO + 8;
	unsigned volatile long *PUPDR = GPIO + 0xC;
	vbyte* IDR_LOW = GPIO + 10;
	vbyte* IDR_HIGH = GPIO + 11;
	vbyte* ODR_LOW = GPIO + 14;
	vbyte* ODR_HIGH = GPIO + 15;
} ;*/

void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void init_app(void);

void delay250(int);

void delay(unsigned long long);

void light_me(vbyte*);

void startup ( void )
{
asm volatile(
	" LDR R0, =0x40023830\n"
	" MOV R1, #0x18\n"
	" STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

boolean systick_flag = false;

//Our interrupt function
void systick_irq_handler(void)
{
	*(STK_CTRL) = 0;
	systick_flag = true;
}

void delay250ns()
{
	systick_flag = false;
	*(STK_LOAD) = 168/4 - 1;
	*(STK_VAL) = 0;
	*(STK_CTRL) = 7;
}

void delay(unsigned long long count)
{
	while(count)
	{
		delay250ns();
		while(!systick_flag);
		count--;
	}
}

void init_app()
{
	*(GPIO_MODER) = 0x55555555;
	*((void (**) (void)) 0x2001C03C) = systick_irq_handler;
}

int main(void)
{
	init_app();
	
	
	while(true)
	{
		light_me(GPIO_ODR_LOW);
		delay(DELAY_COUNT);
	}
	
	return 0;
}

signed int i = 0;
signed char sign = 1;
void light_me(vbyte* port)
{
	*(port) = (1<<(i += sign));
	if(i > 6 || i < 1)
		sign = -sign;
	return;
}