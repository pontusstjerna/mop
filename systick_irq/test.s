   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"systick_irc.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "C:/Users/alebabu/Documents/mop/systick_irq/systick_irc.c"
   1:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #include "boolean.h"
   2:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
   3:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** //#define SIMULATOR
   4:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
   5:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #ifdef SIMULATOR
   6:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define DELAY_COUNT 100
   7:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #else
   8:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define DELAY_COUNT 100000
   9:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #endif
  10:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  11:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_MODER (unsigned volatile long*) 0x40021000
  12:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
  13:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
  14:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
  15:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_IDR_LOW (vbyte*) 0x40021010
  16:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_IDR_HIGH (vbyte*) 0x40021011
  17:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_ODR_LOW (vbyte*) 0x40021014
  18:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define GPIO_ODR_HIGH (vbyte*) 0x40021015
  19:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  20:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define STK_CTRL (unsigned volatile long*) 0xE000E010
  21:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define STK_LOAD (unsigned volatile long*) 0xE000E014
  22:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define STK_VAL (unsigned volatile long*) 0xE000E018
  23:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define STK_CALIB (unsigned volatile long*) 0xE000E01C
  24:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** #define RCC_APB2ENR (unsigned int *) 0x40023844
  25:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  26:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** typedef unsigned char byte;
  27:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** typedef unsigned volatile char vbyte;
  28:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  29:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** /*
  30:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** static struct GPIO
  31:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
  32:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	byte *gpio;
  33:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	unsigned volatile long *MODER = gpio;
  34:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	unsigned volatile short *OTYPER = GPIO + 4;
  35:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	unsigned volatile long *OSPEEDR = GPIO + 8;
  36:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	unsigned volatile long *PUPDR = GPIO + 0xC;
  37:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	vbyte* IDR_LOW = GPIO + 10;
  38:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	vbyte* IDR_HIGH = GPIO + 11;
  39:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	vbyte* ODR_LOW = GPIO + 14;
  40:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	vbyte* ODR_HIGH = GPIO + 15;
  41:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** } ;*/
  42:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  43:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  44:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  45:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void init_app(void);
  46:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  47:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void delay250(int);
  48:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  49:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void delay(unsigned long long);
  50:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  51:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void light_me(vbyte*);
  52:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  53:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void startup ( void )
  54:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
  28              		.loc 1 54 0
  29              		.cfi_startproc
  55:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** asm volatile(
  30              		.loc 1 55 0
  31              		.syntax divided
  32              	@ 55 "C:/Users/alebabu/Documents/mop/systick_irq/systick_irc.c" 1
  33 0000 0448     		 LDR R0, =0x40023830
  34 0002 1821     	 MOV R1, #0x18
  35 0004 0160     	 STR R1, [R0]
  36 0006 0448     	 LDR R0,=0x2001C000
  37 0008 8546     	 MOV SP,R0
  38 000a FFF7FEFF 	 BL main
  39 000e FEE7     	.L1: B .L1
  40              	
  41              	@ 0 "" 2
  56:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" LDR R0, =0x40023830\n"
  57:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" MOV R1, #0x18\n"
  58:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" STR R1, [R0]\n"
  59:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  60:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" MOV SP,R0\n"
  61:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	" BL main\n"				/* call main */
  62:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	".L1: B .L1\n"				/* never return */
  63:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	) ;
  64:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
  42              		.loc 1 64 0
  43              		.thumb
  44              		.syntax unified
  45 0010 C046     		nop
  46              		.cfi_endproc
  47              	.LFE0:
  49              		.global	systick_flag
  50 0012 0000     		.bss
  53              	systick_flag:
  54 0000 00       		.space	1
  55              		.text
  56              		.align	2
  57              		.global	systick_irq_handler
  58              		.code	16
  59              		.thumb_func
  61              	systick_irq_handler:
  62              	.LFB1:
  65:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  66:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** boolean systick_flag = false;
  67:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  68:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** //Our interrupt function
  69:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void systick_irq_handler(void)
  70:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
  63              		.loc 1 70 0
  64              		.cfi_startproc
  65 0000 80B5     		push	{r7, lr}
  66              		.cfi_def_cfa_offset 8
  67              		.cfi_offset 7, -8
  68              		.cfi_offset 14, -4
  69 0002 00AF     		add	r7, sp, #0
  70              		.cfi_def_cfa_register 7
  71:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(STK_CTRL) = 0;
  71              		.loc 1 71 0
  72 0004 044B     		ldr	r3, .L3
  73 0006 0022     		movs	r2, #0
  74 0008 1A60     		str	r2, [r3]
  72:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	systick_flag = true;
  75              		.loc 1 72 0
  76 000a 044B     		ldr	r3, .L3+4
  77 000c 0122     		movs	r2, #1
  78 000e 1A70     		strb	r2, [r3]
  73:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
  79              		.loc 1 73 0
  80 0010 C046     		nop
  81 0012 BD46     		mov	sp, r7
  82              		@ sp needed
  83 0014 80BD     		pop	{r7, pc}
  84              	.L4:
  85 0016 C046     		.align	2
  86              	.L3:
  87 0018 10E000E0 		.word	-536813552
  88 001c 00000000 		.word	systick_flag
  89              		.cfi_endproc
  90              	.LFE1:
  92              		.align	2
  93              		.global	delay250ns
  94              		.code	16
  95              		.thumb_func
  97              	delay250ns:
  98              	.LFB2:
  74:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  75:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void delay250ns()
  76:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
  99              		.loc 1 76 0
 100              		.cfi_startproc
 101 0020 80B5     		push	{r7, lr}
 102              		.cfi_def_cfa_offset 8
 103              		.cfi_offset 7, -8
 104              		.cfi_offset 14, -4
 105 0022 00AF     		add	r7, sp, #0
 106              		.cfi_def_cfa_register 7
  77:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	systick_flag = false;
 107              		.loc 1 77 0
 108 0024 074B     		ldr	r3, .L6
 109 0026 0022     		movs	r2, #0
 110 0028 1A70     		strb	r2, [r3]
  78:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(STK_LOAD) = 168/4 - 1;
 111              		.loc 1 78 0
 112 002a 074B     		ldr	r3, .L6+4
 113 002c 2922     		movs	r2, #41
 114 002e 1A60     		str	r2, [r3]
  79:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(STK_VAL) = 0;
 115              		.loc 1 79 0
 116 0030 064B     		ldr	r3, .L6+8
 117 0032 0022     		movs	r2, #0
 118 0034 1A60     		str	r2, [r3]
  80:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(STK_CTRL) = 7;
 119              		.loc 1 80 0
 120 0036 064B     		ldr	r3, .L6+12
 121 0038 0722     		movs	r2, #7
 122 003a 1A60     		str	r2, [r3]
  81:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
 123              		.loc 1 81 0
 124 003c C046     		nop
 125 003e BD46     		mov	sp, r7
 126              		@ sp needed
 127 0040 80BD     		pop	{r7, pc}
 128              	.L7:
 129 0042 C046     		.align	2
 130              	.L6:
 131 0044 00000000 		.word	systick_flag
 132 0048 14E000E0 		.word	-536813548
 133 004c 18E000E0 		.word	-536813544
 134 0050 10E000E0 		.word	-536813552
 135              		.cfi_endproc
 136              	.LFE2:
 138              		.align	2
 139              		.global	delay
 140              		.code	16
 141              		.thumb_func
 143              	delay:
 144              	.LFB3:
  82:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  83:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void delay(unsigned long long count)
  84:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
 145              		.loc 1 84 0
 146              		.cfi_startproc
 147 0054 90B5     		push	{r4, r7, lr}
 148              		.cfi_def_cfa_offset 12
 149              		.cfi_offset 4, -12
 150              		.cfi_offset 7, -8
 151              		.cfi_offset 14, -4
 152 0056 83B0     		sub	sp, sp, #12
 153              		.cfi_def_cfa_offset 24
 154 0058 00AF     		add	r7, sp, #0
 155              		.cfi_def_cfa_register 7
 156 005a 3860     		str	r0, [r7]
 157 005c 7960     		str	r1, [r7, #4]
  85:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	while(count)
 158              		.loc 1 85 0
 159 005e 0FE0     		b	.L9
 160              	.L11:
  86:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	{
  87:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		delay250ns();
 161              		.loc 1 87 0
 162 0060 FFF7FEFF 		bl	delay250ns
  88:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		while(!systick_flag);
 163              		.loc 1 88 0
 164 0064 C046     		nop
 165              	.L10:
 166              		.loc 1 88 0 is_stmt 0 discriminator 1
 167 0066 0A4B     		ldr	r3, .L12
 168 0068 1B78     		ldrb	r3, [r3]
 169 006a 002B     		cmp	r3, #0
 170 006c FBD0     		beq	.L10
  89:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		count--;
 171              		.loc 1 89 0 is_stmt 1
 172 006e 3B68     		ldr	r3, [r7]
 173 0070 7C68     		ldr	r4, [r7, #4]
 174 0072 0121     		movs	r1, #1
 175 0074 4942     		rsbs	r1, r1, #0
 176 0076 CA17     		asrs	r2, r1, #31
 177 0078 5B18     		adds	r3, r3, r1
 178 007a 5441     		adcs	r4, r4, r2
 179 007c 3B60     		str	r3, [r7]
 180 007e 7C60     		str	r4, [r7, #4]
 181              	.L9:
  85:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	{
 182              		.loc 1 85 0
 183 0080 3B68     		ldr	r3, [r7]
 184 0082 7A68     		ldr	r2, [r7, #4]
 185 0084 1343     		orrs	r3, r2
 186 0086 EBD1     		bne	.L11
  90:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	}
  91:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
 187              		.loc 1 91 0
 188 0088 C046     		nop
 189 008a BD46     		mov	sp, r7
 190 008c 03B0     		add	sp, sp, #12
 191              		@ sp needed
 192 008e 90BD     		pop	{r4, r7, pc}
 193              	.L13:
 194              		.align	2
 195              	.L12:
 196 0090 00000000 		.word	systick_flag
 197              		.cfi_endproc
 198              	.LFE3:
 200              		.align	2
 201              		.global	init_app
 202              		.code	16
 203              		.thumb_func
 205              	init_app:
 206              	.LFB4:
  92:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  93:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void init_app()
  94:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
 207              		.loc 1 94 0
 208              		.cfi_startproc
 209 0094 80B5     		push	{r7, lr}
 210              		.cfi_def_cfa_offset 8
 211              		.cfi_offset 7, -8
 212              		.cfi_offset 14, -4
 213 0096 00AF     		add	r7, sp, #0
 214              		.cfi_def_cfa_register 7
  95:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(GPIO_MODER) = 0x55555555;
 215              		.loc 1 95 0
 216 0098 024B     		ldr	r3, .L15
 217 009a 034A     		ldr	r2, .L15+4
 218 009c 1A60     		str	r2, [r3]
  96:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
 219              		.loc 1 96 0
 220 009e C046     		nop
 221 00a0 BD46     		mov	sp, r7
 222              		@ sp needed
 223 00a2 80BD     		pop	{r7, pc}
 224              	.L16:
 225              		.align	2
 226              	.L15:
 227 00a4 00100240 		.word	1073876992
 228 00a8 55555555 		.word	1431655765
 229              		.cfi_endproc
 230              	.LFE4:
 232              		.align	2
 233              		.global	main
 234              		.code	16
 235              		.thumb_func
 237              	main:
 238              	.LFB5:
  97:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
  98:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** int main(void)
  99:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
 239              		.loc 1 99 0
 240              		.cfi_startproc
 241 00ac B0B5     		push	{r4, r5, r7, lr}
 242              		.cfi_def_cfa_offset 16
 243              		.cfi_offset 4, -16
 244              		.cfi_offset 5, -12
 245              		.cfi_offset 7, -8
 246              		.cfi_offset 14, -4
 247 00ae 00AF     		add	r7, sp, #0
 248              		.cfi_def_cfa_register 7
 100:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	init_app();
 249              		.loc 1 100 0
 250 00b0 FFF7FEFF 		bl	init_app
 101:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*((void (**) (void)) 0x2001C03C) = systick_irq_handler;
 251              		.loc 1 101 0
 252 00b4 064B     		ldr	r3, .L19
 253 00b6 074A     		ldr	r2, .L19+4
 254 00b8 1A60     		str	r2, [r3]
 255              	.L18:
 102:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	
 103:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	while(true)
 104:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	{
 105:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		light_me(GPIO_ODR_LOW);
 256              		.loc 1 105 0 discriminator 1
 257 00ba 074B     		ldr	r3, .L19+8
 258 00bc 1800     		movs	r0, r3
 259 00be FFF7FEFF 		bl	light_me
 106:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		delay(DELAY_COUNT);
 260              		.loc 1 106 0 discriminator 1
 261 00c2 064B     		ldr	r3, .L19+12
 262 00c4 0024     		movs	r4, #0
 263 00c6 1800     		movs	r0, r3
 264 00c8 2100     		movs	r1, r4
 265 00ca FFF7FEFF 		bl	delay
 107:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	}
 266              		.loc 1 107 0 discriminator 1
 267 00ce F4E7     		b	.L18
 268              	.L20:
 269              		.align	2
 270              	.L19:
 271 00d0 3CC00120 		.word	536985660
 272 00d4 00000000 		.word	systick_irq_handler
 273 00d8 14100240 		.word	1073877012
 274 00dc A0860100 		.word	100000
 275              		.cfi_endproc
 276              	.LFE5:
 278              		.global	i
 279              		.bss
 280 0001 000000   		.align	2
 283              	i:
 284 0004 00000000 		.space	4
 285              		.global	sign
 286              		.data
 289              	sign:
 290 0000 01       		.byte	1
 291              		.text
 292              		.align	2
 293              		.global	light_me
 294              		.code	16
 295              		.thumb_func
 297              	light_me:
 298              	.LFB6:
 108:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	
 109:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	return 0;
 110:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }
 111:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 
 112:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** signed int i = 0;
 113:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** signed char sign = 1;
 114:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** void light_me(vbyte* port)
 115:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** {
 299              		.loc 1 115 0
 300              		.cfi_startproc
 301 00e0 80B5     		push	{r7, lr}
 302              		.cfi_def_cfa_offset 8
 303              		.cfi_offset 7, -8
 304              		.cfi_offset 14, -4
 305 00e2 82B0     		sub	sp, sp, #8
 306              		.cfi_def_cfa_offset 16
 307 00e4 00AF     		add	r7, sp, #0
 308              		.cfi_def_cfa_register 7
 309 00e6 7860     		str	r0, [r7, #4]
 116:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	*(port) = (1<<(i += sign));
 310              		.loc 1 116 0
 311 00e8 124B     		ldr	r3, .L26
 312 00ea 1B78     		ldrb	r3, [r3]
 313 00ec 5AB2     		sxtb	r2, r3
 314 00ee 124B     		ldr	r3, .L26+4
 315 00f0 1B68     		ldr	r3, [r3]
 316 00f2 D218     		adds	r2, r2, r3
 317 00f4 104B     		ldr	r3, .L26+4
 318 00f6 1A60     		str	r2, [r3]
 319 00f8 0F4B     		ldr	r3, .L26+4
 320 00fa 1B68     		ldr	r3, [r3]
 321 00fc 0122     		movs	r2, #1
 322 00fe 9A40     		lsls	r2, r2, r3
 323 0100 1300     		movs	r3, r2
 324 0102 DAB2     		uxtb	r2, r3
 325 0104 7B68     		ldr	r3, [r7, #4]
 326 0106 1A70     		strb	r2, [r3]
 117:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	if(i > 6 || i < 1)
 327              		.loc 1 117 0
 328 0108 0B4B     		ldr	r3, .L26+4
 329 010a 1B68     		ldr	r3, [r3]
 330 010c 062B     		cmp	r3, #6
 331 010e 03DC     		bgt	.L22
 332              		.loc 1 117 0 is_stmt 0 discriminator 1
 333 0110 094B     		ldr	r3, .L26+4
 334 0112 1B68     		ldr	r3, [r3]
 335 0114 002B     		cmp	r3, #0
 336 0116 08DC     		bgt	.L25
 337              	.L22:
 118:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 		sign = -sign;
 338              		.loc 1 118 0 is_stmt 1
 339 0118 064B     		ldr	r3, .L26
 340 011a 1B78     		ldrb	r3, [r3]
 341 011c DBB2     		uxtb	r3, r3
 342 011e 5B42     		rsbs	r3, r3, #0
 343 0120 DBB2     		uxtb	r3, r3
 344 0122 DAB2     		uxtb	r2, r3
 345 0124 034B     		ldr	r3, .L26
 346 0126 1A70     		strb	r2, [r3]
 119:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** 	return;
 347              		.loc 1 119 0
 348 0128 C046     		nop
 349              	.L25:
 350 012a C046     		nop
 120:C:/Users/alebabu/Documents/mop/systick_irq\systick_irc.c **** }...
 351              		.loc 1 120 0
 352 012c BD46     		mov	sp, r7
 353 012e 02B0     		add	sp, sp, #8
 354              		@ sp needed
 355 0130 80BD     		pop	{r7, pc}
 356              	.L27:
 357 0132 C046     		.align	2
 358              	.L26:
 359 0134 00000000 		.word	sign
 360 0138 00000000 		.word	i
 361              		.cfi_endproc
 362              	.LFE6:
 364              	.Letext0:
 365              		.file 2 "C:/Users/alebabu/Documents/mop/systick_irq/boolean.h"
