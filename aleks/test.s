   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"asciidisplayAleks.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "F:/Programming/C/MOP/aleks/asciidisplayAleks.c"
   1:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** /*
   2:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****  * 	startup.c
   3:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****  *
   4:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****  */
   5:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
   6:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
   7:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_250ns(void);
   8:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_mikro(unsigned int);
   9:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_milli(unsigned int);
  10:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_ctrl_bit_set(unsigned char);
  11:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_ctrl_bit_clear(unsigned char);
  12:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_controller(unsigned char);
  13:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_controller(void);
  14:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_cmd(unsigned char);
  15:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_data(unsigned char);
  16:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_status(void);
  17:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_data(void);
  18:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_init(void);
  19:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void init_app(void);
  20:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_char(unsigned char);
  21:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_gotoxy(int,int);
  22:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  23:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define PORT_BASE 0x40021000
  24:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portModer (unsigned volatile long*) PORT_BASE
  25:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portOtyper (unsigned volatile short*) PORT_BASE + 0x04
  26:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portOspeedr (unsigned volatile long*) PORT_BASE + 0x08
  27:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portPupdr (unsigned volatile long*) PORT_BASE + 0x0C
  28:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  29:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portIdrLow (unsigned volatile char*) PORT_BASE + 0x10
  30:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portIdrHigh (unsigned volatile char*) PORT_BASE + 0x11
  31:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portOdrLow (unsigned volatile char*) PORT_BASE + 0x14
  32:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define portOdrHigh (unsigned volatile char*) PORT_BASE + 0x15
  33:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  34:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define STK_CTRL (unsigned volatile long *) 0xE000E010
  35:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define STK_LOAD (unsigned volatile long *) 0xE000E010 + 0x04
  36:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define STK_VAL (unsigned volatile long *) 0xE000E010 + 0x08
  37:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  38:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define B_E 0x40
  39:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define B_SELECT 0x04
  40:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define B_RW 0x02
  41:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** #define B_RS 0x01
  42:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  43:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void startup ( void )
  44:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
  28              		.loc 1 44 0
  29              		.cfi_startproc
  45:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** asm volatile(
  30              		.loc 1 45 0
  31              		.syntax divided
  32              	@ 45 "F:/Programming/C/MOP/aleks/asciidisplayAleks.c" 1
  33 0000 0248     		 LDR R0,=0x2001C000
  34 0002 8546     	 MOV SP,R0
  35 0004 FFF7FEFF 	 BL main
  36 0008 FEE7     	.L1: B .L1
  37              	
  38              	@ 0 "" 2
  46:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  47:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 	" MOV SP,R0\n"
  48:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 	" BL main\n"				/* call main */
  49:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 	".L1: B .L1\n"				/* never return */
  50:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 	) ;
  51:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
  39              		.loc 1 51 0
  40              		.thumb
  41              		.syntax unified
  42 000a C046     		nop
  43              		.cfi_endproc
  44              	.LFE0:
  46              		.text
  47              		.align	2
  48              		.global	delay_250ns
  49              		.code	16
  50              		.thumb_func
  52              	delay_250ns:
  53              	.LFB1:
  52:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  53:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_250ns(void)
  54:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
  54              		.loc 1 54 0
  55              		.cfi_startproc
  56 0000 80B5     		push	{r7, lr}
  57              		.cfi_def_cfa_offset 8
  58              		.cfi_offset 7, -8
  59              		.cfi_offset 14, -4
  60 0002 00AF     		add	r7, sp, #0
  61              		.cfi_def_cfa_register 7
  55:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  56:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(STK_CTRL) = 0;
  62              		.loc 1 56 0
  63 0004 0C4B     		ldr	r3, .L4
  64 0006 0022     		movs	r2, #0
  65 0008 1A60     		str	r2, [r3]
  57:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(STK_LOAD) = ( (168/4) -1 );
  66              		.loc 1 57 0
  67 000a 0C4B     		ldr	r3, .L4+4
  68 000c 2922     		movs	r2, #41
  69 000e 1A60     		str	r2, [r3]
  58:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(STK_VAL) = 0;
  70              		.loc 1 58 0
  71 0010 0B4B     		ldr	r3, .L4+8
  72 0012 0022     		movs	r2, #0
  73 0014 1A60     		str	r2, [r3]
  59:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(STK_CTRL) = 5;
  74              		.loc 1 59 0
  75 0016 084B     		ldr	r3, .L4
  76 0018 0522     		movs	r2, #5
  77 001a 1A60     		str	r2, [r3]
  60:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while( (*STK_CTRL & 0x10000 )== 0 )
  78              		.loc 1 60 0
  79 001c C046     		nop
  80              	.L3:
  81              		.loc 1 60 0 is_stmt 0 discriminator 1
  82 001e 064B     		ldr	r3, .L4
  83 0020 1A68     		ldr	r2, [r3]
  84 0022 8023     		movs	r3, #128
  85 0024 5B02     		lsls	r3, r3, #9
  86 0026 1340     		ands	r3, r2
  87 0028 F9D0     		beq	.L3
  61:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {}
  62:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(STK_CTRL) = 0;
  88              		.loc 1 62 0 is_stmt 1
  89 002a 034B     		ldr	r3, .L4
  90 002c 0022     		movs	r2, #0
  91 002e 1A60     		str	r2, [r3]
  63:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
  92              		.loc 1 63 0
  93 0030 C046     		nop
  94 0032 BD46     		mov	sp, r7
  95              		@ sp needed
  96 0034 80BD     		pop	{r7, pc}
  97              	.L5:
  98 0036 C046     		.align	2
  99              	.L4:
 100 0038 10E000E0 		.word	-536813552
 101 003c 20E000E0 		.word	-536813536
 102 0040 30E000E0 		.word	-536813520
 103              		.cfi_endproc
 104              	.LFE1:
 106              		.align	2
 107              		.global	delay_mikro
 108              		.code	16
 109              		.thumb_func
 111              	delay_mikro:
 112              	.LFB2:
  64:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  65:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_mikro(unsigned int us)
  66:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 113              		.loc 1 66 0
 114              		.cfi_startproc
 115 0044 80B5     		push	{r7, lr}
 116              		.cfi_def_cfa_offset 8
 117              		.cfi_offset 7, -8
 118              		.cfi_offset 14, -4
 119 0046 82B0     		sub	sp, sp, #8
 120              		.cfi_def_cfa_offset 16
 121 0048 00AF     		add	r7, sp, #0
 122              		.cfi_def_cfa_register 7
 123 004a 7860     		str	r0, [r7, #4]
  67:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while (us--) {
 124              		.loc 1 67 0
 125 004c 07E0     		b	.L7
 126              	.L8:
  68:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_250ns();
 127              		.loc 1 68 0
 128 004e FFF7FEFF 		bl	delay_250ns
  69:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_250ns();
 129              		.loc 1 69 0
 130 0052 FFF7FEFF 		bl	delay_250ns
  70:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_250ns();
 131              		.loc 1 70 0
 132 0056 FFF7FEFF 		bl	delay_250ns
  71:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_250ns();
 133              		.loc 1 71 0
 134 005a FFF7FEFF 		bl	delay_250ns
 135              	.L7:
  67:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_250ns();
 136              		.loc 1 67 0
 137 005e 7B68     		ldr	r3, [r7, #4]
 138 0060 5A1E     		subs	r2, r3, #1
 139 0062 7A60     		str	r2, [r7, #4]
 140 0064 002B     		cmp	r3, #0
 141 0066 F2D1     		bne	.L8
  72:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     }
  73:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 142              		.loc 1 73 0
 143 0068 C046     		nop
 144 006a BD46     		mov	sp, r7
 145 006c 02B0     		add	sp, sp, #8
 146              		@ sp needed
 147 006e 80BD     		pop	{r7, pc}
 148              		.cfi_endproc
 149              	.LFE2:
 151              		.align	2
 152              		.global	delay_milli
 153              		.code	16
 154              		.thumb_func
 156              	delay_milli:
 157              	.LFB3:
  74:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  75:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void delay_milli(unsigned int ms)
  76:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 158              		.loc 1 76 0
 159              		.cfi_startproc
 160 0070 80B5     		push	{r7, lr}
 161              		.cfi_def_cfa_offset 8
 162              		.cfi_offset 7, -8
 163              		.cfi_offset 14, -4
 164 0072 82B0     		sub	sp, sp, #8
 165              		.cfi_def_cfa_offset 16
 166 0074 00AF     		add	r7, sp, #0
 167              		.cfi_def_cfa_register 7
 168 0076 7860     		str	r0, [r7, #4]
  77:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     int i;
  78:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while(ms--)
 169              		.loc 1 78 0
 170 0078 04E0     		b	.L10
 171              	.L11:
  79:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {
  80:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         delay_mikro(1000);
 172              		.loc 1 80 0
 173 007a FA23     		movs	r3, #250
 174 007c 9B00     		lsls	r3, r3, #2
 175 007e 1800     		movs	r0, r3
 176 0080 FFF7FEFF 		bl	delay_mikro
 177              	.L10:
  78:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {
 178              		.loc 1 78 0
 179 0084 7B68     		ldr	r3, [r7, #4]
 180 0086 5A1E     		subs	r2, r3, #1
 181 0088 7A60     		str	r2, [r7, #4]
 182 008a 002B     		cmp	r3, #0
 183 008c F5D1     		bne	.L11
  81:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     }
  82:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 184              		.loc 1 82 0
 185 008e C046     		nop
 186 0090 BD46     		mov	sp, r7
 187 0092 02B0     		add	sp, sp, #8
 188              		@ sp needed
 189 0094 80BD     		pop	{r7, pc}
 190              		.cfi_endproc
 191              	.LFE3:
 193 0096 C046     		.align	2
 194              		.global	ascii_ctrl_bit_set
 195              		.code	16
 196              		.thumb_func
 198              	ascii_ctrl_bit_set:
 199              	.LFB4:
  83:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  84:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** ascii_ctrl_bit_set( unsigned char x )
  85:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 200              		.loc 1 85 0
 201              		.cfi_startproc
 202 0098 80B5     		push	{r7, lr}
 203              		.cfi_def_cfa_offset 8
 204              		.cfi_offset 7, -8
 205              		.cfi_offset 14, -4
 206 009a 84B0     		sub	sp, sp, #16
 207              		.cfi_def_cfa_offset 24
 208 009c 00AF     		add	r7, sp, #0
 209              		.cfi_def_cfa_register 7
 210 009e 0200     		movs	r2, r0
 211 00a0 FB1D     		adds	r3, r7, #7
 212 00a2 1A70     		strb	r2, [r3]
  86:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****      unsigned char c;
  87:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****      c = *(portOdrLow);
 213              		.loc 1 87 0
 214 00a4 0C4A     		ldr	r2, .L13
 215 00a6 0F23     		movs	r3, #15
 216 00a8 FB18     		adds	r3, r7, r3
 217 00aa 1278     		ldrb	r2, [r2]
 218 00ac 1A70     		strb	r2, [r3]
  88:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****      c |= ( B_SELECT | x );
 219              		.loc 1 88 0
 220 00ae FA1D     		adds	r2, r7, #7
 221 00b0 0F23     		movs	r3, #15
 222 00b2 FB18     		adds	r3, r7, r3
 223 00b4 1278     		ldrb	r2, [r2]
 224 00b6 1B78     		ldrb	r3, [r3]
 225 00b8 1343     		orrs	r3, r2
 226 00ba DAB2     		uxtb	r2, r3
 227 00bc 0F23     		movs	r3, #15
 228 00be FB18     		adds	r3, r7, r3
 229 00c0 0421     		movs	r1, #4
 230 00c2 0A43     		orrs	r2, r1
 231 00c4 1A70     		strb	r2, [r3]
  89:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****      *(portOdrLow) = c;
 232              		.loc 1 89 0
 233 00c6 044A     		ldr	r2, .L13
 234 00c8 0F23     		movs	r3, #15
 235 00ca FB18     		adds	r3, r7, r3
 236 00cc 1B78     		ldrb	r3, [r3]
 237 00ce 1370     		strb	r3, [r2]
  90:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 238              		.loc 1 90 0
 239 00d0 C046     		nop
 240 00d2 BD46     		mov	sp, r7
 241 00d4 04B0     		add	sp, sp, #16
 242              		@ sp needed
 243 00d6 80BD     		pop	{r7, pc}
 244              	.L14:
 245              		.align	2
 246              	.L13:
 247 00d8 14100240 		.word	1073877012
 248              		.cfi_endproc
 249              	.LFE4:
 251              		.align	2
 252              		.global	ascii_ctrl_bit_clear
 253              		.code	16
 254              		.thumb_func
 256              	ascii_ctrl_bit_clear:
 257              	.LFB5:
  91:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
  92:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_ctrl_bit_clear( unsigned char x )
  93:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** { 
 258              		.loc 1 93 0
 259              		.cfi_startproc
 260 00dc 80B5     		push	{r7, lr}
 261              		.cfi_def_cfa_offset 8
 262              		.cfi_offset 7, -8
 263              		.cfi_offset 14, -4
 264 00de 84B0     		sub	sp, sp, #16
 265              		.cfi_def_cfa_offset 24
 266 00e0 00AF     		add	r7, sp, #0
 267              		.cfi_def_cfa_register 7
 268 00e2 0200     		movs	r2, r0
 269 00e4 FB1D     		adds	r3, r7, #7
 270 00e6 1A70     		strb	r2, [r3]
  94:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     unsigned char c;
  95:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     c = *(portOdrLow);
 271              		.loc 1 95 0
 272 00e8 0F4A     		ldr	r2, .L16
 273 00ea 0F23     		movs	r3, #15
 274 00ec FB18     		adds	r3, r7, r3
 275 00ee 1278     		ldrb	r2, [r2]
 276 00f0 1A70     		strb	r2, [r3]
  96:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     c = B_SELECT | ( c & ~x );
 277              		.loc 1 96 0
 278 00f2 FB1D     		adds	r3, r7, #7
 279 00f4 1B78     		ldrb	r3, [r3]
 280 00f6 DB43     		mvns	r3, r3
 281 00f8 D9B2     		uxtb	r1, r3
 282 00fa 0F23     		movs	r3, #15
 283 00fc FB18     		adds	r3, r7, r3
 284 00fe 1A78     		ldrb	r2, [r3]
 285 0100 0B1C     		adds	r3, r1, #0
 286 0102 1340     		ands	r3, r2
 287 0104 DBB2     		uxtb	r3, r3
 288 0106 1A1C     		adds	r2, r3, #0
 289 0108 0423     		movs	r3, #4
 290 010a 1343     		orrs	r3, r2
 291 010c DAB2     		uxtb	r2, r3
 292 010e 0F23     		movs	r3, #15
 293 0110 FB18     		adds	r3, r7, r3
 294 0112 1A70     		strb	r2, [r3]
  97:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portOdrLow) = c;
 295              		.loc 1 97 0
 296 0114 044A     		ldr	r2, .L16
 297 0116 0F23     		movs	r3, #15
 298 0118 FB18     		adds	r3, r7, r3
 299 011a 1B78     		ldrb	r3, [r3]
 300 011c 1370     		strb	r3, [r2]
  98:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 301              		.loc 1 98 0
 302 011e C046     		nop
 303 0120 BD46     		mov	sp, r7
 304 0122 04B0     		add	sp, sp, #16
 305              		@ sp needed
 306 0124 80BD     		pop	{r7, pc}
 307              	.L17:
 308 0126 C046     		.align	2
 309              	.L16:
 310 0128 14100240 		.word	1073877012
 311              		.cfi_endproc
 312              	.LFE5:
 314              		.align	2
 315              		.global	ascii_write_controller
 316              		.code	16
 317              		.thumb_func
 319              	ascii_write_controller:
 320              	.LFB6:
  99:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 100:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_controller( unsigned char c )
 101:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** { 
 321              		.loc 1 101 0
 322              		.cfi_startproc
 323 012c 80B5     		push	{r7, lr}
 324              		.cfi_def_cfa_offset 8
 325              		.cfi_offset 7, -8
 326              		.cfi_offset 14, -4
 327 012e 82B0     		sub	sp, sp, #8
 328              		.cfi_def_cfa_offset 16
 329 0130 00AF     		add	r7, sp, #0
 330              		.cfi_def_cfa_register 7
 331 0132 0200     		movs	r2, r0
 332 0134 FB1D     		adds	r3, r7, #7
 333 0136 1A70     		strb	r2, [r3]
 102:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set( B_E );
 334              		.loc 1 102 0
 335 0138 4020     		movs	r0, #64
 336 013a FFF7FEFF 		bl	ascii_ctrl_bit_set
 103:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portOdrHigh) = c;
 337              		.loc 1 103 0
 338 013e 064A     		ldr	r2, .L19
 339 0140 FB1D     		adds	r3, r7, #7
 340 0142 1B78     		ldrb	r3, [r3]
 341 0144 1370     		strb	r3, [r2]
 104:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear( B_E );
 342              		.loc 1 104 0
 343 0146 4020     		movs	r0, #64
 344 0148 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 105:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_250ns();
 345              		.loc 1 105 0
 346 014c FFF7FEFF 		bl	delay_250ns
 106:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 347              		.loc 1 106 0
 348 0150 C046     		nop
 349 0152 BD46     		mov	sp, r7
 350 0154 02B0     		add	sp, sp, #8
 351              		@ sp needed
 352 0156 80BD     		pop	{r7, pc}
 353              	.L20:
 354              		.align	2
 355              	.L19:
 356 0158 15100240 		.word	1073877013
 357              		.cfi_endproc
 358              	.LFE6:
 360              		.align	2
 361              		.global	ascii_read_controller
 362              		.code	16
 363              		.thumb_func
 365              	ascii_read_controller:
 366              	.LFB7:
 107:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****    
 108:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_controller( void )
 109:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** { 
 367              		.loc 1 109 0
 368              		.cfi_startproc
 369 015c 80B5     		push	{r7, lr}
 370              		.cfi_def_cfa_offset 8
 371              		.cfi_offset 7, -8
 372              		.cfi_offset 14, -4
 373 015e 82B0     		sub	sp, sp, #8
 374              		.cfi_def_cfa_offset 16
 375 0160 00AF     		add	r7, sp, #0
 376              		.cfi_def_cfa_register 7
 110:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     unsigned char c; 
 111:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set( B_E );
 377              		.loc 1 111 0
 378 0162 4020     		movs	r0, #64
 379 0164 FFF7FEFF 		bl	ascii_ctrl_bit_set
 112:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_250ns();
 380              		.loc 1 112 0
 381 0168 FFF7FEFF 		bl	delay_250ns
 113:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_250ns();
 382              		.loc 1 113 0
 383 016c FFF7FEFF 		bl	delay_250ns
 114:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     c = *portIdrHigh;
 384              		.loc 1 114 0
 385 0170 074B     		ldr	r3, .L23
 386 0172 1B78     		ldrb	r3, [r3]
 387 0174 DAB2     		uxtb	r2, r3
 388 0176 FB1D     		adds	r3, r7, #7
 389 0178 1132     		adds	r2, r2, #17
 390 017a 1A70     		strb	r2, [r3]
 115:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear( B_E );
 391              		.loc 1 115 0
 392 017c 4020     		movs	r0, #64
 393 017e FFF7FEFF 		bl	ascii_ctrl_bit_clear
 116:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     return c;
 394              		.loc 1 116 0
 395 0182 FB1D     		adds	r3, r7, #7
 396 0184 1B78     		ldrb	r3, [r3]
 117:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 397              		.loc 1 117 0
 398 0186 1800     		movs	r0, r3
 399 0188 BD46     		mov	sp, r7
 400 018a 02B0     		add	sp, sp, #8
 401              		@ sp needed
 402 018c 80BD     		pop	{r7, pc}
 403              	.L24:
 404 018e C046     		.align	2
 405              	.L23:
 406 0190 00100240 		.word	1073876992
 407              		.cfi_endproc
 408              	.LFE7:
 410              		.align	2
 411              		.global	ascii_write_cmd
 412              		.code	16
 413              		.thumb_func
 415              	ascii_write_cmd:
 416              	.LFB8:
 118:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 119:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_cmd(unsigned char command)
 120:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 417              		.loc 1 120 0
 418              		.cfi_startproc
 419 0194 80B5     		push	{r7, lr}
 420              		.cfi_def_cfa_offset 8
 421              		.cfi_offset 7, -8
 422              		.cfi_offset 14, -4
 423 0196 82B0     		sub	sp, sp, #8
 424              		.cfi_def_cfa_offset 16
 425 0198 00AF     		add	r7, sp, #0
 426              		.cfi_def_cfa_register 7
 427 019a 0200     		movs	r2, r0
 428 019c FB1D     		adds	r3, r7, #7
 429 019e 1A70     		strb	r2, [r3]
 121:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear(B_RS);
 430              		.loc 1 121 0
 431 01a0 0120     		movs	r0, #1
 432 01a2 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 122:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear(B_RW);
 433              		.loc 1 122 0
 434 01a6 0220     		movs	r0, #2
 435 01a8 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 123:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_controller(command);
 436              		.loc 1 123 0
 437 01ac FB1D     		adds	r3, r7, #7
 438 01ae 1B78     		ldrb	r3, [r3]
 439 01b0 1800     		movs	r0, r3
 440 01b2 FFF7FEFF 		bl	ascii_write_controller
 124:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 441              		.loc 1 124 0
 442 01b6 C046     		nop
 443 01b8 BD46     		mov	sp, r7
 444 01ba 02B0     		add	sp, sp, #8
 445              		@ sp needed
 446 01bc 80BD     		pop	{r7, pc}
 447              		.cfi_endproc
 448              	.LFE8:
 450 01be C046     		.align	2
 451              		.global	ascii_write_data
 452              		.code	16
 453              		.thumb_func
 455              	ascii_write_data:
 456              	.LFB9:
 125:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 126:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_data(unsigned char data)
 127:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 457              		.loc 1 127 0
 458              		.cfi_startproc
 459 01c0 80B5     		push	{r7, lr}
 460              		.cfi_def_cfa_offset 8
 461              		.cfi_offset 7, -8
 462              		.cfi_offset 14, -4
 463 01c2 82B0     		sub	sp, sp, #8
 464              		.cfi_def_cfa_offset 16
 465 01c4 00AF     		add	r7, sp, #0
 466              		.cfi_def_cfa_register 7
 467 01c6 0200     		movs	r2, r0
 468 01c8 FB1D     		adds	r3, r7, #7
 469 01ca 1A70     		strb	r2, [r3]
 128:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set(B_RS);
 470              		.loc 1 128 0
 471 01cc 0120     		movs	r0, #1
 472 01ce FFF7FEFF 		bl	ascii_ctrl_bit_set
 129:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear(B_RW);
 473              		.loc 1 129 0
 474 01d2 0220     		movs	r0, #2
 475 01d4 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 130:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_controller(data);
 476              		.loc 1 130 0
 477 01d8 FB1D     		adds	r3, r7, #7
 478 01da 1B78     		ldrb	r3, [r3]
 479 01dc 1800     		movs	r0, r3
 480 01de FFF7FEFF 		bl	ascii_write_controller
 131:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 481              		.loc 1 131 0
 482 01e2 C046     		nop
 483 01e4 BD46     		mov	sp, r7
 484 01e6 02B0     		add	sp, sp, #8
 485              		@ sp needed
 486 01e8 80BD     		pop	{r7, pc}
 487              		.cfi_endproc
 488              	.LFE9:
 490 01ea C046     		.align	2
 491              		.global	ascii_read_status
 492              		.code	16
 493              		.thumb_func
 495              	ascii_read_status:
 496              	.LFB10:
 132:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 133:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_status(void)
 134:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 497              		.loc 1 134 0
 498              		.cfi_startproc
 499 01ec 90B5     		push	{r4, r7, lr}
 500              		.cfi_def_cfa_offset 12
 501              		.cfi_offset 4, -12
 502              		.cfi_offset 7, -8
 503              		.cfi_offset 14, -4
 504 01ee 83B0     		sub	sp, sp, #12
 505              		.cfi_def_cfa_offset 24
 506 01f0 00AF     		add	r7, sp, #0
 507              		.cfi_def_cfa_register 7
 135:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portModer) &= 0x0000FFFF;
 508              		.loc 1 135 0
 509 01f2 0E4B     		ldr	r3, .L29
 510 01f4 0D4A     		ldr	r2, .L29
 511 01f6 1268     		ldr	r2, [r2]
 512 01f8 1204     		lsls	r2, r2, #16
 513 01fa 120C     		lsrs	r2, r2, #16
 514 01fc 1A60     		str	r2, [r3]
 136:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_clear(B_RS);
 515              		.loc 1 136 0
 516 01fe 0120     		movs	r0, #1
 517 0200 FFF7FEFF 		bl	ascii_ctrl_bit_clear
 137:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set(B_RW);
 518              		.loc 1 137 0
 519 0204 0220     		movs	r0, #2
 520 0206 FFF7FEFF 		bl	ascii_ctrl_bit_set
 138:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     unsigned char rv = ascii_read_controller();
 521              		.loc 1 138 0
 522 020a FC1D     		adds	r4, r7, #7
 523 020c FFF7FEFF 		bl	ascii_read_controller
 524 0210 0300     		movs	r3, r0
 525 0212 2370     		strb	r3, [r4]
 139:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portModer) |= 0x55550000;
 526              		.loc 1 139 0
 527 0214 054B     		ldr	r3, .L29
 528 0216 054A     		ldr	r2, .L29
 529 0218 1268     		ldr	r2, [r2]
 530 021a 0549     		ldr	r1, .L29+4
 531 021c 0A43     		orrs	r2, r1
 532 021e 1A60     		str	r2, [r3]
 140:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     return rv;
 533              		.loc 1 140 0
 534 0220 FB1D     		adds	r3, r7, #7
 535 0222 1B78     		ldrb	r3, [r3]
 141:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 536              		.loc 1 141 0
 537 0224 1800     		movs	r0, r3
 538 0226 BD46     		mov	sp, r7
 539 0228 03B0     		add	sp, sp, #12
 540              		@ sp needed
 541 022a 90BD     		pop	{r4, r7, pc}
 542              	.L30:
 543              		.align	2
 544              	.L29:
 545 022c 00100240 		.word	1073876992
 546 0230 00005555 		.word	1431633920
 547              		.cfi_endproc
 548              	.LFE10:
 550              		.align	2
 551              		.global	ascii_read_data
 552              		.code	16
 553              		.thumb_func
 555              	ascii_read_data:
 556              	.LFB11:
 142:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 143:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** unsigned char ascii_read_data(void)
 144:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 557              		.loc 1 144 0
 558              		.cfi_startproc
 559 0234 90B5     		push	{r4, r7, lr}
 560              		.cfi_def_cfa_offset 12
 561              		.cfi_offset 4, -12
 562              		.cfi_offset 7, -8
 563              		.cfi_offset 14, -4
 564 0236 83B0     		sub	sp, sp, #12
 565              		.cfi_def_cfa_offset 24
 566 0238 00AF     		add	r7, sp, #0
 567              		.cfi_def_cfa_register 7
 145:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portModer) &= 0x0000FFFF;
 568              		.loc 1 145 0
 569 023a 0E4B     		ldr	r3, .L33
 570 023c 0D4A     		ldr	r2, .L33
 571 023e 1268     		ldr	r2, [r2]
 572 0240 1204     		lsls	r2, r2, #16
 573 0242 120C     		lsrs	r2, r2, #16
 574 0244 1A60     		str	r2, [r3]
 146:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set(B_RS);
 575              		.loc 1 146 0
 576 0246 0120     		movs	r0, #1
 577 0248 FFF7FEFF 		bl	ascii_ctrl_bit_set
 147:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_ctrl_bit_set(B_RW);
 578              		.loc 1 147 0
 579 024c 0220     		movs	r0, #2
 580 024e FFF7FEFF 		bl	ascii_ctrl_bit_set
 148:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     unsigned char rv = ascii_read_controller();
 581              		.loc 1 148 0
 582 0252 FC1D     		adds	r4, r7, #7
 583 0254 FFF7FEFF 		bl	ascii_read_controller
 584 0258 0300     		movs	r3, r0
 585 025a 2370     		strb	r3, [r4]
 149:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portModer) |= 0x55550000;
 586              		.loc 1 149 0
 587 025c 054B     		ldr	r3, .L33
 588 025e 054A     		ldr	r2, .L33
 589 0260 1268     		ldr	r2, [r2]
 590 0262 0549     		ldr	r1, .L33+4
 591 0264 0A43     		orrs	r2, r1
 592 0266 1A60     		str	r2, [r3]
 150:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     return rv;
 593              		.loc 1 150 0
 594 0268 FB1D     		adds	r3, r7, #7
 595 026a 1B78     		ldrb	r3, [r3]
 151:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 596              		.loc 1 151 0
 597 026c 1800     		movs	r0, r3
 598 026e BD46     		mov	sp, r7
 599 0270 03B0     		add	sp, sp, #12
 600              		@ sp needed
 601 0272 90BD     		pop	{r4, r7, pc}
 602              	.L34:
 603              		.align	2
 604              	.L33:
 605 0274 00100240 		.word	1073876992
 606 0278 00005555 		.word	1431633920
 607              		.cfi_endproc
 608              	.LFE11:
 610              		.align	2
 611              		.global	ascii_init
 612              		.code	16
 613              		.thumb_func
 615              	ascii_init:
 616              	.LFB12:
 152:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 153:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_init(void)
 154:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 617              		.loc 1 154 0
 618              		.cfi_startproc
 619 027c 80B5     		push	{r7, lr}
 620              		.cfi_def_cfa_offset 8
 621              		.cfi_offset 7, -8
 622              		.cfi_offset 14, -4
 623 027e 00AF     		add	r7, sp, #0
 624              		.cfi_def_cfa_register 7
 155:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while( (ascii_read_status() & 0x80)== 0x80 )
 625              		.loc 1 155 0
 626 0280 C046     		nop
 627              	.L36:
 628              		.loc 1 155 0 is_stmt 0 discriminator 1
 629 0282 FFF7FEFF 		bl	ascii_read_status
 630 0286 0300     		movs	r3, r0
 631 0288 DBB2     		uxtb	r3, r3
 632 028a 5BB2     		sxtb	r3, r3
 633 028c 002B     		cmp	r3, #0
 634 028e F8DB     		blt	.L36
 156:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {}
 157:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(8);
 635              		.loc 1 157 0 is_stmt 1
 636 0290 0820     		movs	r0, #8
 637 0292 FFF7FEFF 		bl	delay_mikro
 158:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_cmd(0b0000111000);
 638              		.loc 1 158 0
 639 0296 3820     		movs	r0, #56
 640 0298 FFF7FEFF 		bl	ascii_write_cmd
 159:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(40);
 641              		.loc 1 159 0
 642 029c 2820     		movs	r0, #40
 643 029e FFF7FEFF 		bl	delay_mikro
 160:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_cmd(0b0000001110);
 644              		.loc 1 160 0
 645 02a2 0E20     		movs	r0, #14
 646 02a4 FFF7FEFF 		bl	ascii_write_cmd
 161:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(40);
 647              		.loc 1 161 0
 648 02a8 2820     		movs	r0, #40
 649 02aa FFF7FEFF 		bl	delay_mikro
 162:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_cmd(1);
 650              		.loc 1 162 0
 651 02ae 0120     		movs	r0, #1
 652 02b0 FFF7FEFF 		bl	ascii_write_cmd
 163:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_milli(2);
 653              		.loc 1 163 0
 654 02b4 0220     		movs	r0, #2
 655 02b6 FFF7FEFF 		bl	delay_milli
 164:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_cmd(0b0000000110);
 656              		.loc 1 164 0
 657 02ba 0620     		movs	r0, #6
 658 02bc FFF7FEFF 		bl	ascii_write_cmd
 165:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(40);
 659              		.loc 1 165 0
 660 02c0 2820     		movs	r0, #40
 661 02c2 FFF7FEFF 		bl	delay_mikro
 166:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 662              		.loc 1 166 0
 663 02c6 C046     		nop
 664 02c8 BD46     		mov	sp, r7
 665              		@ sp needed
 666 02ca 80BD     		pop	{r7, pc}
 667              		.cfi_endproc
 668              	.LFE12:
 670              		.align	2
 671              		.global	init_app
 672              		.code	16
 673              		.thumb_func
 675              	init_app:
 676              	.LFB13:
 167:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 168:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void init_app(void)
 169:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 677              		.loc 1 169 0
 678              		.cfi_startproc
 679 02cc 80B5     		push	{r7, lr}
 680              		.cfi_def_cfa_offset 8
 681              		.cfi_offset 7, -8
 682              		.cfi_offset 14, -4
 683 02ce 00AF     		add	r7, sp, #0
 684              		.cfi_def_cfa_register 7
 170:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     *(portModer) = 0x55555555;
 685              		.loc 1 170 0
 686 02d0 024B     		ldr	r3, .L38
 687 02d2 034A     		ldr	r2, .L38+4
 688 02d4 1A60     		str	r2, [r3]
 171:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 689              		.loc 1 171 0
 690 02d6 C046     		nop
 691 02d8 BD46     		mov	sp, r7
 692              		@ sp needed
 693 02da 80BD     		pop	{r7, pc}
 694              	.L39:
 695              		.align	2
 696              	.L38:
 697 02dc 00100240 		.word	1073876992
 698 02e0 55555555 		.word	1431655765
 699              		.cfi_endproc
 700              	.LFE13:
 702              		.align	2
 703              		.global	ascii_write_char
 704              		.code	16
 705              		.thumb_func
 707              	ascii_write_char:
 708              	.LFB14:
 172:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 173:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_write_char(unsigned char c)
 174:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 709              		.loc 1 174 0
 710              		.cfi_startproc
 711 02e4 80B5     		push	{r7, lr}
 712              		.cfi_def_cfa_offset 8
 713              		.cfi_offset 7, -8
 714              		.cfi_offset 14, -4
 715 02e6 82B0     		sub	sp, sp, #8
 716              		.cfi_def_cfa_offset 16
 717 02e8 00AF     		add	r7, sp, #0
 718              		.cfi_def_cfa_register 7
 719 02ea 0200     		movs	r2, r0
 720 02ec FB1D     		adds	r3, r7, #7
 721 02ee 1A70     		strb	r2, [r3]
 175:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while ((ascii_read_status() & 0x80) == 0x80)
 722              		.loc 1 175 0
 723 02f0 C046     		nop
 724              	.L41:
 725              		.loc 1 175 0 is_stmt 0 discriminator 1
 726 02f2 FFF7FEFF 		bl	ascii_read_status
 727 02f6 0300     		movs	r3, r0
 728 02f8 DBB2     		uxtb	r3, r3
 729 02fa 5BB2     		sxtb	r3, r3
 730 02fc 002B     		cmp	r3, #0
 731 02fe F8DB     		blt	.L41
 176:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {}
 177:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(8);
 732              		.loc 1 177 0 is_stmt 1
 733 0300 0820     		movs	r0, #8
 734 0302 FFF7FEFF 		bl	delay_mikro
 178:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_data(c);
 735              		.loc 1 178 0
 736 0306 FB1D     		adds	r3, r7, #7
 737 0308 1B78     		ldrb	r3, [r3]
 738 030a 1800     		movs	r0, r3
 739 030c FFF7FEFF 		bl	ascii_write_data
 179:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     delay_mikro(43);
 740              		.loc 1 179 0
 741 0310 2B20     		movs	r0, #43
 742 0312 FFF7FEFF 		bl	delay_mikro
 180:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 743              		.loc 1 180 0
 744 0316 C046     		nop
 745 0318 BD46     		mov	sp, r7
 746 031a 02B0     		add	sp, sp, #8
 747              		@ sp needed
 748 031c 80BD     		pop	{r7, pc}
 749              		.cfi_endproc
 750              	.LFE14:
 752 031e C046     		.align	2
 753              		.global	ascii_gotoxy
 754              		.code	16
 755              		.thumb_func
 757              	ascii_gotoxy:
 758              	.LFB15:
 181:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** 
 182:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** void ascii_gotoxy(int x, int y)
 183:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 759              		.loc 1 183 0
 760              		.cfi_startproc
 761 0320 80B5     		push	{r7, lr}
 762              		.cfi_def_cfa_offset 8
 763              		.cfi_offset 7, -8
 764              		.cfi_offset 14, -4
 765 0322 84B0     		sub	sp, sp, #16
 766              		.cfi_def_cfa_offset 24
 767 0324 00AF     		add	r7, sp, #0
 768              		.cfi_def_cfa_register 7
 769 0326 7860     		str	r0, [r7, #4]
 770 0328 3960     		str	r1, [r7]
 184:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     int address = x - 1;
 771              		.loc 1 184 0
 772 032a 7B68     		ldr	r3, [r7, #4]
 773 032c 013B     		subs	r3, r3, #1
 774 032e FB60     		str	r3, [r7, #12]
 185:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     if (y == 2)
 775              		.loc 1 185 0
 776 0330 3B68     		ldr	r3, [r7]
 777 0332 022B     		cmp	r3, #2
 778 0334 02D1     		bne	.L43
 186:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     {
 187:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         address = address + 0x40;
 779              		.loc 1 187 0
 780 0336 FB68     		ldr	r3, [r7, #12]
 781 0338 4033     		adds	r3, r3, #64
 782 033a FB60     		str	r3, [r7, #12]
 783              	.L43:
 188:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     }
 189:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_write_cmd(0x80 | address);
 784              		.loc 1 189 0
 785 033c FB68     		ldr	r3, [r7, #12]
 786 033e DBB2     		uxtb	r3, r3
 787 0340 1A1C     		adds	r2, r3, #0
 788 0342 8023     		movs	r3, #128
 789 0344 5B42     		rsbs	r3, r3, #0
 790 0346 1343     		orrs	r3, r2
 791 0348 DBB2     		uxtb	r3, r3
 792 034a DBB2     		uxtb	r3, r3
 793 034c 1800     		movs	r0, r3
 794 034e FFF7FEFF 		bl	ascii_write_cmd
 190:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 795              		.loc 1 190 0
 796 0352 C046     		nop
 797 0354 BD46     		mov	sp, r7
 798 0356 04B0     		add	sp, sp, #16
 799              		@ sp needed
 800 0358 80BD     		pop	{r7, pc}
 801              		.cfi_endproc
 802              	.LFE15:
 804              		.section	.rodata
 805              		.align	2
 806              	.LC0:
 807 0000 416C656B 		.ascii	"Aleks gillar\000"
 807      73206769 
 807      6C6C6172 
 807      00
 808 000d 000000   		.align	2
 809              	.LC2:
 810 0010 666C7566 		.ascii	"fluffiga kattungar :)\000"
 810      66696761 
 810      206B6174 
 810      74756E67 
 810      6172203A 
 811              		.text
 812 035a C046     		.align	2
 813              		.global	main
 814              		.code	16
 815              		.thumb_func
 817              	main:
 818              	.LFB16:
 191:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****  
 192:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** int main(int argc, char **argv)
 193:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** {
 819              		.loc 1 193 0
 820              		.cfi_startproc
 821 035c 90B5     		push	{r4, r7, lr}
 822              		.cfi_def_cfa_offset 12
 823              		.cfi_offset 4, -12
 824              		.cfi_offset 7, -8
 825              		.cfi_offset 14, -4
 826 035e 8FB0     		sub	sp, sp, #60
 827              		.cfi_def_cfa_offset 72
 828 0360 00AF     		add	r7, sp, #0
 829              		.cfi_def_cfa_register 7
 830 0362 7860     		str	r0, [r7, #4]
 831 0364 3960     		str	r1, [r7]
 194:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     char *s;
 195:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     char test1[] = "Aleks gillar";
 832              		.loc 1 195 0
 833 0366 2423     		movs	r3, #36
 834 0368 FB18     		adds	r3, r7, r3
 835 036a 1E4A     		ldr	r2, .L50
 836 036c 13CA     		ldmia	r2!, {r0, r1, r4}
 837 036e 13C3     		stmia	r3!, {r0, r1, r4}
 838 0370 1278     		ldrb	r2, [r2]
 839 0372 1A70     		strb	r2, [r3]
 196:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     char test2[] = "fluffiga kattungar :)";
 840              		.loc 1 196 0
 841 0374 0C23     		movs	r3, #12
 842 0376 FB18     		adds	r3, r7, r3
 843 0378 1B4A     		ldr	r2, .L50+4
 844 037a 13CA     		ldmia	r2!, {r0, r1, r4}
 845 037c 13C3     		stmia	r3!, {r0, r1, r4}
 846 037e 03CA     		ldmia	r2!, {r0, r1}
 847 0380 03C3     		stmia	r3!, {r0, r1}
 848 0382 1288     		ldrh	r2, [r2]
 849 0384 1A80     		strh	r2, [r3]
 197:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     
 198:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     init_app();
 850              		.loc 1 198 0
 851 0386 FFF7FEFF 		bl	init_app
 199:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_init();
 852              		.loc 1 199 0
 853 038a FFF7FEFF 		bl	ascii_init
 200:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_gotoxy(1,1);
 854              		.loc 1 200 0
 855 038e 0121     		movs	r1, #1
 856 0390 0120     		movs	r0, #1
 857 0392 FFF7FEFF 		bl	ascii_gotoxy
 201:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     s = test1;
 858              		.loc 1 201 0
 859 0396 2423     		movs	r3, #36
 860 0398 FB18     		adds	r3, r7, r3
 861 039a 7B63     		str	r3, [r7, #52]
 202:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while(*s)
 862              		.loc 1 202 0
 863 039c 06E0     		b	.L45
 864              	.L46:
 203:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         ascii_write_char(*s++);
 865              		.loc 1 203 0
 866 039e 7B6B     		ldr	r3, [r7, #52]
 867 03a0 5A1C     		adds	r2, r3, #1
 868 03a2 7A63     		str	r2, [r7, #52]
 869 03a4 1B78     		ldrb	r3, [r3]
 870 03a6 1800     		movs	r0, r3
 871 03a8 FFF7FEFF 		bl	ascii_write_char
 872              	.L45:
 202:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while(*s)
 873              		.loc 1 202 0
 874 03ac 7B6B     		ldr	r3, [r7, #52]
 875 03ae 1B78     		ldrb	r3, [r3]
 876 03b0 002B     		cmp	r3, #0
 877 03b2 F4D1     		bne	.L46
 204:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     ascii_gotoxy(1,2);
 878              		.loc 1 204 0
 879 03b4 0221     		movs	r1, #2
 880 03b6 0120     		movs	r0, #1
 881 03b8 FFF7FEFF 		bl	ascii_gotoxy
 205:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     s = test2;
 882              		.loc 1 205 0
 883 03bc 0C23     		movs	r3, #12
 884 03be FB18     		adds	r3, r7, r3
 885 03c0 7B63     		str	r3, [r7, #52]
 206:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while(*s)
 886              		.loc 1 206 0
 887 03c2 06E0     		b	.L47
 888              	.L48:
 207:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****         ascii_write_char(*s++);
 889              		.loc 1 207 0
 890 03c4 7B6B     		ldr	r3, [r7, #52]
 891 03c6 5A1C     		adds	r2, r3, #1
 892 03c8 7A63     		str	r2, [r7, #52]
 893 03ca 1B78     		ldrb	r3, [r3]
 894 03cc 1800     		movs	r0, r3
 895 03ce FFF7FEFF 		bl	ascii_write_char
 896              	.L47:
 206:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     while(*s)
 897              		.loc 1 206 0
 898 03d2 7B6B     		ldr	r3, [r7, #52]
 899 03d4 1B78     		ldrb	r3, [r3]
 900 03d6 002B     		cmp	r3, #0
 901 03d8 F4D1     		bne	.L48
 208:F:/Programming/C/MOP/aleks\asciidisplayAleks.c ****     return 0;
 902              		.loc 1 208 0
 903 03da 0023     		movs	r3, #0
 209:F:/Programming/C/MOP/aleks\asciidisplayAleks.c **** }
 904              		.loc 1 209 0
 905 03dc 1800     		movs	r0, r3
 906 03de BD46     		mov	sp, r7
 907 03e0 0FB0     		add	sp, sp, #60
 908              		@ sp needed
 909 03e2 90BD     		pop	{r4, r7, pc}
 910              	.L51:
 911              		.align	2
 912              	.L50:
 913 03e4 00000000 		.word	.LC0
 914 03e8 10000000 		.word	.LC2
 915              		.cfi_endproc
 916              	.LFE16:
 918              	.Letext0:
