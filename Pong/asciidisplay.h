#ifndef ASCIIDISPLAY_H
#define ASCIIDISPLAY_H

#define GPIO_MODER (unsigned volatile long*) 0x40021000
#define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
#define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
#define GPIO_IDR_LOW (unsigned volatile char*) 0x40021010
#define GPIO_IDR_HIGH (unsigned volatile char*) 0x40021011
#define GPIO_ODR_LOW (unsigned volatile char*) 0x40021014
#define GPIO_ODR_HIGH (unsigned volatile char*) 0x40021015


#define B_E 0x40
#define B_SELECT 0x4
#define B_RW 0x2
#define B_RS 0x1

#define STK_CTRL (unsigned volatile long*) 0xE000E010
#define STK_LOAD (unsigned volatile long*) 0xE000E014
#define STK_VAL (unsigned volatile long*) 0xE000E018
#define STK_CALIB (unsigned volatile long*) 0xE000E01C

void ascii_init(void);

//Set set bits in x to 1
void ascii_ctrl_bit_set(unsigned char);

//Set set bits in x to 0
void ascii_ctrl_bit_clear(unsigned char);

void ascii_write_controller(unsigned char);

unsigned char ascii_read_controller(void);

void ascii_write_cmd(unsigned char);

void ascii_write_data(unsigned char);

unsigned char ascii_read_status(void);

unsigned char ascii_read_data(void);

void ascii_command(unsigned char, unsigned int);

void ascii_gotoPos(int, int);

void ascii_write_char(unsigned char);

void writeScoreToAscii(char*);

void writeScore(int player1, int player2);

void toCharArray(int value, int nDigits, char result[nDigits]);

int countDigits(int);

#endif //asciidisplay