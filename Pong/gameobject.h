#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#define MAX_POINTS 50
typedef struct tGeometry
{
	int numpoints, width, height;
	unsigned char points[MAX_POINTS][2];
} GEOMETRY,*PGEOMETRY;



typedef struct TOBJ
{
	PGEOMETRY geo;
	int dirx,diry;
	int posx;
    int posy;
	void (* move) (struct TOBJ *);
	void (* setDir) (struct TOBJ *, int, int);
    void (* setPos) (struct TOBJ *, int, int);
} OBJECT, *POBJECT;

void setDir(POBJECT,int,int);

void move(POBJECT);
void setPos(POBJECT, int, int);

#endif //GAMEOBJECT_H