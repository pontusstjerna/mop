   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"main.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "C:/Users/pontu/Documents/Programmering/C/mop/Pong/main.c"
   1:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #include "keypad.h"
   2:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #include "graphicdisplay.h"
   3:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #include "asciidisplay.h"
   4:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #include "PongGame.h"
   5:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
   6:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** //Port D
   7:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_MODER (unsigned volatile long*) 0x40020C00
   8:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_OTYPER (unsigned volatile short*) 0x40020C04
   9:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_OSPEEDR (unsigned volatile long*) 0x40020C08
  10:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_PUPDR (unsigned volatile long*) 0x40020C0C
  11:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_IDR_PLAYER1 (vbyte*) 0x40020C10
  12:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_IDR_PLAYER2 (vbyte*) 0x40020C11
  13:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_ODR_PLAYER1 (vbyte*) 0x40020C14
  14:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** #define GPIO_D_ODR_PLAYER2 (vbyte*) 0x40020C15
  15:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  16:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void moveRackets();
  17:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  18:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  19:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  20:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  21:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void startup ( void )
  22:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** {
  28              		.loc 1 22 0
  29              		.cfi_startproc
  23:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** asm volatile(
  30              		.loc 1 23 0
  31              		.syntax divided
  32              	@ 23 "C:/Users/pontu/Documents/Programmering/C/mop/Pong/main.c" 1
  33 0000 0448     		 LDR R0, =0x40023830
  34 0002 1821     	 MOV R1, #0x18
  35 0004 0160     	 STR R1, [R0]
  36 0006 0448     	 LDR R0,=0x2001C000
  37 0008 8546     	 MOV SP,R0
  38 000a FFF7FEFF 	 BL main
  39 000e FEE7     	.L1: B .L1
  40              	
  41              	@ 0 "" 2
  24:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" LDR R0, =0x40023830\n"
  25:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" MOV R1, #0x18\n"
  26:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" STR R1, [R0]\n"
  27:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  28:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" MOV SP,R0\n"
  29:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	" BL main\n"				/* call main */
  30:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	".L1: B .L1\n"				/* never return */
  31:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	) ;
  32:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** }
  42              		.loc 1 32 0
  43              		.thumb
  44              		.syntax unified
  45 0010 C046     		nop
  46              		.cfi_endproc
  47              	.LFE0:
  49 0012 0000     		.text
  50              		.align	2
  51              		.global	game_init
  52              		.code	16
  53              		.thumb_func
  55              	game_init:
  56              	.LFB1:
  33:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  34:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void game_init()
  35:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** {
  57              		.loc 1 35 0
  58              		.cfi_startproc
  59 0000 80B5     		push	{r7, lr}
  60              		.cfi_def_cfa_offset 8
  61              		.cfi_offset 7, -8
  62              		.cfi_offset 14, -4
  63 0002 00AF     		add	r7, sp, #0
  64              		.cfi_def_cfa_register 7
  36:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	//Includes setting upp GPIO_E
  37:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	graphic_initialize();
  65              		.loc 1 37 0
  66 0004 FFF7FEFF 		bl	graphic_initialize
  38:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	
  39:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	//Use borth high and low port D for 2 keypads
  40:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	*(GPIO_D_MODER) = 0x55005500;
  67              		.loc 1 40 0
  68 0008 044B     		ldr	r3, .L3
  69 000a 054A     		ldr	r2, .L3+4
  70 000c 1A60     		str	r2, [r3]
  41:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	*(GPIO_D_PUPDR) = 0x00AA00AA;
  71              		.loc 1 41 0
  72 000e 054B     		ldr	r3, .L3+8
  73 0010 054A     		ldr	r2, .L3+12
  74 0012 1A60     		str	r2, [r3]
  42:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** }
  75              		.loc 1 42 0
  76 0014 C046     		nop
  77 0016 BD46     		mov	sp, r7
  78              		@ sp needed
  79 0018 80BD     		pop	{r7, pc}
  80              	.L4:
  81 001a C046     		.align	2
  82              	.L3:
  83 001c 000C0240 		.word	1073875968
  84 0020 00550055 		.word	1426085120
  85 0024 0C0C0240 		.word	1073875980
  86 0028 AA00AA00 		.word	11141290
  87              		.cfi_endproc
  88              	.LFE1:
  90              		.global	player1ScoreMain
  91              		.bss
  92              		.align	2
  95              	player1ScoreMain:
  96 0000 00000000 		.space	4
  97              		.global	player2ScoreMain
  98              		.align	2
 101              	player2ScoreMain:
 102 0004 00000000 		.space	4
 103              		.text
 104              		.align	2
 105              		.global	main
 106              		.code	16
 107              		.thumb_func
 109              	main:
 110              	.LFB2:
  43:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  44:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** int player1ScoreMain = 0;
  45:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** int player2ScoreMain = 0;
  46:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  47:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void main(void)
  48:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** {
 111              		.loc 1 48 0
 112              		.cfi_startproc
 113 002c F0B5     		push	{r4, r5, r6, r7, lr}
 114              		.cfi_def_cfa_offset 20
 115              		.cfi_offset 4, -20
 116              		.cfi_offset 5, -16
 117              		.cfi_offset 6, -12
 118              		.cfi_offset 7, -8
 119              		.cfi_offset 14, -4
 120 002e 89B0     		sub	sp, sp, #36
 121              		.cfi_def_cfa_offset 56
 122 0030 00AF     		add	r7, sp, #0
 123              		.cfi_def_cfa_register 7
  49:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	game_init();
 124              		.loc 1 49 0
 125 0032 FFF7FEFF 		bl	game_init
  50:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     player1ScoreMain = get_player1_score();
 126              		.loc 1 50 0
 127 0036 FFF7FEFF 		bl	get_player1_score
 128 003a 0200     		movs	r2, r0
 129 003c 3A4B     		ldr	r3, .L9
 130 003e 1A60     		str	r2, [r3]
  51:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     player2ScoreMain = get_player2_score();
 131              		.loc 1 51 0
 132 0040 FFF7FEFF 		bl	get_player2_score
 133 0044 0200     		movs	r2, r0
 134 0046 394B     		ldr	r3, .L9+4
 135 0048 1A60     		str	r2, [r3]
  52:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     writeScore(player1ScoreMain,player2ScoreMain);
 136              		.loc 1 52 0
 137 004a 374B     		ldr	r3, .L9
 138 004c 1A68     		ldr	r2, [r3]
 139 004e 374B     		ldr	r3, .L9+4
 140 0050 1B68     		ldr	r3, [r3]
 141 0052 1900     		movs	r1, r3
 142 0054 1000     		movs	r0, r2
 143 0056 FFF7FEFF 		bl	writeScore
  53:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     unsigned char pixels[get_numberof_pixels()][2];
 144              		.loc 1 53 0
 145 005a FFF7FEFF 		bl	get_numberof_pixels
 146 005e 0300     		movs	r3, r0
 147 0060 5A1E     		subs	r2, r3, #1
 148 0062 FA61     		str	r2, [r7, #28]
 149 0064 1A00     		movs	r2, r3
 150 0066 BA60     		str	r2, [r7, #8]
 151 0068 0022     		movs	r2, #0
 152 006a FA60     		str	r2, [r7, #12]
 153 006c B868     		ldr	r0, [r7, #8]
 154 006e F968     		ldr	r1, [r7, #12]
 155 0070 0200     		movs	r2, r0
 156 0072 120F     		lsrs	r2, r2, #28
 157 0074 0E00     		movs	r6, r1
 158 0076 3601     		lsls	r6, r6, #4
 159 0078 7E61     		str	r6, [r7, #20]
 160 007a 7E69     		ldr	r6, [r7, #20]
 161 007c 1643     		orrs	r6, r2
 162 007e 7E61     		str	r6, [r7, #20]
 163 0080 0200     		movs	r2, r0
 164 0082 1201     		lsls	r2, r2, #4
 165 0084 3A61     		str	r2, [r7, #16]
 166 0086 1A00     		movs	r2, r3
 167 0088 3A60     		str	r2, [r7]
 168 008a 0022     		movs	r2, #0
 169 008c 7A60     		str	r2, [r7, #4]
 170 008e 3868     		ldr	r0, [r7]
 171 0090 7968     		ldr	r1, [r7, #4]
 172 0092 0200     		movs	r2, r0
 173 0094 120F     		lsrs	r2, r2, #28
 174 0096 0E00     		movs	r6, r1
 175 0098 3501     		lsls	r5, r6, #4
 176 009a 1543     		orrs	r5, r2
 177 009c 0200     		movs	r2, r0
 178 009e 1401     		lsls	r4, r2, #4
 179 00a0 5B00     		lsls	r3, r3, #1
 180 00a2 0733     		adds	r3, r3, #7
 181 00a4 DB08     		lsrs	r3, r3, #3
 182 00a6 DB00     		lsls	r3, r3, #3
 183 00a8 6A46     		mov	r2, sp
 184 00aa D31A     		subs	r3, r2, r3
 185 00ac 9D46     		mov	sp, r3
 186 00ae 6B46     		mov	r3, sp
 187 00b0 0033     		adds	r3, r3, #0
 188 00b2 BB61     		str	r3, [r7, #24]
 189              	.L8:
 190              	.LBB2:
  54:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	while(1)
  55:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	{
  56:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         //Clear
  57:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         clearPixels(get_numberof_pixels(), 2, pixels);
 191              		.loc 1 57 0
 192 00b4 FFF7FEFF 		bl	get_numberof_pixels
 193 00b8 BB69     		ldr	r3, [r7, #24]
 194 00ba 1A00     		movs	r2, r3
 195 00bc 0221     		movs	r1, #2
 196 00be FFF7FEFF 		bl	clearPixels
  58:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         update();
 197              		.loc 1 58 0
 198 00c2 FFF7FEFF 		bl	update
  59:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         moveRackets();
 199              		.loc 1 59 0
 200 00c6 FFF7FEFF 		bl	moveRackets
  60:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         get_pixels(get_numberof_pixels(), 2, pixels);
 201              		.loc 1 60 0
 202 00ca FFF7FEFF 		bl	get_numberof_pixels
 203 00ce BB69     		ldr	r3, [r7, #24]
 204 00d0 1A00     		movs	r2, r3
 205 00d2 0221     		movs	r1, #2
 206 00d4 FFF7FEFF 		bl	get_pixels
  61:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         drawPixels(get_numberof_pixels(), 2, pixels);
 207              		.loc 1 61 0
 208 00d8 FFF7FEFF 		bl	get_numberof_pixels
 209 00dc BB69     		ldr	r3, [r7, #24]
 210 00de 1A00     		movs	r2, r3
 211 00e0 0221     		movs	r1, #2
 212 00e2 FFF7FEFF 		bl	drawPixels
  62:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         if(player1ScoreMain != get_player1_score() || player2ScoreMain != get_player2_score())
 213              		.loc 1 62 0
 214 00e6 FFF7FEFF 		bl	get_player1_score
 215 00ea 021E     		subs	r2, r0, #0
 216 00ec 0E4B     		ldr	r3, .L9
 217 00ee 1B68     		ldr	r3, [r3]
 218 00f0 9A42     		cmp	r2, r3
 219 00f2 06D1     		bne	.L6
 220              		.loc 1 62 0 is_stmt 0 discriminator 1
 221 00f4 FFF7FEFF 		bl	get_player2_score
 222 00f8 021E     		subs	r2, r0, #0
 223 00fa 0C4B     		ldr	r3, .L9+4
 224 00fc 1B68     		ldr	r3, [r3]
 225 00fe 9A42     		cmp	r2, r3
 226 0100 D8D0     		beq	.L8
 227              	.L6:
  63:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         {
  64:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****             player1ScoreMain = get_player1_score();
 228              		.loc 1 64 0 is_stmt 1
 229 0102 FFF7FEFF 		bl	get_player1_score
 230 0106 0200     		movs	r2, r0
 231 0108 074B     		ldr	r3, .L9
 232 010a 1A60     		str	r2, [r3]
  65:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****             player2ScoreMain = get_player2_score();
 233              		.loc 1 65 0
 234 010c FFF7FEFF 		bl	get_player2_score
 235 0110 0200     		movs	r2, r0
 236 0112 064B     		ldr	r3, .L9+4
 237 0114 1A60     		str	r2, [r3]
  66:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****             writeScore(player1ScoreMain, player2ScoreMain);
 238              		.loc 1 66 0
 239 0116 044B     		ldr	r3, .L9
 240 0118 1A68     		ldr	r2, [r3]
 241 011a 044B     		ldr	r3, .L9+4
 242 011c 1B68     		ldr	r3, [r3]
 243 011e 1900     		movs	r1, r3
 244 0120 1000     		movs	r0, r2
 245 0122 FFF7FEFF 		bl	writeScore
 246              	.LBE2:
  67:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         }
  68:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****             
  69:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 	}
 247              		.loc 1 69 0
 248 0126 C5E7     		b	.L8
 249              	.L10:
 250              		.align	2
 251              	.L9:
 252 0128 00000000 		.word	player1ScoreMain
 253 012c 00000000 		.word	player2ScoreMain
 254              		.cfi_endproc
 255              	.LFE2:
 257              		.align	2
 258              		.global	moveRackets
 259              		.code	16
 260              		.thumb_func
 262              	moveRackets:
 263              	.LFB3:
  70:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** }
  71:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** 
  72:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** void moveRackets()
  73:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** {
 264              		.loc 1 73 0
 265              		.cfi_startproc
 266 0130 90B5     		push	{r4, r7, lr}
 267              		.cfi_def_cfa_offset 12
 268              		.cfi_offset 4, -12
 269              		.cfi_offset 7, -8
 270              		.cfi_offset 14, -4
 271 0132 83B0     		sub	sp, sp, #12
 272              		.cfi_def_cfa_offset 24
 273 0134 00AF     		add	r7, sp, #0
 274              		.cfi_def_cfa_register 7
  74:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     char player1 = get_key_down(GPIO_D_IDR_PLAYER1, GPIO_D_ODR_PLAYER1);
 275              		.loc 1 74 0
 276 0136 FC1D     		adds	r4, r7, #7
 277 0138 1E4A     		ldr	r2, .L19
 278 013a 1F4B     		ldr	r3, .L19+4
 279 013c 1100     		movs	r1, r2
 280 013e 1800     		movs	r0, r3
 281 0140 FFF7FEFF 		bl	get_key_down
 282 0144 0300     		movs	r3, r0
 283 0146 2370     		strb	r3, [r4]
  75:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     char player2 = get_key_down(GPIO_D_IDR_PLAYER2, GPIO_D_ODR_PLAYER2);
 284              		.loc 1 75 0
 285 0148 BC1D     		adds	r4, r7, #6
 286 014a 1C4A     		ldr	r2, .L19+8
 287 014c 1C4B     		ldr	r3, .L19+12
 288 014e 1100     		movs	r1, r2
 289 0150 1800     		movs	r0, r3
 290 0152 FFF7FEFF 		bl	get_key_down
 291 0156 0300     		movs	r3, r0
 292 0158 2370     		strb	r3, [r4]
  76:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     
  77:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     if(player1 == '2')
 293              		.loc 1 77 0
 294 015a FB1D     		adds	r3, r7, #7
 295 015c 1B78     		ldrb	r3, [r3]
 296 015e 322B     		cmp	r3, #50
 297 0160 05D1     		bne	.L12
  78:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
  79:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         move_racket1(up);
 298              		.loc 1 79 0
 299 0162 0223     		movs	r3, #2
 300 0164 5B42     		rsbs	r3, r3, #0
 301 0166 1800     		movs	r0, r3
 302 0168 FFF7FEFF 		bl	move_racket1
 303 016c 09E0     		b	.L13
 304              	.L12:
  80:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
  81:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     else if(player1 == '8')
 305              		.loc 1 81 0
 306 016e FB1D     		adds	r3, r7, #7
 307 0170 1B78     		ldrb	r3, [r3]
 308 0172 382B     		cmp	r3, #56
 309 0174 03D1     		bne	.L14
  82:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
  83:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         move_racket1(down);
 310              		.loc 1 83 0
 311 0176 0220     		movs	r0, #2
 312 0178 FFF7FEFF 		bl	move_racket1
 313 017c 01E0     		b	.L13
 314              	.L14:
  84:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
  85:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     else
  86:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
  87:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****        stop_racket1(); 
 315              		.loc 1 87 0
 316 017e FFF7FEFF 		bl	stop_racket1
 317              	.L13:
  88:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
  89:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     
  90:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     if(player2 == '2')
 318              		.loc 1 90 0
 319 0182 BB1D     		adds	r3, r7, #6
 320 0184 1B78     		ldrb	r3, [r3]
 321 0186 322B     		cmp	r3, #50
 322 0188 05D1     		bne	.L15
  91:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
  92:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         move_racket2(up);
 323              		.loc 1 92 0
 324 018a 0223     		movs	r3, #2
 325 018c 5B42     		rsbs	r3, r3, #0
 326 018e 1800     		movs	r0, r3
 327 0190 FFF7FEFF 		bl	move_racket2
  93:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
  94:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     else if(player2 == '8')
  95:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
  96:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****         move_racket2(down);
  97:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
  98:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     else
  99:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
 100:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****        stop_racket2(); 
 101:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
 102:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c **** }
 328              		.loc 1 102 0
 329 0194 09E0     		b	.L18
 330              	.L15:
  94:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     {
 331              		.loc 1 94 0
 332 0196 BB1D     		adds	r3, r7, #6
 333 0198 1B78     		ldrb	r3, [r3]
 334 019a 382B     		cmp	r3, #56
 335 019c 03D1     		bne	.L17
  96:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
 336              		.loc 1 96 0
 337 019e 0220     		movs	r0, #2
 338 01a0 FFF7FEFF 		bl	move_racket2
 339              		.loc 1 102 0
 340 01a4 01E0     		b	.L18
 341              	.L17:
 100:C:/Users/pontu/Documents/Programmering/C/mop/Pong\main.c ****     }
 342              		.loc 1 100 0
 343 01a6 FFF7FEFF 		bl	stop_racket2
 344              	.L18:
 345              		.loc 1 102 0
 346 01aa C046     		nop
 347 01ac BD46     		mov	sp, r7
 348 01ae 03B0     		add	sp, sp, #12
 349              		@ sp needed
 350 01b0 90BD     		pop	{r4, r7, pc}
 351              	.L20:
 352 01b2 C046     		.align	2
 353              	.L19:
 354 01b4 140C0240 		.word	1073875988
 355 01b8 100C0240 		.word	1073875984
 356 01bc 150C0240 		.word	1073875989
 357 01c0 110C0240 		.word	1073875985
 358              		.cfi_endproc
 359              	.LFE3:
 361              	.Letext0:
 362              		.file 2 "C:/Users/pontu/Documents/Programmering/C/mop/Pong/graphicdisplay.h"
 363              		.file 3 "C:/Users/pontu/Documents/Programmering/C/mop/Pong/PongGame.h"
