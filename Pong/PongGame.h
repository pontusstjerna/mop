#ifndef PONGGAME_H
#define PONGGAME_H

typedef enum {up = -2, down = 2} Dir;
typedef enum {x = 0, y = 1} POINT;

void update();
void move_racket1(Dir dir);
void move_racket2(Dir dir);
void stop_racket1();
void stop_racket2();
void get_pixels(int sizex, int sizey, unsigned char pixels[sizex][sizey]);
int get_player1_score();
int get_player2_score();

#endif //PONGGAME_H