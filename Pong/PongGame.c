#include "ponggame.h"
#include "gameobject.h"

#define HEIGHT 63
#define WIDTH 127

//Declerations
void checkCollisions();
void checkRacketCollision(POBJECT ball, POBJECT rackets[2]);
void checkBounce(POBJECT);
void bouncY(POBJECT);

void score(POBJECT);
int player1Score = 0;
int player2Score = 0;

static GEOMETRY ball_geometry =
{
	12,
	4,4,
	{   {1,0},{2,0},
     {0,1},{1,1},{2,1},{3,1},
     {0,2},{1,2},{2,2},{3,2},
        {1,3},{2,3}

        
    }
};


void setDir(POBJECT obj, int speedX, int speedY)
{
	obj->dirx = speedX;
	obj->diry = speedY;
}


void move(POBJECT obj)
{
	int newX = obj->posx + obj->dirx; 
	int newY = obj->posy + obj->diry;
	int newDirX = obj->dirx;
	int newDirY = obj->diry;
		
	obj->posx = obj->posx + newDirX;
	obj->posy = obj->posy + newDirY;
}

void setPos(POBJECT obj, int x, int y)
{
	int newX = x; 
	int newY = y;		
	obj->posx = newX;
	obj->posy = newY;
}

static GEOMETRY racket_geometry =
{
	30,
	2,15,
	{{0,0},{0,1},{0,2},{0,3},{0,4},{0,5},{0,6},{0,7},{0,8},{0,9},{0,10},{0,11},{0,12},{0,13},{0,14},
	 {1,0},{1,1},{1,2},{1,3},{1,4},{1,5},{1,6},{1,7},{1,8},{1,9},{1,10},{1,11},{1,12},{1,13},{1,14}}
};

static OBJECT ball = 
{
	&ball_geometry,
	5,2,
	WIDTH-30,5,
	move,
	setDir,
    setPos
};

static OBJECT racket1 = 
{
	&racket_geometry,
	0,0,
	4,24,
	move,
	setDir,
    setPos
};

static OBJECT racket2 = 
{
	&racket_geometry,
	0,0,
	123,24,
	move,
	setDir,
    setPos
};

void update()
{
    POBJECT gameObjects[] = {&ball, &racket1, &racket2};
    
    //POBJECT b = &ball;
    
  //  b->move(b);
    
    int i;
    for(i = 0; i < 3; i++)
    {
        
        gameObjects[i]->move(gameObjects[i]);
        checkCollisions();
    }
    score(gameObjects[0]);
    
	//Game logics
}

void resetBall(POBJECT balltemp)
{
    balltemp->setPos(balltemp, 60, 5);
    balltemp->setDir(balltemp, -5, 2);
}

void score(POBJECT ball)
{
    if(ball->posx + (ball->geo->width>>1) < 0)
    {
        player2Score++;
    } else if(ball->posx + (ball->geo->width>>1) > WIDTH){
        player1Score++;
    }
    else
    {
        return;
    }
    
    resetBall(ball);
}

int get_player1_score()
{
    return player1Score;
}
int get_player2_score()
{
    return player2Score;
}

int isBetween(int pos, int min, int max)
{
    return pos <= max && pos >= min;
}

void checkRacketCollision(POBJECT ball, POBJECT rackets[2])
{
    int i;
    for(i = 0; i < 2; i++)
    {
           if(((ball->posx < rackets[i]->posx + rackets[i]->geo->width &&
               ball->posx + ball->geo->width > rackets[i]->posx + rackets[i]->geo->width) ||
              (ball->posx + ball->geo->width > rackets[i]->posx &&
               ball->posx < rackets[i]->posx)) &&
               isBetween(ball->posy + ball->geo->height/2, rackets[i]->posy, rackets[i]->posy + rackets[i]->geo->height))
               {
                   int newDirX = -(ball->dirx);
                    ball->dirx = newDirX;
               }
               
               if(((ball->posy < rackets[i]->posy + rackets[i]->geo->height &&
               ball->posy + ball->geo->height > rackets[i]->posy + rackets[i]->geo->height) ||
              (ball->posy + ball->geo->height > rackets[i]->posy &&
               ball->posy < rackets[i]->posy)) &&
               isBetween(ball->posx + ball->geo->width/2, rackets[i]->posx, rackets[i]->posx + rackets[i]->geo->width))
               {
                   bounceY(ball);
               }

          /* if((b.getX() - b.getRadius() < r.getX() + r.getWidth()/2 &&
                        b.getX() + b.getRadius() > r.getX() - r.getWidth()/2) &&
                        (b.getY() - b.getRadius() < r.getY() + r.getLength()/2 &&
                        b.getY() + b.getRadius() > r.getY() - r.getLength()/2)){
                    b.bounce(new Vector2D(r.getNormal()));
                }*/
    }
    
}

void checkCollisions()
{
    POBJECT rackets[] = {&racket1, &racket2};
    checkBounce(&ball);
    checkRacketCollision(&ball, rackets);
    checkRacketWallCollision(rackets);
}

void checkRacketWallCollision(POBJECT rackets[2])
{
    int i;
    for(i = 0; i < 2; i++)
    {
        if(rackets[i]->posy < 0 || rackets[i]->posy + rackets[i]->geo->height > HEIGHT)
        {
            int newDirY = -rackets[i]->diry;
            rackets[i]->setDir(rackets[i], 0, newDirY);
        }
    }
}


void checkBounce(POBJECT ball)
{
    if(ball->posy + ball->geo->height >= HEIGHT || ball->posy <= 0)
    {
        bounceY(ball);
    }
}

void bounceY(POBJECT ball)
{
    int newDirY = -ball->diry;
    ball->diry = newDirY;
}

void move_racket1(Dir dir)
{
	racket1.setDir(&racket1, 0,dir);
}

void move_racket2(Dir dir)
{
	racket2.setDir(&racket2, 0,dir);
}

void stop_racket1()
{
    racket1.setDir(&racket1, 0, 0);
}

void stop_racket2()
{
    racket2.setDir(&racket2, 0, 0);
}

int get_numberof_pixels()
{
	POBJECT gameObjects[] = {&ball, &racket1, &racket2};
	return gameObjects[0]->geo->numpoints + gameObjects[1]->geo->numpoints*2;
}

//Dont touch this shit
void get_pixels(int sizex, int sizey, unsigned char pixels[sizex][sizey])
{
	POBJECT gameObjects[] = {&ball, &racket1, &racket2};
	
	int h = 0;
	int i,j;
	//Go through all 3 game objects
	for(i = 0; i < 3; i++)
	{
		//For every object, go through all their pixels
		for(j = 0; j < (gameObjects[i]->geo->numpoints); j++)
		{
			int testPoints = gameObjects[i]->geo->numpoints;
			pixels[h][x] = gameObjects[i]->geo->points[j][x] + gameObjects[i]->posx;
			pixels[h][y] = gameObjects[i]->geo->points[j][y] + gameObjects[i]->posy;
			
			h++;
		}
	}
}