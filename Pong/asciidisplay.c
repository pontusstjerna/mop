#include "asciidisplay.h"


void ascii_init(void){
	//Set rows and size
	ascii_command(0x38, 39);
	
	//Start display, start and set marker constant
	ascii_command(0x0E, 39);
	
	//Clear display
	ascii_command(0x01, 1530);
	
	//Set increment and no shift
	ascii_command(0x03, 39);
}

//Set set bits in x to 1
void ascii_ctrl_bit_set(unsigned char x){ 
	unsigned char c = *(GPIO_ODR_LOW);
	c = c | (B_SELECT | x);
	*(GPIO_ODR_LOW) = c;
}

//Set set bits in x to 0
void ascii_ctrl_bit_clear(unsigned char x){
	unsigned char c;
	c = *(GPIO_ODR_LOW);
	c = B_SELECT | (c & ~x);
	*(GPIO_ODR_LOW) = c;
}

void ascii_write_controller(unsigned char byte){
	ascii_ctrl_bit_set(B_E);
	*(GPIO_ODR_HIGH) = byte;
	delay_250ns();
	ascii_ctrl_bit_clear(B_E);
}

unsigned char ascii_read_controller(void){
	ascii_ctrl_bit_set(B_E);
	delay_250ns();
	delay_250ns();
	unsigned char byte = *(GPIO_IDR_HIGH);
	ascii_ctrl_bit_clear(B_E);
	return byte;
}

void ascii_write_cmd(unsigned char command){
	ascii_ctrl_bit_clear(B_RS);
	ascii_ctrl_bit_clear(B_RW);
	ascii_write_controller(command);
}

void ascii_write_data(unsigned char data){
	ascii_ctrl_bit_set(B_RS);
	ascii_ctrl_bit_clear(B_RW);
	ascii_write_controller(data);
}

unsigned char ascii_read_status(void){
	*(GPIO_MODER) = 0x00005555;
	ascii_ctrl_bit_clear(B_RS);
	ascii_ctrl_bit_set(B_RW);
	unsigned char status = ascii_read_controller();
	*(GPIO_MODER) = 0x55555555;
	*(GPIO_ODR_HIGH) = status;
	return status;
}

unsigned char ascii_read_data(void){
	unsigned int reg = *(GPIO_MODER);
	*(GPIO_MODER) = 0x0000FFFF & reg;
	ascii_ctrl_bit_set(B_RS);
	ascii_ctrl_bit_set(B_RW);
	unsigned char status = ascii_read_controller();
	*(GPIO_MODER) = 0x55550000 | reg;
	return status;
}

void ascii_command(unsigned char command, unsigned int delayUs){
	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
	delay_mikro(8); //Wait 8 us
	ascii_write_cmd(command); //Send command
	delay_mikro(delayUs); //Wait specific time
}

void ascii_gotoPos(int x, int y){
	if(x < 1){
		x = 1;
	}else if(x > 20){
		x = 20;
	}
	if(y < 1){
		y = 1;
	}else if(y > 2){
		y = 2;
	}
	
	unsigned char bits = x-1;
	if(y == 2){
		bits += 0x40;
	}
	
	ascii_write_cmd(bits | 0x80);
}

void ascii_write_char(unsigned char c){
	while((ascii_read_status() & 0x80) == 0x80); //Is busy?
	delay_mikro(8); 
	ascii_write_data(c);
	delay_mikro(43);
}

short divu10(short dividend) {
  short quotient = 0;
  #define div10_step(n) \
    do { if (dividend >= (n*10)) { quotient += n; dividend -= n*10; } } while (0)
  div10_step(0x1000);
  div10_step(0x0800);
  div10_step(0x0400);
  div10_step(0x0200);
  div10_step(0x0100);
  div10_step(0x0080);
  div10_step(0x0040);
  div10_step(0x0020);
  div10_step(0x0010);
  div10_step(0x0008);
  div10_step(0x0004);
  div10_step(0x0002);
  div10_step(0x0001);
  #undef div10_step
  //if (dividend >= 5) ++quotient; // round the result (optional)
  return quotient;
}

void writeScore(int player1, int player2)
{
    
	char player1text[] = "Player 1: ";
	char player2text[] = "Player 2: ";
	//init_app();
	ascii_init();
	ascii_gotoPos(1,1);
	//Writing player1:
	writeScoreToAscii(player1text);
    //Writing player 1 score:
    int nDigits = countDigits(player1);
    char score1[nDigits];
    toCharArray(player1, nDigits, score1);
    writeScoreToAscii(score1);
	ascii_gotoPos(1,2);
	writeScoreToAscii(player2text);
    nDigits = countDigits(player2);
    char score2[nDigits];
    toCharArray(player2, nDigits, score2);
    writeScoreToAscii(score2);
}

void writeScoreToAscii(char* array)
{
    while(*array){
		ascii_write_char(*array++);
	}
}

unsigned modu10(unsigned int n) {
    unsigned q, r;
    q = (n >> 1) + (n >> 2);
    q = q + (q >> 4);
    q = q + (q >> 8);
    q = q + (q >> 16);
    q = q >> 3;
    r = n - (((q << 2) + q) << 1);
    return r;
}

void toCharArray(int value, int nDigits, char result[nDigits])
{
    int i;
    for(i = 0; i < nDigits; i++)
    {
        int x = divu10(value);
        int rest = value - (divu10(value) * 10);
        if (rest != 0)
            value = divu10(value);
        else
            value = divu10(value+1);
        char elem = 48 + rest;
        result[nDigits - i - 1] = elem;
    }
}

int countDigits(int value)
{
    if (value == 0)
        return 1;
    int count = 0;
    while(value != 0)
    {
        value = divu10(value);
        count++;
    }
    return count;
}



/*
unsigned modu10(unsigned n) {
    unsigned q, r;
    q = (n >> 1) + (n >> 2);
    q = q + (q >> 4);
    q = q + (q >> 8);
    q = q + (q >> 16);
    q = q >> 3;
    r = n - (((q << 2) + q) << 1);
    return q + (r > 9);
}
*/