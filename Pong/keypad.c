#include "keypad.h"

void activateRow(int i, unsigned volatile char* portIn, unsigned  volatile char* portOut);
int readColumn( unsigned volatile char* port);

char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};

unsigned char get_key_down(unsigned volatile char* portIn, unsigned volatile char* portOut)
{
	int i;
	for(i = 0; i < 4; i++){
		activateRow(i,portIn, portOut);
		int column = readColumn(portIn);
		if(column != -1)
		{
			return keyValues[i][column];
		}
	}
	return 0xFF;
}

void activateRow(int i, unsigned volatile char* portIn, unsigned volatile char* portOut)
{
	unsigned char bits;
	switch(i){
		case 0: //Set bits to 0001xxxx
			bits = 0x10;
			break;
		case 1: //Set bits to 0010xxxx
			bits = 0x20;
			break;
		case 2: //Set bits to 0100xxxx
			bits = 0x40;
			break;
		case 3: //Set bits to 1000xxxx
			bits = 0x80;
			break;
	}
	//First half is output other half is input
	unsigned char setBits = *portIn;
	
	//Set first 4 to 0 and keep the others
	setBits = setBits & 0x0F;
	
	//Now set the first 4 to wanted and keep others
	setBits = setBits | bits;
	
	*portOut = setBits;
}

int readColumn(unsigned volatile char* port){
	unsigned char bits = *port;
	int i;
	for(i = 0; i < 4; i++){
		if(bits % 2 == 1){ //If odd
			return i;
		}
		bits = bits>>1;
	}
	return -1;
}