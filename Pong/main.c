#include "keypad.h"
#include "graphicdisplay.h"
#include "asciidisplay.h"
#include "PongGame.h"

//Port D
#define GPIO_D_MODER (unsigned volatile long*) 0x40020C00
#define GPIO_D_OTYPER (unsigned volatile short*) 0x40020C04
#define GPIO_D_OSPEEDR (unsigned volatile long*) 0x40020C08
#define GPIO_D_PUPDR (unsigned volatile long*) 0x40020C0C
#define GPIO_D_IDR_PLAYER1 (vbyte*) 0x40020C10
#define GPIO_D_IDR_PLAYER2 (vbyte*) 0x40020C11
#define GPIO_D_ODR_PLAYER1 (vbyte*) 0x40020C14
#define GPIO_D_ODR_PLAYER2 (vbyte*) 0x40020C15

void moveRackets();


void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void startup ( void )
{
asm volatile(
	" LDR R0, =0x40023830\n"
	" MOV R1, #0x18\n"
	" STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void game_init()
{
	//Includes setting upp GPIO_E
	graphic_initialize();
	
	//Use borth high and low port D for 2 keypads
	*(GPIO_D_MODER) = 0x55005500;
	*(GPIO_D_PUPDR) = 0x00AA00AA;
}

int player1ScoreMain = 0;
int player2ScoreMain = 0;

void main(void)
{
	game_init();
    player1ScoreMain = get_player1_score();
    player2ScoreMain = get_player2_score();
    writeScore(player1ScoreMain,player2ScoreMain);
    unsigned char pixels[get_numberof_pixels()][2];
	while(1)
	{
        //Clear
        clearPixels(get_numberof_pixels(), 2, pixels);
        update();
        moveRackets();
        get_pixels(get_numberof_pixels(), 2, pixels);
        drawPixels(get_numberof_pixels(), 2, pixels);
        if(player1ScoreMain != get_player1_score() || player2ScoreMain != get_player2_score())
        {
            player1ScoreMain = get_player1_score();
            player2ScoreMain = get_player2_score();
            writeScore(player1ScoreMain, player2ScoreMain);
        }
            
	}
}

void moveRackets()
{
    char player1 = get_key_down(GPIO_D_IDR_PLAYER1, GPIO_D_ODR_PLAYER1);
    char player2 = get_key_down(GPIO_D_IDR_PLAYER2, GPIO_D_ODR_PLAYER2);
    
    if(player1 == '2')
    {
        move_racket1(up);
    }
    else if(player1 == '8')
    {
        move_racket1(down);
    }
    else
    {
       stop_racket1(); 
    }
    
    if(player2 == '2')
    {
        move_racket2(up);
    }
    else if(player2 == '8')
    {
        move_racket2(down);
    }
    else
    {
       stop_racket2(); 
    }
}

