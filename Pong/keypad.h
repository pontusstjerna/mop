#ifndef KEYPAD_H
#define KEYPAD_H

unsigned char get_key_down(volatile unsigned char* portIn, volatile unsigned char* portOut);
#endif