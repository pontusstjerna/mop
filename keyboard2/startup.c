/*
 * 	startup.c
 *
 */
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );


void startup ( void )
{
asm volatile(
	"LDR R0, =0x40023830\n"
	"MOV R1, #0x18\n"
	"STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return :'( */
	) ;
}

#define GPIO_MODER (unsigned long*) 0x40020C00
#define GPIO_IPR_LOW (unsigned char*) 0x40020C10
#define GPIO_IPR_HIGH (unsigned char*) 0x40020C11
#define GPIO_OPR_LOW (unsigned char*) 0x40020C14
#define GPIO_OPR_HIGH (unsigned char*) 0x40020C15
#define GPIO_PUPDR (unsigned long*) 0x40020C0C

char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};

void app_init(void);
unsigned char keyb(void);
void out7seg(unsigned char c);
void activateRow(int i);
int readColumn(void);

void main(void)
{
	app_init();
	
	while(1){
		out7seg(keyb());
	}
}

void app_init(){
	*(GPIO_MODER) = 0x55005555;
	*(GPIO_PUPDR) = 0x00AA0000;
	
}

unsigned char keyb(void){
	int i;
	for(i = 0; i < 4; i++){
		activateRow(i);
		int column = readColumn();
		if(column != -1){
			return keyValues[i][column];
		}
	}
	return 0xFF;
}

void activateRow(int i){
	unsigned char bits;
	switch(i){
		case 0: //Set bits to 0001xxxx
			bits = 0x10;
			break;
		case 1: //Set bits to 0010xxxx
			bits = 0x20;
			break;
		case 2: //Set bits to 0100xxxx
			bits = 0x40;
			break;
		case 3: //Set bits to 1000xxxx
			bits = 0x80;
			break;
	}
	//First half is output other half is input
	unsigned char setBits = *(GPIO_IPR_HIGH);
	
	//Set first 4 to 0 and keep the others
	setBits = setBits & 0x0F;
	
	//Now set the first 4 to wanted and keep others
	setBits = setBits | bits;
	
	*(GPIO_OPR_HIGH) = setBits;
}

int readColumn(void){
	unsigned char bits = *(GPIO_IPR_HIGH);
	int i;
	for(i = 0; i < 4; i++){
		if(bits % 2 == 1){ //If odd
			return i;
		}
		bits = bits>>1;
	}
	return -1;
}


int testI = 0;
void out7seg(unsigned char c){
	unsigned char bits;
	
	/*
	 *  _0_
	 *5|_6|1
	 *4|__|2
	 *  3 .7
	 */
	
	switch(c){
		case 0xFF: bits = 0x00; break;
		case '1': bits = 0x06; break;
		case '2': bits = 0x5B; break;
		case '3': bits = 0x4F; break;
		case '4': bits = 0x66; break;
		case '5': bits = 0x6D; break;
		case '6': bits = 0x7D; break;
		case '7': bits = 0x07; break;
		case '8': bits = 0x7F; break;
		case '9': bits = 0x6F; break;
		case '0': bits = 0x3F; break;
		case 'A': bits = 0x77; break;
		case 'B': bits = 0x7C; break;
		case 'C': bits = 0x39; break;
		case 'D': bits = 0x5E; break;
		case '*': bits = 0x49; break;
		default: bits = 0x79; break;
		
		//
	}
	
	*(GPIO_OPR_LOW) = bits;
}