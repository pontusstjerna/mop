/*
 * 	startup.c
 *
 */
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void delay_250ns(void);
void delay_mikro(unsigned int);
void delay_milli(unsigned int);
void ascii_ctrl_bit_set(unsigned char);
void ascii_ctrl_bit_clear(unsigned char);
void ascii_write_controller(unsigned char);
unsigned char ascii_read_controller(void);
void ascii_write_cmd(unsigned char);
void ascii_write_data(unsigned char);
unsigned char ascii_read_status(void);
unsigned char ascii_read_data(void);
void ascii_init(void);
void init_app(void);
void ascii_write_char(unsigned char);
void ascii_gotoxy(int,int);

#define PORT_BASE 0x40021000
#define portModer (unsigned volatile long*) PORT_BASE
#define portOtyper (unsigned volatile short*) PORT_BASE + 0x04
#define portOspeedr (unsigned volatile long*) PORT_BASE + 0x08
#define portPupdr (unsigned volatile long*) PORT_BASE + 0x0C

#define portIdrLow (unsigned volatile char*) PORT_BASE + 0x10
#define portIdrHigh (unsigned volatile char*) PORT_BASE + 0x11
#define portOdrLow (unsigned volatile char*) PORT_BASE + 0x14
#define portOdrHigh (unsigned volatile char*) PORT_BASE + 0x15

#define STK_CTRL (unsigned volatile long *) 0xE000E010
#define STK_LOAD (unsigned volatile long *) 0xE000E010 + 0x04
#define STK_VAL (unsigned volatile long *) 0xE000E010 + 0x08

#define B_E 0x40
#define B_SELECT 0x04
#define B_RW 0x02
#define B_RS 0x01

void startup ( void )
{
asm volatile(
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void delay_250ns(void)
{

    *(STK_CTRL) = 0;
    *(STK_LOAD) = ( (168/4) -1 );
    *(STK_VAL) = 0;
    *(STK_CTRL) = 5;
    while( (*STK_CTRL & 0x10000 )== 0 )
    {}
    *(STK_CTRL) = 0;
}

void delay_mikro(unsigned int us)
{
    while (us--) {
        delay_250ns();
        delay_250ns();
        delay_250ns();
        delay_250ns();
    }
}

void delay_milli(unsigned int ms)
{
    int i;
    while(ms--)
    {
        delay_mikro(1000);
    }
}

ascii_ctrl_bit_set( unsigned char x )
{
     unsigned char c;
     c = *(portOdrLow);
     c |= ( B_SELECT | x );
     *(portOdrLow) = c;
}

void ascii_ctrl_bit_clear( unsigned char x )
{ 
    unsigned char c;
    c = *(portOdrLow);
    c = B_SELECT | ( c & ~x );
    *(portOdrLow) = c;
}

void ascii_write_controller( unsigned char c )
{ 
    ascii_ctrl_bit_set( B_E );
    *(portOdrHigh) = c;
    ascii_ctrl_bit_clear( B_E );
    delay_250ns();
}
   
unsigned char ascii_read_controller( void )
{ 
    unsigned char c; 
    ascii_ctrl_bit_set( B_E );
    delay_250ns();
    delay_250ns();
    c = *portIdrHigh;
    ascii_ctrl_bit_clear( B_E );
    return c;
}

void ascii_write_cmd(unsigned char command)
{
    ascii_ctrl_bit_clear(B_RS);
    ascii_ctrl_bit_clear(B_RW);
    ascii_write_controller(command);
}

void ascii_write_data(unsigned char data)
{
    ascii_ctrl_bit_set(B_RS);
    ascii_ctrl_bit_clear(B_RW);
    ascii_write_controller(data);
}

unsigned char ascii_read_status(void)
{
    *(portModer) &= 0x0000FFFF;
    ascii_ctrl_bit_clear(B_RS);
    ascii_ctrl_bit_set(B_RW);
    unsigned char rv = ascii_read_controller();
    *(portModer) |= 0x55550000;
    return rv;
}

unsigned char ascii_read_data(void)
{
    *(portModer) &= 0x0000FFFF;
    ascii_ctrl_bit_set(B_RS);
    ascii_ctrl_bit_set(B_RW);
    unsigned char rv = ascii_read_controller();
    *(portModer) |= 0x55550000;
    return rv;
}

void ascii_init(void)
{
    while( (ascii_read_status() & 0x80)== 0x80 )
    {}
    delay_mikro(8);
    ascii_write_cmd(0b0000111000);
    delay_mikro(40);
    ascii_write_cmd(0b0000001110);
    delay_mikro(40);
    ascii_write_cmd(1);
    delay_milli(2);
    ascii_write_cmd(0b0000000100);
    delay_mikro(40);
}

void init_app(void)
{
    *(portModer) = 0x55555555;
}

void ascii_write_char(unsigned char c)
{
    while ((ascii_read_status() & 0x80) == 0x80)
    {}
    delay_mikro(8);
    ascii_write_data(c);
    delay_mikro(43);
}

void ascii_gotoxy(int x, int y)
{
    int address = x - 1;
    if (y == 2)
    {
        address = address + 0x40;
    }
    ascii_write_cmd(0x80 | address);
}
 
int main(int argc, char **argv)
{
    char *s;
    char test1[] = "Alfanumerisk";
    char test2[] = "Display - test";
    
    init_app();
    ascii_init();
    ascii_gotoxy(1,1);
    s = test1;
    while(*s)
        ascii_write_char(*s++);
    ascii_gotoxy(1,2);
    s = test2;
    while(*s)
        ascii_write_char(*s++);
    return 0;
}