   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"graphicdisplay.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc6
   1:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** #include "delay.h"
   2:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** #include "boolean.h"
   3:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** #include "graphicdisplay.h"
   4:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
   5:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** //#define SIMULATOR
   6:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
   7:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
   8:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
   9:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  10:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  11:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** // The graphic declarations
  12:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_ctrl_bit_set(byte);
  13:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_ctrl_bit_clear(byte);
  14:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void select_controller(byte);
  15:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_wait_ready(void);
  16:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** byte graphic_read(byte);
  17:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** byte graphic_read_data(byte);
  18:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write(byte, byte);
  19:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write_cmd(byte, byte);
  20:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write_data(byte, byte);
  21:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void put_pixel(byte, byte, byte);
  22:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  23:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void drawSmiley(boolean);
  24:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  25:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_clear_screen(void);
  26:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  27:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void startup ( void )
  28:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
  28              		.loc 1 28 0
  29              		.cfi_startproc
  29:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** asm volatile(
  30              		.loc 1 29 0
  31              		.syntax divided
  32              	@ 29 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b9
  33 0000 0448     		LDR R0, =0x40023830
  34 0002 1821     	MOV R1, #0x18
  35 0004 0160     	STR R1, [R0]
  36 0006 0448     	 LDR R0,=0x2001C000
  37 0008 8546     	 MOV SP,R0
  38 000a FFF7FEFF 	 BL main
  39 000e FEE7     	.L1: B .L1
  40              	
  41              	@ 0 "" 2
  30:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	"LDR R0, =0x40023830\n"
  31:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	"MOV R1, #0x18\n"
  32:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	"STR R1, [R0]\n"
  33:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  34:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	" MOV SP,R0\n"
  35:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	" BL main\n"				/* call main */
  36:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	".L1: B .L1\n"				/* never return */
  37:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	) ;
  38:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
  42              		.loc 1 38 0
  43              		.thumb
  44              		.syntax unified
  45 0010 C046     		nop
  46              		.cfi_endproc
  47              	.LFE0:
  49 0012 0000     		.text
  50              		.align	2
  51              		.global	init_app
  52              		.code	16
  53              		.thumb_func
  55              	init_app:
  56              	.LFB1:
  39:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  40:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void init_app()
  41:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
  57              		.loc 1 41 0
  58              		.cfi_startproc
  59 0000 80B5     		push	{r7, lr}
  60              		.cfi_def_cfa_offset 8
  61              		.cfi_offset 7, -8
  62              		.cfi_offset 14, -4
  63 0002 00AF     		add	r7, sp, #0
  64              		.cfi_def_cfa_register 7
  42:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_MODER) = 0x55555555;
  65              		.loc 1 42 0
  66 0004 024B     		ldr	r3, .L3
  67 0006 034A     		ldr	r2, .L3+4
  68 0008 1A60     		str	r2, [r3]
  43:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
  69              		.loc 1 43 0
  70 000a C046     		nop
  71 000c BD46     		mov	sp, r7
  72              		@ sp needed
  73 000e 80BD     		pop	{r7, pc}
  74              	.L4:
  75              		.align	2
  76              	.L3:
  77 0010 00100240 		.word	1073876992
  78 0014 55555555 		.word	1431655765
  79              		.cfi_endproc
  80              	.LFE1:
  82              		.align	2
  83              		.global	graphic_initialize
  84              		.code	16
  85              		.thumb_func
  87              	graphic_initialize:
  88              	.LFB2:
  44:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  45:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_initialize(void)
  46:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
  89              		.loc 1 46 0
  90              		.cfi_startproc
  91 0018 80B5     		push	{r7, lr}
  92              		.cfi_def_cfa_offset 8
  93              		.cfi_offset 7, -8
  94              		.cfi_offset 14, -4
  95 001a 00AF     		add	r7, sp, #0
  96              		.cfi_def_cfa_register 7
  47:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_E);
  97              		.loc 1 47 0
  98 001c 4020     		movs	r0, #64
  99 001e FFF7FEFF 		bl	graphic_ctrl_bit_set
  48:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_mikro(10);
 100              		.loc 1 48 0
 101 0022 0A20     		movs	r0, #10
 102 0024 FFF7FEFF 		bl	delay_mikro
  49:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_CS1 | B_CS2 | B_RST | B_E);
 103              		.loc 1 49 0
 104 0028 7820     		movs	r0, #120
 105 002a FFF7FEFF 		bl	graphic_ctrl_bit_clear
  50:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	#ifndef SIMULATOR
  51:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_milli(30);
 106              		.loc 1 51 0
 107 002e 1E20     		movs	r0, #30
 108 0030 FFF7FEFF 		bl	delay_milli
  52:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	#elif
  53:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_milli(3);
  54:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	#endif
  55:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_RST);
 109              		.loc 1 55 0
 110 0034 2020     		movs	r0, #32
 111 0036 FFF7FEFF 		bl	graphic_ctrl_bit_set
  56:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_OFF, B_CS1 | B_CS2);
 112              		.loc 1 56 0
 113 003a 1821     		movs	r1, #24
 114 003c 3E20     		movs	r0, #62
 115 003e FFF7FEFF 		bl	graphic_write_cmd
  57:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_ON, B_CS1 | B_CS2);
 116              		.loc 1 57 0
 117 0042 1821     		movs	r1, #24
 118 0044 3F20     		movs	r0, #63
 119 0046 FFF7FEFF 		bl	graphic_write_cmd
  58:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_DISP_START, B_CS1 | B_CS2);
 120              		.loc 1 58 0
 121 004a 1821     		movs	r1, #24
 122 004c C020     		movs	r0, #192
 123 004e FFF7FEFF 		bl	graphic_write_cmd
  59:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_SET_ADD, B_CS1 | B_CS2);
 124              		.loc 1 59 0
 125 0052 1821     		movs	r1, #24
 126 0054 4020     		movs	r0, #64
 127 0056 FFF7FEFF 		bl	graphic_write_cmd
  60:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_SET_PAGE, B_CS1 | B_CS2);
 128              		.loc 1 60 0
 129 005a 1821     		movs	r1, #24
 130 005c B820     		movs	r0, #184
 131 005e FFF7FEFF 		bl	graphic_write_cmd
  61:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
  62:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(0);
 132              		.loc 1 62 0
 133 0062 0020     		movs	r0, #0
 134 0064 FFF7FEFF 		bl	select_controller
  63:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 135              		.loc 1 63 0
 136 0068 C046     		nop
 137 006a BD46     		mov	sp, r7
 138              		@ sp needed
 139 006c 80BD     		pop	{r7, pc}
 140              		.cfi_endproc
 141              	.LFE2:
 143 006e C046     		.align	2
 144              		.global	main
 145              		.code	16
 146              		.thumb_func
 148              	main:
 149              	.LFB3:
  64:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  65:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void main(void)
  66:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 150              		.loc 1 66 0
 151              		.cfi_startproc
 152 0070 80B5     		push	{r7, lr}
 153              		.cfi_def_cfa_offset 8
 154              		.cfi_offset 7, -8
 155              		.cfi_offset 14, -4
 156 0072 00AF     		add	r7, sp, #0
 157              		.cfi_def_cfa_register 7
  67:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	init_app();
 158              		.loc 1 67 0
 159 0074 FFF7FEFF 		bl	init_app
  68:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_initialize();
 160              		.loc 1 68 0
 161 0078 FFF7FEFF 		bl	graphic_initialize
  69:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
  70:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	#ifndef SIMULATOR
  71:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_clear_screen();
 162              		.loc 1 71 0
 163 007c FFF7FEFF 		bl	graphic_clear_screen
 164              	.L7:
  72:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	#endif
  73:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
  74:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	while(true) 
  75:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
  76:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		drawSmiley(true);
 165              		.loc 1 76 0 discriminator 1
 166 0080 0120     		movs	r0, #1
 167 0082 FFF7FEFF 		bl	drawSmiley
  77:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_milli(500);
 168              		.loc 1 77 0 discriminator 1
 169 0086 FA23     		movs	r3, #250
 170 0088 5B00     		lsls	r3, r3, #1
 171 008a 1800     		movs	r0, r3
 172 008c FFF7FEFF 		bl	delay_milli
  78:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		drawSmiley(false);
 173              		.loc 1 78 0 discriminator 1
 174 0090 0020     		movs	r0, #0
 175 0092 FFF7FEFF 		bl	drawSmiley
  79:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_milli(500);
 176              		.loc 1 79 0 discriminator 1
 177 0096 FA23     		movs	r3, #250
 178 0098 5B00     		lsls	r3, r3, #1
 179 009a 1800     		movs	r0, r3
 180 009c FFF7FEFF 		bl	delay_milli
  80:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 181              		.loc 1 80 0 discriminator 1
 182 00a0 EEE7     		b	.L7
 183              		.cfi_endproc
 184              	.LFE3:
 186 00a2 C046     		.align	2
 187              		.global	graphic_ctrl_bit_set
 188              		.code	16
 189              		.thumb_func
 191              	graphic_ctrl_bit_set:
 192              	.LFB4:
  81:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
  82:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  83:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  84:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_ctrl_bit_set(byte x) //Super tested
  85:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 193              		.loc 1 85 0
 194              		.cfi_startproc
 195 00a4 80B5     		push	{r7, lr}
 196              		.cfi_def_cfa_offset 8
 197              		.cfi_offset 7, -8
 198              		.cfi_offset 14, -4
 199 00a6 84B0     		sub	sp, sp, #16
 200              		.cfi_def_cfa_offset 24
 201 00a8 00AF     		add	r7, sp, #0
 202              		.cfi_def_cfa_register 7
 203 00aa 0200     		movs	r2, r0
 204 00ac FB1D     		adds	r3, r7, #7
 205 00ae 1A70     		strb	r2, [r3]
  86:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte bits = *(GPIO_ODR_LOW);
 206              		.loc 1 86 0
 207 00b0 0B4A     		ldr	r2, .L9
 208 00b2 0F23     		movs	r3, #15
 209 00b4 FB18     		adds	r3, r7, r3
 210 00b6 1278     		ldrb	r2, [r2]
 211 00b8 1A70     		strb	r2, [r3]
  87:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_ODR_LOW) = bits | (x & ~B_SELECT);
 212              		.loc 1 87 0
 213 00ba 094A     		ldr	r2, .L9
 214 00bc FB1D     		adds	r3, r7, #7
 215 00be 1B78     		ldrb	r3, [r3]
 216 00c0 191C     		adds	r1, r3, #0
 217 00c2 0423     		movs	r3, #4
 218 00c4 9943     		bics	r1, r3
 219 00c6 0B00     		movs	r3, r1
 220 00c8 D9B2     		uxtb	r1, r3
 221 00ca 0F23     		movs	r3, #15
 222 00cc FB18     		adds	r3, r7, r3
 223 00ce 1B78     		ldrb	r3, [r3]
 224 00d0 0B43     		orrs	r3, r1
 225 00d2 DBB2     		uxtb	r3, r3
 226 00d4 DBB2     		uxtb	r3, r3
 227 00d6 1370     		strb	r3, [r2]
  88:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 228              		.loc 1 88 0
 229 00d8 C046     		nop
 230 00da BD46     		mov	sp, r7
 231 00dc 04B0     		add	sp, sp, #16
 232              		@ sp needed
 233 00de 80BD     		pop	{r7, pc}
 234              	.L10:
 235              		.align	2
 236              	.L9:
 237 00e0 14100240 		.word	1073877012
 238              		.cfi_endproc
 239              	.LFE4:
 241              		.align	2
 242              		.global	graphic_ctrl_bit_clear
 243              		.code	16
 244              		.thumb_func
 246              	graphic_ctrl_bit_clear:
 247              	.LFB5:
  89:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  90:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_ctrl_bit_clear(byte x) //Super tested
  91:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 248              		.loc 1 91 0
 249              		.cfi_startproc
 250 00e4 80B5     		push	{r7, lr}
 251              		.cfi_def_cfa_offset 8
 252              		.cfi_offset 7, -8
 253              		.cfi_offset 14, -4
 254 00e6 84B0     		sub	sp, sp, #16
 255              		.cfi_def_cfa_offset 24
 256 00e8 00AF     		add	r7, sp, #0
 257              		.cfi_def_cfa_register 7
 258 00ea 0200     		movs	r2, r0
 259 00ec FB1D     		adds	r3, r7, #7
 260 00ee 1A70     		strb	r2, [r3]
  92:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte bits = *(GPIO_ODR_LOW);
 261              		.loc 1 92 0
 262 00f0 0B4A     		ldr	r2, .L12
 263 00f2 0F23     		movs	r3, #15
 264 00f4 FB18     		adds	r3, r7, r3
 265 00f6 1278     		ldrb	r2, [r2]
 266 00f8 1A70     		strb	r2, [r3]
  93:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_ODR_LOW) = bits & (~x & ~B_SELECT);
 267              		.loc 1 93 0
 268 00fa 0949     		ldr	r1, .L12
 269 00fc FB1D     		adds	r3, r7, #7
 270 00fe 1B78     		ldrb	r3, [r3]
 271 0100 DB43     		mvns	r3, r3
 272 0102 DBB2     		uxtb	r3, r3
 273 0104 0F22     		movs	r2, #15
 274 0106 BA18     		adds	r2, r7, r2
 275 0108 1278     		ldrb	r2, [r2]
 276 010a 1340     		ands	r3, r2
 277 010c DBB2     		uxtb	r3, r3
 278 010e 0422     		movs	r2, #4
 279 0110 9343     		bics	r3, r2
 280 0112 DBB2     		uxtb	r3, r3
 281 0114 0B70     		strb	r3, [r1]
  94:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 282              		.loc 1 94 0
 283 0116 C046     		nop
 284 0118 BD46     		mov	sp, r7
 285 011a 04B0     		add	sp, sp, #16
 286              		@ sp needed
 287 011c 80BD     		pop	{r7, pc}
 288              	.L13:
 289 011e C046     		.align	2
 290              	.L12:
 291 0120 14100240 		.word	1073877012
 292              		.cfi_endproc
 293              	.LFE5:
 295              		.align	2
 296              		.global	select_controller
 297              		.code	16
 298              		.thumb_func
 300              	select_controller:
 301              	.LFB6:
  95:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
  96:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void select_controller(byte controller) //Über tested
  97:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 302              		.loc 1 97 0
 303              		.cfi_startproc
 304 0124 80B5     		push	{r7, lr}
 305              		.cfi_def_cfa_offset 8
 306              		.cfi_offset 7, -8
 307              		.cfi_offset 14, -4
 308 0126 82B0     		sub	sp, sp, #8
 309              		.cfi_def_cfa_offset 16
 310 0128 00AF     		add	r7, sp, #0
 311              		.cfi_def_cfa_register 7
 312 012a 0200     		movs	r2, r0
 313 012c FB1D     		adds	r3, r7, #7
 314 012e 1A70     		strb	r2, [r3]
  98:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	switch(controller)
 315              		.loc 1 98 0
 316 0130 FB1D     		adds	r3, r7, #7
 317 0132 1B78     		ldrb	r3, [r3]
 318 0134 082B     		cmp	r3, #8
 319 0136 08D0     		beq	.L16
 320 0138 02DC     		bgt	.L17
 321 013a 002B     		cmp	r3, #0
 322 013c 1AD0     		beq	.L18
  99:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 100:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case B_CS1:
 101:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS1);
 102:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS2);
 103:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 104:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c ****         case B_CS2:
 105:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS1);
 106:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS2);
 107:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 108:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case (B_CS1 | B_CS2):
 109:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS1);
 110:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS2);
 111:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 112:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 0:
 113:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS1);
 114:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS2);
 115:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 116:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 117:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 323              		.loc 1 117 0
 324 013e 20E0     		b	.L21
 325              	.L17:
  98:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	switch(controller)
 326              		.loc 1 98 0
 327 0140 102B     		cmp	r3, #16
 328 0142 09D0     		beq	.L19
 329 0144 182B     		cmp	r3, #24
 330 0146 0ED0     		beq	.L20
 331              		.loc 1 117 0
 332 0148 1BE0     		b	.L21
 333              	.L16:
 101:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS2);
 334              		.loc 1 101 0
 335 014a 0820     		movs	r0, #8
 336 014c FFF7FEFF 		bl	graphic_ctrl_bit_set
 102:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 337              		.loc 1 102 0
 338 0150 1020     		movs	r0, #16
 339 0152 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 103:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c ****         case B_CS2:
 340              		.loc 1 103 0
 341 0156 14E0     		b	.L15
 342              	.L19:
 105:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS2);
 343              		.loc 1 105 0
 344 0158 0820     		movs	r0, #8
 345 015a FFF7FEFF 		bl	graphic_ctrl_bit_clear
 106:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 346              		.loc 1 106 0
 347 015e 1020     		movs	r0, #16
 348 0160 FFF7FEFF 		bl	graphic_ctrl_bit_set
 107:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case (B_CS1 | B_CS2):
 349              		.loc 1 107 0
 350 0164 0DE0     		b	.L15
 351              	.L20:
 109:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_set(B_CS2);
 352              		.loc 1 109 0
 353 0166 0820     		movs	r0, #8
 354 0168 FFF7FEFF 		bl	graphic_ctrl_bit_set
 110:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 355              		.loc 1 110 0
 356 016c 1020     		movs	r0, #16
 357 016e FFF7FEFF 		bl	graphic_ctrl_bit_set
 111:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 0:
 358              		.loc 1 111 0
 359 0172 06E0     		b	.L15
 360              	.L18:
 113:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_ctrl_bit_clear(B_CS2);
 361              		.loc 1 113 0
 362 0174 0820     		movs	r0, #8
 363 0176 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 114:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 364              		.loc 1 114 0
 365 017a 1020     		movs	r0, #16
 366 017c FFF7FEFF 		bl	graphic_ctrl_bit_clear
 115:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 367              		.loc 1 115 0
 368 0180 C046     		nop
 369              	.L15:
 370              	.L21:
 371              		.loc 1 117 0
 372 0182 C046     		nop
 373 0184 BD46     		mov	sp, r7
 374 0186 02B0     		add	sp, sp, #8
 375              		@ sp needed
 376 0188 80BD     		pop	{r7, pc}
 377              		.cfi_endproc
 378              	.LFE6:
 380 018a C046     		.align	2
 381              		.global	graphic_wait_ready
 382              		.code	16
 383              		.thumb_func
 385              	graphic_wait_ready:
 386              	.LFB7:
 118:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 119:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_wait_ready(void)
 120:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 387              		.loc 1 120 0
 388              		.cfi_startproc
 389 018c 80B5     		push	{r7, lr}
 390              		.cfi_def_cfa_offset 8
 391              		.cfi_offset 7, -8
 392              		.cfi_offset 14, -4
 393 018e 00AF     		add	r7, sp, #0
 394              		.cfi_def_cfa_register 7
 121:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 395              		.loc 1 121 0
 396 0190 4020     		movs	r0, #64
 397 0192 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 122:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_MODER) = 0x00005555;
 398              		.loc 1 122 0
 399 0196 134B     		ldr	r3, .L28
 400 0198 134A     		ldr	r2, .L28+4
 401 019a 1A60     		str	r2, [r3]
 123:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_RS);
 402              		.loc 1 123 0
 403 019c 0120     		movs	r0, #1
 404 019e FFF7FEFF 		bl	graphic_ctrl_bit_clear
 124:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_RW);
 405              		.loc 1 124 0
 406 01a2 0220     		movs	r0, #2
 407 01a4 FFF7FEFF 		bl	graphic_ctrl_bit_set
 125:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_500ns();
 408              		.loc 1 125 0
 409 01a8 FFF7FEFF 		bl	delay_500ns
 410              	.L25:
 126:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	while(true)
 127:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 128:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_ctrl_bit_set(B_E);
 411              		.loc 1 128 0
 412 01ac 4020     		movs	r0, #64
 413 01ae FFF7FEFF 		bl	graphic_ctrl_bit_set
 129:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_500ns();
 414              		.loc 1 129 0
 415 01b2 FFF7FEFF 		bl	delay_500ns
 130:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_ctrl_bit_clear(B_E);
 416              		.loc 1 130 0
 417 01b6 4020     		movs	r0, #64
 418 01b8 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 131:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		if(!(*(GPIO_IDR_HIGH) & LCD_BUSY ))
 419              		.loc 1 131 0
 420 01bc 0B4B     		ldr	r3, .L28+8
 421 01be 1B78     		ldrb	r3, [r3]
 422 01c0 DBB2     		uxtb	r3, r3
 423 01c2 DBB2     		uxtb	r3, r3
 424 01c4 5BB2     		sxtb	r3, r3
 425 01c6 002B     		cmp	r3, #0
 426 01c8 02DA     		bge	.L27
 132:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		{
 133:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 134:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		}
 135:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		delay_500ns();	
 427              		.loc 1 135 0
 428 01ca FFF7FEFF 		bl	delay_500ns
 136:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 429              		.loc 1 136 0
 430 01ce EDE7     		b	.L25
 431              	.L27:
 133:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		}
 432              		.loc 1 133 0
 433 01d0 C046     		nop
 137:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_E);
 434              		.loc 1 137 0
 435 01d2 4020     		movs	r0, #64
 436 01d4 FFF7FEFF 		bl	graphic_ctrl_bit_set
 138:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_MODER) = 0x55555555;
 437              		.loc 1 138 0
 438 01d8 024B     		ldr	r3, .L28
 439 01da 054A     		ldr	r2, .L28+12
 440 01dc 1A60     		str	r2, [r3]
 139:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 441              		.loc 1 139 0
 442 01de C046     		nop
 443 01e0 BD46     		mov	sp, r7
 444              		@ sp needed
 445 01e2 80BD     		pop	{r7, pc}
 446              	.L29:
 447              		.align	2
 448              	.L28:
 449 01e4 00100240 		.word	1073876992
 450 01e8 55550000 		.word	21845
 451 01ec 11100240 		.word	1073877009
 452 01f0 55555555 		.word	1431655765
 453              		.cfi_endproc
 454              	.LFE7:
 456              		.align	2
 457              		.global	graphic_read
 458              		.code	16
 459              		.thumb_func
 461              	graphic_read:
 462              	.LFB8:
 140:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 141:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** byte graphic_read(byte din_mamma)
 142:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 463              		.loc 1 142 0
 464              		.cfi_startproc
 465 01f4 80B5     		push	{r7, lr}
 466              		.cfi_def_cfa_offset 8
 467              		.cfi_offset 7, -8
 468              		.cfi_offset 14, -4
 469 01f6 84B0     		sub	sp, sp, #16
 470              		.cfi_def_cfa_offset 24
 471 01f8 00AF     		add	r7, sp, #0
 472              		.cfi_def_cfa_register 7
 473 01fa 0200     		movs	r2, r0
 474 01fc FB1D     		adds	r3, r7, #7
 475 01fe 1A70     		strb	r2, [r3]
 143:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 476              		.loc 1 143 0
 477 0200 4020     		movs	r0, #64
 478 0202 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 144:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_MODER) = 0x00005555;
 479              		.loc 1 144 0
 480 0206 1B4B     		ldr	r3, .L34
 481 0208 1B4A     		ldr	r2, .L34+4
 482 020a 1A60     		str	r2, [r3]
 145:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_RS);
 483              		.loc 1 145 0
 484 020c 0120     		movs	r0, #1
 485 020e FFF7FEFF 		bl	graphic_ctrl_bit_set
 146:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_RW);
 486              		.loc 1 146 0
 487 0212 0220     		movs	r0, #2
 488 0214 FFF7FEFF 		bl	graphic_ctrl_bit_set
 147:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(din_mamma);
 489              		.loc 1 147 0
 490 0218 FB1D     		adds	r3, r7, #7
 491 021a 1B78     		ldrb	r3, [r3]
 492 021c 1800     		movs	r0, r3
 493 021e FFF7FEFF 		bl	select_controller
 148:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_500ns();
 494              		.loc 1 148 0
 495 0222 FFF7FEFF 		bl	delay_500ns
 149:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_E);
 496              		.loc 1 149 0
 497 0226 4020     		movs	r0, #64
 498 0228 FFF7FEFF 		bl	graphic_ctrl_bit_set
 150:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_500ns();
 499              		.loc 1 150 0
 500 022c FFF7FEFF 		bl	delay_500ns
 151:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte value = *(GPIO_IDR_HIGH);
 501              		.loc 1 151 0
 502 0230 124A     		ldr	r2, .L34+8
 503 0232 0F23     		movs	r3, #15
 504 0234 FB18     		adds	r3, r7, r3
 505 0236 1278     		ldrb	r2, [r2]
 506 0238 1A70     		strb	r2, [r3]
 152:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 507              		.loc 1 152 0
 508 023a 4020     		movs	r0, #64
 509 023c FFF7FEFF 		bl	graphic_ctrl_bit_clear
 153:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_MODER) = 0x55555555;
 510              		.loc 1 153 0
 511 0240 0C4B     		ldr	r3, .L34
 512 0242 0F4A     		ldr	r2, .L34+12
 513 0244 1A60     		str	r2, [r3]
 154:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 155:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (din_mamma == B_CS1 || din_mamma == B_CS2){
 514              		.loc 1 155 0
 515 0246 FB1D     		adds	r3, r7, #7
 516 0248 1B78     		ldrb	r3, [r3]
 517 024a 082B     		cmp	r3, #8
 518 024c 03D0     		beq	.L31
 519              		.loc 1 155 0 is_stmt 0 discriminator 1
 520 024e FB1D     		adds	r3, r7, #7
 521 0250 1B78     		ldrb	r3, [r3]
 522 0252 102B     		cmp	r3, #16
 523 0254 06D1     		bne	.L32
 524              	.L31:
 156:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		select_controller(din_mamma);
 525              		.loc 1 156 0 is_stmt 1
 526 0256 FB1D     		adds	r3, r7, #7
 527 0258 1B78     		ldrb	r3, [r3]
 528 025a 1800     		movs	r0, r3
 529 025c FFF7FEFF 		bl	select_controller
 157:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_wait_ready();
 530              		.loc 1 157 0
 531 0260 FFF7FEFF 		bl	graphic_wait_ready
 532              	.L32:
 158:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 159:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 160:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	return value;
 533              		.loc 1 160 0
 534 0264 0F23     		movs	r3, #15
 535 0266 FB18     		adds	r3, r7, r3
 536 0268 1B78     		ldrb	r3, [r3]
 161:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 537              		.loc 1 161 0
 538 026a 1800     		movs	r0, r3
 539 026c BD46     		mov	sp, r7
 540 026e 04B0     		add	sp, sp, #16
 541              		@ sp needed
 542 0270 80BD     		pop	{r7, pc}
 543              	.L35:
 544 0272 C046     		.align	2
 545              	.L34:
 546 0274 00100240 		.word	1073876992
 547 0278 55550000 		.word	21845
 548 027c 11100240 		.word	1073877009
 549 0280 55555555 		.word	1431655765
 550              		.cfi_endproc
 551              	.LFE8:
 553              		.align	2
 554              		.global	graphic_read_data
 555              		.code	16
 556              		.thumb_func
 558              	graphic_read_data:
 559              	.LFB9:
 162:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 163:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** byte graphic_read_data(byte controller)
 164:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 560              		.loc 1 164 0
 561              		.cfi_startproc
 562 0284 80B5     		push	{r7, lr}
 563              		.cfi_def_cfa_offset 8
 564              		.cfi_offset 7, -8
 565              		.cfi_offset 14, -4
 566 0286 82B0     		sub	sp, sp, #8
 567              		.cfi_def_cfa_offset 16
 568 0288 00AF     		add	r7, sp, #0
 569              		.cfi_def_cfa_register 7
 570 028a 0200     		movs	r2, r0
 571 028c FB1D     		adds	r3, r7, #7
 572 028e 1A70     		strb	r2, [r3]
 165:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_read(controller); //Dummy read
 573              		.loc 1 165 0
 574 0290 FB1D     		adds	r3, r7, #7
 575 0292 1B78     		ldrb	r3, [r3]
 576 0294 1800     		movs	r0, r3
 577 0296 FFF7FEFF 		bl	graphic_read
 166:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	return graphic_read(controller);
 578              		.loc 1 166 0
 579 029a FB1D     		adds	r3, r7, #7
 580 029c 1B78     		ldrb	r3, [r3]
 581 029e 1800     		movs	r0, r3
 582 02a0 FFF7FEFF 		bl	graphic_read
 583 02a4 0300     		movs	r3, r0
 167:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 584              		.loc 1 167 0
 585 02a6 1800     		movs	r0, r3
 586 02a8 BD46     		mov	sp, r7
 587 02aa 02B0     		add	sp, sp, #8
 588              		@ sp needed
 589 02ac 80BD     		pop	{r7, pc}
 590              		.cfi_endproc
 591              	.LFE9:
 593 02ae C046     		.align	2
 594              		.global	graphic_write
 595              		.code	16
 596              		.thumb_func
 598              	graphic_write:
 599              	.LFB10:
 168:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 169:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write(byte value, byte controller)
 170:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 600              		.loc 1 170 0
 601              		.cfi_startproc
 602 02b0 80B5     		push	{r7, lr}
 603              		.cfi_def_cfa_offset 8
 604              		.cfi_offset 7, -8
 605              		.cfi_offset 14, -4
 606 02b2 82B0     		sub	sp, sp, #8
 607              		.cfi_def_cfa_offset 16
 608 02b4 00AF     		add	r7, sp, #0
 609              		.cfi_def_cfa_register 7
 610 02b6 0200     		movs	r2, r0
 611 02b8 FB1D     		adds	r3, r7, #7
 612 02ba 1A70     		strb	r2, [r3]
 613 02bc BB1D     		adds	r3, r7, #6
 614 02be 0A1C     		adds	r2, r1, #0
 615 02c0 1A70     		strb	r2, [r3]
 171:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_ODR_HIGH) = value;
 616              		.loc 1 171 0
 617 02c2 1B4A     		ldr	r2, .L41
 618 02c4 FB1D     		adds	r3, r7, #7
 619 02c6 1B78     		ldrb	r3, [r3]
 620 02c8 1370     		strb	r3, [r2]
 172:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(controller);
 621              		.loc 1 172 0
 622 02ca BB1D     		adds	r3, r7, #6
 623 02cc 1B78     		ldrb	r3, [r3]
 624 02ce 1800     		movs	r0, r3
 625 02d0 FFF7FEFF 		bl	select_controller
 173:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_250ns();
 626              		.loc 1 173 0
 627 02d4 FFF7FEFF 		bl	delay_250ns
 174:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_E);
 628              		.loc 1 174 0
 629 02d8 4020     		movs	r0, #64
 630 02da FFF7FEFF 		bl	graphic_ctrl_bit_set
 175:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_500ns();
 631              		.loc 1 175 0
 632 02de FFF7FEFF 		bl	delay_500ns
 176:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 633              		.loc 1 176 0
 634 02e2 4020     		movs	r0, #64
 635 02e4 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 177:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 178:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (controller & B_CS1)
 636              		.loc 1 178 0
 637 02e8 BB1D     		adds	r3, r7, #6
 638 02ea 1B78     		ldrb	r3, [r3]
 639 02ec 0822     		movs	r2, #8
 640 02ee 1340     		ands	r3, r2
 641 02f0 04D0     		beq	.L39
 179:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 180:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		select_controller(B_CS1);
 642              		.loc 1 180 0
 643 02f2 0820     		movs	r0, #8
 644 02f4 FFF7FEFF 		bl	select_controller
 181:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_wait_ready();
 645              		.loc 1 181 0
 646 02f8 FFF7FEFF 		bl	graphic_wait_ready
 647              	.L39:
 182:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 183:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (controller & B_CS2)
 648              		.loc 1 183 0
 649 02fc BB1D     		adds	r3, r7, #6
 650 02fe 1B78     		ldrb	r3, [r3]
 651 0300 1022     		movs	r2, #16
 652 0302 1340     		ands	r3, r2
 653 0304 04D0     		beq	.L40
 184:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 185:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		select_controller(B_CS2);
 654              		.loc 1 185 0
 655 0306 1020     		movs	r0, #16
 656 0308 FFF7FEFF 		bl	select_controller
 186:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_wait_ready();
 657              		.loc 1 186 0
 658 030c FFF7FEFF 		bl	graphic_wait_ready
 659              	.L40:
 187:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 188:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_wait_ready();
 660              		.loc 1 188 0
 661 0310 FFF7FEFF 		bl	graphic_wait_ready
 189:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	*(GPIO_ODR_HIGH) = 0;
 662              		.loc 1 189 0
 663 0314 064B     		ldr	r3, .L41
 664 0316 0022     		movs	r2, #0
 665 0318 1A70     		strb	r2, [r3]
 190:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_E);
 666              		.loc 1 190 0
 667 031a 4020     		movs	r0, #64
 668 031c FFF7FEFF 		bl	graphic_ctrl_bit_set
 191:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(0);
 669              		.loc 1 191 0
 670 0320 0020     		movs	r0, #0
 671 0322 FFF7FEFF 		bl	select_controller
 192:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 672              		.loc 1 192 0
 673 0326 C046     		nop
 674 0328 BD46     		mov	sp, r7
 675 032a 02B0     		add	sp, sp, #8
 676              		@ sp needed
 677 032c 80BD     		pop	{r7, pc}
 678              	.L42:
 679 032e C046     		.align	2
 680              	.L41:
 681 0330 15100240 		.word	1073877013
 682              		.cfi_endproc
 683              	.LFE10:
 685              		.align	2
 686              		.global	graphic_write_cmd
 687              		.code	16
 688              		.thumb_func
 690              	graphic_write_cmd:
 691              	.LFB11:
 193:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 194:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write_cmd(byte cmd, byte controller)
 195:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 692              		.loc 1 195 0
 693              		.cfi_startproc
 694 0334 80B5     		push	{r7, lr}
 695              		.cfi_def_cfa_offset 8
 696              		.cfi_offset 7, -8
 697              		.cfi_offset 14, -4
 698 0336 82B0     		sub	sp, sp, #8
 699              		.cfi_def_cfa_offset 16
 700 0338 00AF     		add	r7, sp, #0
 701              		.cfi_def_cfa_register 7
 702 033a 0200     		movs	r2, r0
 703 033c FB1D     		adds	r3, r7, #7
 704 033e 1A70     		strb	r2, [r3]
 705 0340 BB1D     		adds	r3, r7, #6
 706 0342 0A1C     		adds	r2, r1, #0
 707 0344 1A70     		strb	r2, [r3]
 196:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 708              		.loc 1 196 0
 709 0346 4020     		movs	r0, #64
 710 0348 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 197:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(controller); //Can be both
 711              		.loc 1 197 0
 712 034c BB1D     		adds	r3, r7, #6
 713 034e 1B78     		ldrb	r3, [r3]
 714 0350 1800     		movs	r0, r3
 715 0352 FFF7FEFF 		bl	select_controller
 198:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_RS);
 716              		.loc 1 198 0
 717 0356 0120     		movs	r0, #1
 718 0358 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 199:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_RW);
 719              		.loc 1 199 0
 720 035c 0220     		movs	r0, #2
 721 035e FFF7FEFF 		bl	graphic_ctrl_bit_clear
 200:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_250ns();
 722              		.loc 1 200 0
 723 0362 FFF7FEFF 		bl	delay_250ns
 201:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write(cmd, controller);
 724              		.loc 1 201 0
 725 0366 BB1D     		adds	r3, r7, #6
 726 0368 1A78     		ldrb	r2, [r3]
 727 036a FB1D     		adds	r3, r7, #7
 728 036c 1B78     		ldrb	r3, [r3]
 729 036e 1100     		movs	r1, r2
 730 0370 1800     		movs	r0, r3
 731 0372 FFF7FEFF 		bl	graphic_write
 202:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 732              		.loc 1 202 0
 733 0376 C046     		nop
 734 0378 BD46     		mov	sp, r7
 735 037a 02B0     		add	sp, sp, #8
 736              		@ sp needed
 737 037c 80BD     		pop	{r7, pc}
 738              		.cfi_endproc
 739              	.LFE11:
 741 037e C046     		.align	2
 742              		.global	graphic_write_data
 743              		.code	16
 744              		.thumb_func
 746              	graphic_write_data:
 747              	.LFB12:
 203:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 204:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_write_data(byte data, byte controller)
 205:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 748              		.loc 1 205 0
 749              		.cfi_startproc
 750 0380 80B5     		push	{r7, lr}
 751              		.cfi_def_cfa_offset 8
 752              		.cfi_offset 7, -8
 753              		.cfi_offset 14, -4
 754 0382 82B0     		sub	sp, sp, #8
 755              		.cfi_def_cfa_offset 16
 756 0384 00AF     		add	r7, sp, #0
 757              		.cfi_def_cfa_register 7
 758 0386 0200     		movs	r2, r0
 759 0388 FB1D     		adds	r3, r7, #7
 760 038a 1A70     		strb	r2, [r3]
 761 038c BB1D     		adds	r3, r7, #6
 762 038e 0A1C     		adds	r2, r1, #0
 763 0390 1A70     		strb	r2, [r3]
 206:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_E);
 764              		.loc 1 206 0
 765 0392 4020     		movs	r0, #64
 766 0394 FFF7FEFF 		bl	graphic_ctrl_bit_clear
 207:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	select_controller(controller); //Can be both
 767              		.loc 1 207 0
 768 0398 BB1D     		adds	r3, r7, #6
 769 039a 1B78     		ldrb	r3, [r3]
 770 039c 1800     		movs	r0, r3
 771 039e FFF7FEFF 		bl	select_controller
 208:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_set(B_RS);
 772              		.loc 1 208 0
 773 03a2 0120     		movs	r0, #1
 774 03a4 FFF7FEFF 		bl	graphic_ctrl_bit_set
 209:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_ctrl_bit_clear(B_RW);
 775              		.loc 1 209 0
 776 03a8 0220     		movs	r0, #2
 777 03aa FFF7FEFF 		bl	graphic_ctrl_bit_clear
 210:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	delay_250ns();
 778              		.loc 1 210 0
 779 03ae FFF7FEFF 		bl	delay_250ns
 211:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write(data, controller);
 780              		.loc 1 211 0
 781 03b2 BB1D     		adds	r3, r7, #6
 782 03b4 1A78     		ldrb	r2, [r3]
 783 03b6 FB1D     		adds	r3, r7, #7
 784 03b8 1B78     		ldrb	r3, [r3]
 785 03ba 1100     		movs	r1, r2
 786 03bc 1800     		movs	r0, r3
 787 03be FFF7FEFF 		bl	graphic_write
 212:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 788              		.loc 1 212 0
 789 03c2 C046     		nop
 790 03c4 BD46     		mov	sp, r7
 791 03c6 02B0     		add	sp, sp, #8
 792              		@ sp needed
 793 03c8 80BD     		pop	{r7, pc}
 794              		.cfi_endproc
 795              	.LFE12:
 797 03ca C046     		.align	2
 798              		.global	graphic_clear_screen
 799              		.code	16
 800              		.thumb_func
 802              	graphic_clear_screen:
 803              	.LFB13:
 213:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 214:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void graphic_clear_screen(void)
 215:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 804              		.loc 1 215 0
 805              		.cfi_startproc
 806 03cc 80B5     		push	{r7, lr}
 807              		.cfi_def_cfa_offset 8
 808              		.cfi_offset 7, -8
 809              		.cfi_offset 14, -4
 810 03ce 82B0     		sub	sp, sp, #8
 811              		.cfi_def_cfa_offset 16
 812 03d0 00AF     		add	r7, sp, #0
 813              		.cfi_def_cfa_register 7
 216:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int page;
 217:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	for (page = 0; page < 8; page++)
 814              		.loc 1 217 0
 815 03d2 0023     		movs	r3, #0
 816 03d4 7B60     		str	r3, [r7, #4]
 817 03d6 26E0     		b	.L46
 818              	.L49:
 819              	.LBB2:
 218:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 219:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		graphic_write_cmd(LCD_SET_PAGE | page, B_CS1 | B_CS2);
 820              		.loc 1 219 0
 821 03d8 7B68     		ldr	r3, [r7, #4]
 822 03da DBB2     		uxtb	r3, r3
 823 03dc 1A1C     		adds	r2, r3, #0
 824 03de 4823     		movs	r3, #72
 825 03e0 5B42     		rsbs	r3, r3, #0
 826 03e2 1343     		orrs	r3, r2
 827 03e4 DBB2     		uxtb	r3, r3
 828 03e6 DBB2     		uxtb	r3, r3
 829 03e8 1821     		movs	r1, #24
 830 03ea 1800     		movs	r0, r3
 831 03ec FFF7FEFF 		bl	graphic_write_cmd
 220:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 221:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		int addr;
 222:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		for (addr = 0; addr < 64; addr++)
 832              		.loc 1 222 0
 833 03f0 0023     		movs	r3, #0
 834 03f2 3B60     		str	r3, [r7]
 835 03f4 11E0     		b	.L47
 836              	.L48:
 223:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		{
 224:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_write_cmd(LCD_SET_ADD | addr, B_CS1 | B_CS2);
 837              		.loc 1 224 0 discriminator 3
 838 03f6 3B68     		ldr	r3, [r7]
 839 03f8 DBB2     		uxtb	r3, r3
 840 03fa 1A1C     		adds	r2, r3, #0
 841 03fc 4023     		movs	r3, #64
 842 03fe 1343     		orrs	r3, r2
 843 0400 DBB2     		uxtb	r3, r3
 844 0402 DBB2     		uxtb	r3, r3
 845 0404 1821     		movs	r1, #24
 846 0406 1800     		movs	r0, r3
 847 0408 FFF7FEFF 		bl	graphic_write_cmd
 225:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			graphic_write_data(0x00000000, B_CS1 | B_CS2);
 848              		.loc 1 225 0 discriminator 3
 849 040c 1821     		movs	r1, #24
 850 040e 0020     		movs	r0, #0
 851 0410 FFF7FEFF 		bl	graphic_write_data
 222:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		{
 852              		.loc 1 222 0 discriminator 3
 853 0414 3B68     		ldr	r3, [r7]
 854 0416 0133     		adds	r3, r3, #1
 855 0418 3B60     		str	r3, [r7]
 856              	.L47:
 222:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		{
 857              		.loc 1 222 0 is_stmt 0 discriminator 1
 858 041a 3B68     		ldr	r3, [r7]
 859 041c 3F2B     		cmp	r3, #63
 860 041e EADD     		ble	.L48
 861              	.LBE2:
 217:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 862              		.loc 1 217 0 is_stmt 1 discriminator 2
 863 0420 7B68     		ldr	r3, [r7, #4]
 864 0422 0133     		adds	r3, r3, #1
 865 0424 7B60     		str	r3, [r7, #4]
 866              	.L46:
 217:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 867              		.loc 1 217 0 is_stmt 0 discriminator 1
 868 0426 7B68     		ldr	r3, [r7, #4]
 869 0428 072B     		cmp	r3, #7
 870 042a D5DD     		ble	.L49
 226:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			
 227:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			/*
 228:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|x|x|x|x|x|x|x|x|x| <- addr
 229:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|||||||||||||||
 230:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|||||||||
 231:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x||||||
 232:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|||||
 233:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|0||||
 234:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|x|||||
 235:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|1|00|||
 236:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			
 237:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			|||||
 238:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			||||
 239:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			 */
 240:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		}
 241:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 242:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 243:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 871              		.loc 1 243 0 is_stmt 1
 872 042c C046     		nop
 873 042e BD46     		mov	sp, r7
 874 0430 02B0     		add	sp, sp, #8
 875              		@ sp needed
 876 0432 80BD     		pop	{r7, pc}
 877              		.cfi_endproc
 878              	.LFE13:
 880              		.align	2
 881              		.global	put_pixel
 882              		.code	16
 883              		.thumb_func
 885              	put_pixel:
 886              	.LFB14:
 244:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 245:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void put_pixel(byte x, byte y, byte set)
 246:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 887              		.loc 1 246 0
 888              		.cfi_startproc
 889 0434 90B5     		push	{r4, r7, lr}
 890              		.cfi_def_cfa_offset 12
 891              		.cfi_offset 4, -12
 892              		.cfi_offset 7, -8
 893              		.cfi_offset 14, -4
 894 0436 85B0     		sub	sp, sp, #20
 895              		.cfi_def_cfa_offset 32
 896 0438 00AF     		add	r7, sp, #0
 897              		.cfi_def_cfa_register 7
 898 043a 0400     		movs	r4, r0
 899 043c 0800     		movs	r0, r1
 900 043e 1100     		movs	r1, r2
 901 0440 FB1D     		adds	r3, r7, #7
 902 0442 221C     		adds	r2, r4, #0
 903 0444 1A70     		strb	r2, [r3]
 904 0446 BB1D     		adds	r3, r7, #6
 905 0448 021C     		adds	r2, r0, #0
 906 044a 1A70     		strb	r2, [r3]
 907 044c 7B1D     		adds	r3, r7, #5
 908 044e 0A1C     		adds	r2, r1, #0
 909 0450 1A70     		strb	r2, [r3]
 247:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (x > 127 || x < 0 || y > 63 || y < 0)
 910              		.loc 1 247 0
 911 0452 FB1D     		adds	r3, r7, #7
 912 0454 1B78     		ldrb	r3, [r3]
 913 0456 5BB2     		sxtb	r3, r3
 914 0458 002B     		cmp	r3, #0
 915 045a 00DA     		bge	.LCB748
 916 045c B5E0     		b	.L68	@long jump
 917              	.LCB748:
 918              		.loc 1 247 0 is_stmt 0 discriminator 1
 919 045e BB1D     		adds	r3, r7, #6
 920 0460 1B78     		ldrb	r3, [r3]
 921 0462 3F2B     		cmp	r3, #63
 922 0464 00D9     		bls	.LCB752
 923 0466 B0E0     		b	.L68	@long jump
 924              	.LCB752:
 248:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		return;
 249:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 250:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	//34 / 8 = vilken page = 4
 251:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	//34 % 8 = vilken bit  = 2 0x00000010
 252:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 253:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte page = y / 8;
 925              		.loc 1 253 0 is_stmt 1
 926 0468 0C23     		movs	r3, #12
 927 046a FB18     		adds	r3, r7, r3
 928 046c BA1D     		adds	r2, r7, #6
 929 046e 1278     		ldrb	r2, [r2]
 930 0470 D208     		lsrs	r2, r2, #3
 931 0472 1A70     		strb	r2, [r3]
 254:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte pagebit;
 255:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte controller = B_CS1;
 932              		.loc 1 255 0
 933 0474 0E23     		movs	r3, #14
 934 0476 FB18     		adds	r3, r7, r3
 935 0478 0822     		movs	r2, #8
 936 047a 1A70     		strb	r2, [r3]
 256:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte physic_x = x;
 937              		.loc 1 256 0
 938 047c 0D23     		movs	r3, #13
 939 047e FB18     		adds	r3, r7, r3
 940 0480 FA1D     		adds	r2, r7, #7
 941 0482 1278     		ldrb	r2, [r2]
 942 0484 1A70     		strb	r2, [r3]
 257:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 258:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	switch(y % 8)
 943              		.loc 1 258 0
 944 0486 BB1D     		adds	r3, r7, #6
 945 0488 1B78     		ldrb	r3, [r3]
 946 048a 0722     		movs	r2, #7
 947 048c 1340     		ands	r3, r2
 948 048e 072B     		cmp	r3, #7
 949 0490 2CD8     		bhi	.L54
 950 0492 9A00     		lsls	r2, r3, #2
 951 0494 4F4B     		ldr	r3, .L69
 952 0496 D318     		adds	r3, r2, r3
 953 0498 1B68     		ldr	r3, [r3]
 954 049a 9F46     		mov	pc, r3
 955              		.section	.rodata
 956              		.align	2
 957              	.L56:
 958 0000 9C040000 		.word	.L55
 959 0004 A6040000 		.word	.L57
 960 0008 B0040000 		.word	.L58
 961 000c BA040000 		.word	.L59
 962 0010 C4040000 		.word	.L60
 963 0014 CE040000 		.word	.L61
 964 0018 D8040000 		.word	.L62
 965 001c E2040000 		.word	.L63
 966              		.text
 967              	.L55:
 259:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 260:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 0:
 261:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 1;
 968              		.loc 1 261 0
 969 049c 0F23     		movs	r3, #15
 970 049e FB18     		adds	r3, r7, r3
 971 04a0 0122     		movs	r2, #1
 972 04a2 1A70     		strb	r2, [r3]
 262:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 973              		.loc 1 262 0
 974 04a4 22E0     		b	.L54
 975              	.L57:
 263:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 1:
 264:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 2;
 976              		.loc 1 264 0
 977 04a6 0F23     		movs	r3, #15
 978 04a8 FB18     		adds	r3, r7, r3
 979 04aa 0222     		movs	r2, #2
 980 04ac 1A70     		strb	r2, [r3]
 265:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 981              		.loc 1 265 0
 982 04ae 1DE0     		b	.L54
 983              	.L58:
 266:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 2:
 267:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 4;
 984              		.loc 1 267 0
 985 04b0 0F23     		movs	r3, #15
 986 04b2 FB18     		adds	r3, r7, r3
 987 04b4 0422     		movs	r2, #4
 988 04b6 1A70     		strb	r2, [r3]
 268:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 989              		.loc 1 268 0
 990 04b8 18E0     		b	.L54
 991              	.L59:
 269:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 3:
 270:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 8;
 992              		.loc 1 270 0
 993 04ba 0F23     		movs	r3, #15
 994 04bc FB18     		adds	r3, r7, r3
 995 04be 0822     		movs	r2, #8
 996 04c0 1A70     		strb	r2, [r3]
 271:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 997              		.loc 1 271 0
 998 04c2 13E0     		b	.L54
 999              	.L60:
 272:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 4:
 273:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 0x10;
 1000              		.loc 1 273 0
 1001 04c4 0F23     		movs	r3, #15
 1002 04c6 FB18     		adds	r3, r7, r3
 1003 04c8 1022     		movs	r2, #16
 1004 04ca 1A70     		strb	r2, [r3]
 274:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 1005              		.loc 1 274 0
 1006 04cc 0EE0     		b	.L54
 1007              	.L61:
 275:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 5:
 276:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 0x20;
 1008              		.loc 1 276 0
 1009 04ce 0F23     		movs	r3, #15
 1010 04d0 FB18     		adds	r3, r7, r3
 1011 04d2 2022     		movs	r2, #32
 1012 04d4 1A70     		strb	r2, [r3]
 277:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 1013              		.loc 1 277 0
 1014 04d6 09E0     		b	.L54
 1015              	.L62:
 278:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 6:
 279:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 0x40;
 1016              		.loc 1 279 0
 1017 04d8 0F23     		movs	r3, #15
 1018 04da FB18     		adds	r3, r7, r3
 1019 04dc 4022     		movs	r2, #64
 1020 04de 1A70     		strb	r2, [r3]
 280:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 1021              		.loc 1 280 0
 1022 04e0 04E0     		b	.L54
 1023              	.L63:
 281:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		case 7:
 282:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			pagebit = 0x80;
 1024              		.loc 1 282 0
 1025 04e2 0F23     		movs	r3, #15
 1026 04e4 FB18     		adds	r3, r7, r3
 1027 04e6 8022     		movs	r2, #128
 1028 04e8 1A70     		strb	r2, [r3]
 283:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 			break;
 1029              		.loc 1 283 0
 1030 04ea C046     		nop
 1031              	.L54:
 284:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 285:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (!set)
 1032              		.loc 1 285 0
 1033 04ec 7B1D     		adds	r3, r7, #5
 1034 04ee 1B78     		ldrb	r3, [r3]
 1035 04f0 002B     		cmp	r3, #0
 1036 04f2 06D1     		bne	.L64
 286:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		pagebit = ~pagebit; //00001000 clear -> xxxxxxx & 11110111
 1037              		.loc 1 286 0
 1038 04f4 0F23     		movs	r3, #15
 1039 04f6 FB18     		adds	r3, r7, r3
 1040 04f8 0F22     		movs	r2, #15
 1041 04fa BA18     		adds	r2, r7, r2
 1042 04fc 1278     		ldrb	r2, [r2]
 1043 04fe D243     		mvns	r2, r2
 1044 0500 1A70     		strb	r2, [r3]
 1045              	.L64:
 287:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 288:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if (x > 63)
 1046              		.loc 1 288 0
 1047 0502 FB1D     		adds	r3, r7, #7
 1048 0504 1B78     		ldrb	r3, [r3]
 1049 0506 3F2B     		cmp	r3, #63
 1050 0508 09D9     		bls	.L65
 289:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 290:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		controller = B_CS2;
 1051              		.loc 1 290 0
 1052 050a 0E23     		movs	r3, #14
 1053 050c FB18     		adds	r3, r7, r3
 1054 050e 1022     		movs	r2, #16
 1055 0510 1A70     		strb	r2, [r3]
 291:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		physic_x = x - 64;
 1056              		.loc 1 291 0
 1057 0512 0D23     		movs	r3, #13
 1058 0514 FB18     		adds	r3, r7, r3
 1059 0516 FA1D     		adds	r2, r7, #7
 1060 0518 1278     		ldrb	r2, [r2]
 1061 051a 403A     		subs	r2, r2, #64
 1062 051c 1A70     		strb	r2, [r3]
 1063              	.L65:
 292:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 293:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 294:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_SET_ADD | physic_x, controller);
 1064              		.loc 1 294 0
 1065 051e 0D23     		movs	r3, #13
 1066 0520 FB18     		adds	r3, r7, r3
 1067 0522 1B78     		ldrb	r3, [r3]
 1068 0524 4022     		movs	r2, #64
 1069 0526 1343     		orrs	r3, r2
 1070 0528 DAB2     		uxtb	r2, r3
 1071 052a 0E23     		movs	r3, #14
 1072 052c FB18     		adds	r3, r7, r3
 1073 052e 1B78     		ldrb	r3, [r3]
 1074 0530 1900     		movs	r1, r3
 1075 0532 1000     		movs	r0, r2
 1076 0534 FFF7FEFF 		bl	graphic_write_cmd
 295:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_SET_PAGE | page, controller);
 1077              		.loc 1 295 0
 1078 0538 0C23     		movs	r3, #12
 1079 053a FB18     		adds	r3, r7, r3
 1080 053c 1B78     		ldrb	r3, [r3]
 1081 053e 4822     		movs	r2, #72
 1082 0540 5242     		rsbs	r2, r2, #0
 1083 0542 1343     		orrs	r3, r2
 1084 0544 DAB2     		uxtb	r2, r3
 1085 0546 0E23     		movs	r3, #14
 1086 0548 FB18     		adds	r3, r7, r3
 1087 054a 1B78     		ldrb	r3, [r3]
 1088 054c 1900     		movs	r1, r3
 1089 054e 1000     		movs	r0, r2
 1090 0550 FFF7FEFF 		bl	graphic_write_cmd
 296:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	byte currentPixels = graphic_read_data(controller);
 1091              		.loc 1 296 0
 1092 0554 0B23     		movs	r3, #11
 1093 0556 FC18     		adds	r4, r7, r3
 1094 0558 0E23     		movs	r3, #14
 1095 055a FB18     		adds	r3, r7, r3
 1096 055c 1B78     		ldrb	r3, [r3]
 1097 055e 1800     		movs	r0, r3
 1098 0560 FFF7FEFF 		bl	graphic_read_data
 1099 0564 0300     		movs	r3, r0
 1100 0566 2370     		strb	r3, [r4]
 297:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 298:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_cmd(LCD_SET_ADD | physic_x, controller);
 1101              		.loc 1 298 0
 1102 0568 0D23     		movs	r3, #13
 1103 056a FB18     		adds	r3, r7, r3
 1104 056c 1B78     		ldrb	r3, [r3]
 1105 056e 4022     		movs	r2, #64
 1106 0570 1343     		orrs	r3, r2
 1107 0572 DAB2     		uxtb	r2, r3
 1108 0574 0E23     		movs	r3, #14
 1109 0576 FB18     		adds	r3, r7, r3
 1110 0578 1B78     		ldrb	r3, [r3]
 1111 057a 1900     		movs	r1, r3
 1112 057c 1000     		movs	r0, r2
 1113 057e FFF7FEFF 		bl	graphic_write_cmd
 299:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 300:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	if(!set)
 1114              		.loc 1 300 0
 1115 0582 7B1D     		adds	r3, r7, #5
 1116 0584 1B78     		ldrb	r3, [r3]
 1117 0586 002B     		cmp	r3, #0
 1118 0588 0AD1     		bne	.L66
 301:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		pagebit &= currentPixels;
 1119              		.loc 1 301 0
 1120 058a 0F23     		movs	r3, #15
 1121 058c FB18     		adds	r3, r7, r3
 1122 058e 0F22     		movs	r2, #15
 1123 0590 BA18     		adds	r2, r7, r2
 1124 0592 0B21     		movs	r1, #11
 1125 0594 7918     		adds	r1, r7, r1
 1126 0596 1278     		ldrb	r2, [r2]
 1127 0598 0978     		ldrb	r1, [r1]
 1128 059a 0A40     		ands	r2, r1
 1129 059c 1A70     		strb	r2, [r3]
 1130 059e 09E0     		b	.L67
 1131              	.L66:
 302:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	else
 303:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		pagebit |= currentPixels;
 1132              		.loc 1 303 0
 1133 05a0 0F23     		movs	r3, #15
 1134 05a2 FB18     		adds	r3, r7, r3
 1135 05a4 0F22     		movs	r2, #15
 1136 05a6 B918     		adds	r1, r7, r2
 1137 05a8 0B22     		movs	r2, #11
 1138 05aa BA18     		adds	r2, r7, r2
 1139 05ac 0978     		ldrb	r1, [r1]
 1140 05ae 1278     		ldrb	r2, [r2]
 1141 05b0 0A43     		orrs	r2, r1
 1142 05b2 1A70     		strb	r2, [r3]
 1143              	.L67:
 304:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 305:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	graphic_write_data(pagebit, controller);
 1144              		.loc 1 305 0
 1145 05b4 0E23     		movs	r3, #14
 1146 05b6 FB18     		adds	r3, r7, r3
 1147 05b8 1A78     		ldrb	r2, [r3]
 1148 05ba 0F23     		movs	r3, #15
 1149 05bc FB18     		adds	r3, r7, r3
 1150 05be 1B78     		ldrb	r3, [r3]
 1151 05c0 1100     		movs	r1, r2
 1152 05c2 1800     		movs	r0, r3
 1153 05c4 FFF7FEFF 		bl	graphic_write_data
 1154 05c8 00E0     		b	.L50
 1155              	.L68:
 248:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		
 1156              		.loc 1 248 0
 1157 05ca C046     		nop
 1158              	.L50:
 306:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }
 1159              		.loc 1 306 0
 1160 05cc BD46     		mov	sp, r7
 1161 05ce 05B0     		add	sp, sp, #20
 1162              		@ sp needed
 1163 05d0 90BD     		pop	{r4, r7, pc}
 1164              	.L70:
 1165 05d2 C046     		.align	2
 1166              	.L69:
 1167 05d4 00000000 		.word	.L56
 1168              		.cfi_endproc
 1169              	.LFE14:
 1171              		.align	2
 1172              		.global	drawSmiley
 1173              		.code	16
 1174              		.thumb_func
 1176              	drawSmiley:
 1177              	.LFB15:
 307:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 
 308:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** void drawSmiley(boolean set)
 309:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** {
 1178              		.loc 1 309 0
 1179              		.cfi_startproc
 1180 05d8 80B5     		push	{r7, lr}
 1181              		.cfi_def_cfa_offset 8
 1182              		.cfi_offset 7, -8
 1183              		.cfi_offset 14, -4
 1184 05da 8AB0     		sub	sp, sp, #40
 1185              		.cfi_def_cfa_offset 48
 1186 05dc 00AF     		add	r7, sp, #0
 1187              		.cfi_def_cfa_register 7
 1188 05de 0200     		movs	r2, r0
 1189 05e0 FB1D     		adds	r3, r7, #7
 1190 05e2 1A70     		strb	r2, [r3]
 310:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int eye_length = 15;
 1191              		.loc 1 310 0
 1192 05e4 0F23     		movs	r3, #15
 1193 05e6 3B62     		str	r3, [r7, #32]
 311:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int x_offset = 64;
 1194              		.loc 1 311 0
 1195 05e8 4023     		movs	r3, #64
 1196 05ea FB61     		str	r3, [r7, #28]
 312:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int y_offset = 10;
 1197              		.loc 1 312 0
 1198 05ec 0A23     		movs	r3, #10
 1199 05ee BB61     		str	r3, [r7, #24]
 313:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int width = 30;
 1200              		.loc 1 313 0
 1201 05f0 1E23     		movs	r3, #30
 1202 05f2 7B61     		str	r3, [r7, #20]
 314:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int mouth_offset = 10;
 1203              		.loc 1 314 0
 1204 05f4 0A23     		movs	r3, #10
 1205 05f6 3B61     		str	r3, [r7, #16]
 315:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int mouth_height = 30;
 1206              		.loc 1 315 0
 1207 05f8 1E23     		movs	r3, #30
 1208 05fa FB60     		str	r3, [r7, #12]
 316:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 317:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	int i;
 318:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 319:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	//Draw eyes
 320:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	for(i = 0; i < eye_length; i++)
 1209              		.loc 1 320 0
 1210 05fc 0023     		movs	r3, #0
 1211 05fe 7B62     		str	r3, [r7, #36]
 1212 0600 0FE0     		b	.L72
 1213              	.L73:
 321:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 322:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		put_pixel(x_offset, y_offset + i, set);
 1214              		.loc 1 322 0 discriminator 3
 1215 0602 FB69     		ldr	r3, [r7, #28]
 1216 0604 D8B2     		uxtb	r0, r3
 1217 0606 BB69     		ldr	r3, [r7, #24]
 1218 0608 DAB2     		uxtb	r2, r3
 1219 060a 7B6A     		ldr	r3, [r7, #36]
 1220 060c DBB2     		uxtb	r3, r3
 1221 060e D318     		adds	r3, r2, r3
 1222 0610 D9B2     		uxtb	r1, r3
 1223 0612 FB1D     		adds	r3, r7, #7
 1224 0614 1B78     		ldrb	r3, [r3]
 1225 0616 1A00     		movs	r2, r3
 1226 0618 FFF7FEFF 		bl	put_pixel
 320:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1227              		.loc 1 320 0 discriminator 3
 1228 061c 7B6A     		ldr	r3, [r7, #36]
 1229 061e 0133     		adds	r3, r3, #1
 1230 0620 7B62     		str	r3, [r7, #36]
 1231              	.L72:
 320:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1232              		.loc 1 320 0 is_stmt 0 discriminator 1
 1233 0622 7A6A     		ldr	r2, [r7, #36]
 1234 0624 3B6A     		ldr	r3, [r7, #32]
 1235 0626 9A42     		cmp	r2, r3
 1236 0628 EBDB     		blt	.L73
 323:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 324:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	for(i = 0; i < eye_length; i++)
 1237              		.loc 1 324 0 is_stmt 1
 1238 062a 0023     		movs	r3, #0
 1239 062c 7B62     		str	r3, [r7, #36]
 1240 062e 13E0     		b	.L74
 1241              	.L75:
 325:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 326:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		put_pixel(x_offset + width, y_offset + i, set);
 1242              		.loc 1 326 0 discriminator 3
 1243 0630 FB69     		ldr	r3, [r7, #28]
 1244 0632 DAB2     		uxtb	r2, r3
 1245 0634 7B69     		ldr	r3, [r7, #20]
 1246 0636 DBB2     		uxtb	r3, r3
 1247 0638 D318     		adds	r3, r2, r3
 1248 063a D8B2     		uxtb	r0, r3
 1249 063c BB69     		ldr	r3, [r7, #24]
 1250 063e DAB2     		uxtb	r2, r3
 1251 0640 7B6A     		ldr	r3, [r7, #36]
 1252 0642 DBB2     		uxtb	r3, r3
 1253 0644 D318     		adds	r3, r2, r3
 1254 0646 D9B2     		uxtb	r1, r3
 1255 0648 FB1D     		adds	r3, r7, #7
 1256 064a 1B78     		ldrb	r3, [r3]
 1257 064c 1A00     		movs	r2, r3
 1258 064e FFF7FEFF 		bl	put_pixel
 324:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1259              		.loc 1 324 0 discriminator 3
 1260 0652 7B6A     		ldr	r3, [r7, #36]
 1261 0654 0133     		adds	r3, r3, #1
 1262 0656 7B62     		str	r3, [r7, #36]
 1263              	.L74:
 324:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1264              		.loc 1 324 0 is_stmt 0 discriminator 1
 1265 0658 7A6A     		ldr	r2, [r7, #36]
 1266 065a 3B6A     		ldr	r3, [r7, #32]
 1267 065c 9A42     		cmp	r2, r3
 1268 065e E7DB     		blt	.L75
 327:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 328:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	
 329:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	//Draw mouth
 330:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	for(i = 0; i < width; i++)
 1269              		.loc 1 330 0 is_stmt 1
 1270 0660 0023     		movs	r3, #0
 1271 0662 7B62     		str	r3, [r7, #36]
 1272 0664 17E0     		b	.L76
 1273              	.L77:
 331:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 332:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 		put_pixel(x_offset + i, y_offset + eye_length + mouth_offset, set);
 1274              		.loc 1 332 0 discriminator 3
 1275 0666 FB69     		ldr	r3, [r7, #28]
 1276 0668 DAB2     		uxtb	r2, r3
 1277 066a 7B6A     		ldr	r3, [r7, #36]
 1278 066c DBB2     		uxtb	r3, r3
 1279 066e D318     		adds	r3, r2, r3
 1280 0670 D8B2     		uxtb	r0, r3
 1281 0672 BB69     		ldr	r3, [r7, #24]
 1282 0674 DAB2     		uxtb	r2, r3
 1283 0676 3B6A     		ldr	r3, [r7, #32]
 1284 0678 DBB2     		uxtb	r3, r3
 1285 067a D318     		adds	r3, r2, r3
 1286 067c DAB2     		uxtb	r2, r3
 1287 067e 3B69     		ldr	r3, [r7, #16]
 1288 0680 DBB2     		uxtb	r3, r3
 1289 0682 D318     		adds	r3, r2, r3
 1290 0684 D9B2     		uxtb	r1, r3
 1291 0686 FB1D     		adds	r3, r7, #7
 1292 0688 1B78     		ldrb	r3, [r3]
 1293 068a 1A00     		movs	r2, r3
 1294 068c FFF7FEFF 		bl	put_pixel
 330:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1295              		.loc 1 330 0 discriminator 3
 1296 0690 7B6A     		ldr	r3, [r7, #36]
 1297 0692 0133     		adds	r3, r3, #1
 1298 0694 7B62     		str	r3, [r7, #36]
 1299              	.L76:
 330:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	{
 1300              		.loc 1 330 0 is_stmt 0 discriminator 1
 1301 0696 7A6A     		ldr	r2, [r7, #36]
 1302 0698 7B69     		ldr	r3, [r7, #20]
 1303 069a 9A42     		cmp	r2, r3
 1304 069c E3DB     		blt	.L77
 333:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** 	}
 334:C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc674b90/graphicdisplay\graphicdisplay.c **** }...
 1305              		.loc 1 334 0 is_stmt 1
 1306 069e C046     		nop
 1307 06a0 BD46     		mov	sp, r7
 1308 06a2 0AB0     		add	sp, sp, #40
 1309              		@ sp needed
 1310 06a4 80BD     		pop	{r7, pc}
 1311              		.cfi_endproc
 1312              	.LFE15:
 1314              	.Letext0:
 1315              		.file 2 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc6
 1316              		.file 3 "C:/Users/alebabu/Documents/repos/pontusstjerna-mop-c004dc674b90/pontusstjerna-mop-c004dc6
