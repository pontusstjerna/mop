#ifndef GRAPHICDISPLAY_H
#define GRAPHICDISPLAY_H

typedef unsigned char byte;
typedef unsigned volatile char vbyte;

#define GPIO_MODER (unsigned volatile long*) 0x40021000
#define GPIO_OTYPER (unsigned volatile short*) 0x40021004;
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008;
#define GPIO_PUPDR (unsigned volatile long*) 0x4002100C;
#define GPIO_IDR_LOW (vbyte*) 0x40021010
#define GPIO_IDR_HIGH (vbyte*) 0x40021011
#define GPIO_ODR_LOW (vbyte*) 0x40021014
#define GPIO_ODR_HIGH (vbyte*) 0x40021015

#define B_E 0x40 //010000000
#define B_RST 0x20
#define B_RS 0x01
#define B_CS2 0x10
#define B_CS1 8
#define B_SELECT 0x4 //0000100
#define B_RW 0x2 //00000010

#define LCD_ON 0x3F
#define LCD_OFF 0x3E
#define LCD_SET_ADD 0x40
#define LCD_SET_PAGE 0xB8
#define LCD_DISP_START 0xC0
#define LCD_BUSY 0x80 //10000000

// The graphic declarations
void graphic_ctrl_bit_set(byte);
void graphic_ctrl_bit_clear(byte);
void select_controller(byte);
void graphic_wait_ready(void);
byte graphic_read(byte);
byte graphic_read_data(byte);
void graphic_write(byte, byte);
void graphic_write_cmd(byte, byte);
void graphic_write_data(byte, byte);
void put_pixel(byte, byte, byte);

void drawSmiley(boolean);

void graphic_clear_screen(void);

typedef unsigned char byte;

#define B_E 0x40 //010000000
#define B_RST 0x20
#define B_RS 0x01
#define B_CS2 0x10
#define B_CS1 8
#define B_SELECT 0x4 //0000100
#define B_RW 0x2 //00000010

#define LCD_ON 0x3F
#define LCD_OFF 0x3E
#define LCD_SET_ADD 0x40
#define LCD_SET_PAGE 0xB8
#define LCD_DISP_START 0xC0
#define LCD_BUSY 0x80 //10000000


/*
 * 	startup.c
 *
 */
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );


// The graphic declarations
void graphic_ctrl_bit_set(byte);
void graphic_ctrl_bit_clear(byte);
void select_controller(byte);
void graphic_wait_ready(void);
byte graphic_read(byte);
byte graphic_read_data(byte);
void graphic_write(byte, byte);
void graphic_write_cmd(byte, byte);
void graphic_write_data(byte, byte);
void put_pixel(byte, byte, byte);

void drawSmiley(boolean);

void graphic_clear_screen(void);

#endif //GRAPHICDISPLAY_H