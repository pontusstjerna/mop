#ifndef DELAY_H
#define DELAY_H

#define STK_CTRL (unsigned volatile long *) 0xE000E010
#define STK_LOAD (unsigned volatile long *) 0xE000E014
#define STK_VAL (unsigned volatile long *) 0xE000E018

void delay_500ns(void);
void delay_250ns(void);
void delay_mikro(unsigned int);
void delay_milli(unsigned int);

#endif //DELAY_H