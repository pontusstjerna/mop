#include "delay.h"

void delay_milli(unsigned int ms){
	#ifdef SIMULATOR
		ms = 5; //Some weird bug here with division
	#endif
	
	int i;
	for(i = 0; i < 1000; i++){
		delay_mikro(ms);
	}
}

void delay_mikro(unsigned int us){
	unsigned int i;
	
	for(i = us; i > 0; i--){
		delay_250ns();
		delay_250ns();
		delay_250ns();
		delay_250ns();
	}
}

void delay_500ns(void)
{
	delay_250ns();
	delay_250ns();
}

void delay_250ns(void){
	*(STK_CTRL) = 0;
	*(STK_LOAD) = 168/4 - 1;
	*(STK_VAL) = 0;
	*(STK_CTRL) = 0x5; //ends with 0101
	while((*(STK_CTRL) & 0x10000) == 0); //wait for bit 16 to be 1
	*(STK_CTRL) = 0;
	
	//Kontrollregister:
	/*Bit 16: 1 om klar
	 * Bit 2: 1 efter RESET, använd systemklocka om 1 annars systemklocka/8
	 * Bit 1: Aktivera avbrott
	 * Bit 0: ENABLE
	 * DET ÄR DISPLAYEN SOM HAR ALL MAGI :o
	 */
}

