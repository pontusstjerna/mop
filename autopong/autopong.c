#include "delay.h"
#include "boolean.h"
#include "graphicdisplay.h"

void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void startup ( void )
{
asm volatile(
	"LDR R0, =0x40023830\n"
	"MOV R1, #0x18\n"
	"STR R1, [R0]\n"
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

typedef enum {x = 0, y = 1} POINT;

#define MAX_POINTS 20

#define GPIO_D_MODER (unsigned long*) 0x40020C00
#define GPIO_D_PUPDR (unsigned long*) 0x40020C0C
#define GPIO_D_IPR_HIGH (unsigned char*) 0x40020C11
#define GPIO_D_OPR_HIGH (unsigned char*) 0x40020C15


typedef struct tGeometry
{
	int numpoints, width, height;
	volatile int pixels[MAX_POINTS][2];
} GEOMETRY,*PGEOMETRY;



typedef struct TOBJ
{
	PGEOMETRY geo;
	int dirx,diry;
	int posx, posy;
	void (* draw ) (struct TOBJ *);
	void (* clear) (struct TOBJ *);
	void (* move) (struct TOBJ *);
	void (* setDir) (struct TOBJ *, int, int);
} OBJECT, *POBJECT;

void setDir(POBJECT obj, int speedX, int speedY)
{
	obj->dirx = speedX;
	obj->diry = speedY;
}

void draw(POBJECT obj)
{
	int i;
	for(i = 0; i < obj->geo->numpoints; i++)
	{
		put_pixel(obj->posx + obj->geo->pixels[i][x], obj->posy + obj->geo->pixels[i][y], true);
	}
}

void clear(POBJECT obj)
{
	int i;
	for(i = 0; i < obj->geo->numpoints; i++)
	{
		put_pixel(obj->posx + obj->geo->pixels[i][x], obj->posy + obj->geo->pixels[i][y], false);
	}
}

void move(POBJECT obj)
{
	//OBJECT kaka = *obj; kan använda punkter
	clear(obj);
	int newX = obj->posx + obj->dirx; 
	int newY = obj->posy + obj->diry;
	int newDirX = obj->dirx;
	int newDirY = obj->diry;
	
	if(newX < 0 || newX + obj->geo->width > 127) //Walls
		newDirX = -obj->dirx; 
	if(newY < 0 || newY + obj->geo->height > 63) //Floor/roof
		newDirY = -obj->diry;
		
	obj->dirx = newDirX;
	obj->diry = newDirY;
	
	obj->posx = obj->posx + newDirX;
	obj->posy = obj->posy + newDirY;
	obj->draw(obj);
}
/*
static GEOMETRY ball_geometry =
{
	12,
	4,4,
	{{0,0}, {1,0}, {2,0}, {3,0},
	{0,1},                {3,1},
	{0,2}, 	 	          {3,2},
	{0,3}, {1,3}, {2,3},  {3,3}}
};*/


static GEOMETRY ball_geometry =
{
	12,
	4,4,
	{{0,0}               ,{4,0},
	{0,1},                {4,1},
	     	 	          
	      {1,3}, {3,3}, {2,4}  }
};

//Gör en boll
static OBJECT ball = 
{
	&ball_geometry,
	0,0,
	0,0,
	draw,
	clear,
	move,
	setDir
};

char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};

void activateRow(int i){
	unsigned char bits;
	switch(i){
		case 0: //Set bits to 0001xxxx
			bits = 0x10;
			break;
		case 1: //Set bits to 0010xxxx
			bits = 0x20;
			break;
		case 2: //Set bits to 0100xxxx
			bits = 0x40;
			break;
		case 3: //Set bits to 1000xxxx
			bits = 0x80;
			break;
	}
	
	//First half is output other half is input
	unsigned char setBits = *(GPIO_D_IPR_HIGH);
	
	//Set first 4 to 0 and keep the others
	setBits = setBits & 0x0F;
	
	//Now set the first 4 to wanted and keep others
	setBits = setBits | bits;
	
	*(GPIO_D_OPR_HIGH) = setBits;
}
	
int readColumn(void){
	unsigned char bits = *(GPIO_D_IPR_HIGH);
	int i;
	for(i = 0; i < 4; i++){
		if(bits % 2 == 1){ //If odd
			return i;
		}
		bits = bits>>1;
	}
	return -1;
}

unsigned char keyb(void){
	int i;
	for(i = 0; i < 4; i++){
		activateRow(i);
		int column = readColumn();
		if(column != -1){
			return keyValues[i][column];
		}
	}
	return 0xFF;
}

/*
void main()
{
	init_app();
	graphic_initialize();
	//graphic_clear_screen();
	
	POBJECT myBall = &ball;
	myBall->setDir(myBall, 2,1);

	while(true)
	{
		#ifdef SIMULATOR 
			delay_milli(10);
		#endif
		delay_milli(50);
		myBall->move(myBall);
	}
	
	
}*/
  
  
  
void main() 
{
	char c;
	POBJECT p = &ball;
	init_app();
	*GPIO_D_MODER = 0x55005555;
	*(GPIO_D_PUPDR) = 0x00AA0000;
	graphic_initialize();
	graphic_clear_screen();
	while(true)
	{
		p->move(p);
		delay_milli(40);
		c = keyb();
		switch(c)
		{
			case '1': p->setDir(p,-1,-1); break;
			case '3': p->setDir(p,1,-1); break;
			case '7': p->setDir(p,-1,1); break;
			case '9': p->setDir(p,1,1); break;
			case '6': 
				p->setDir(p,2,0);
				break;
			case '4': 
				p->setDir(p,-2,0);
				break;
			case '2': 
				p->setDir(p,0,-2);
				break;
			case '8': 
				p->setDir(p,0,2);
				break;
			case 0xFF:
				p->setDir(p,0,0);
				break;
		}
	}
}

