#include "delay.h"
#include "boolean.h"
#include "graphicdisplay.h"


void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );


// The graphic declarations
void graphic_ctrl_bit_set(byte);
void graphic_ctrl_bit_clear(byte);
void select_controller(byte);
void graphic_wait_ready(void);
byte graphic_read(byte);
byte graphic_read_data(byte);
void graphic_write(byte, byte);
void graphic_write_cmd(byte, byte);
void graphic_write_data(byte, byte);
void put_pixel(byte, byte, byte);

void drawSmiley(boolean);

void graphic_clear_screen(void);

void init_app()
{
	*(GPIO_MODER) = 0x55555555;
}

void graphic_initialize(void)
{
	graphic_ctrl_bit_set(B_E);
	delay_mikro(10);
	graphic_ctrl_bit_clear(B_CS1 | B_CS2 | B_RST | B_E);
	#ifndef SIMULATOR
		delay_milli(30);
	#elif
		delay_milli(3);
	#endif
	graphic_ctrl_bit_set(B_RST);
	graphic_write_cmd(LCD_OFF, B_CS1 | B_CS2);
	graphic_write_cmd(LCD_ON, B_CS1 | B_CS2);
	graphic_write_cmd(LCD_DISP_START, B_CS1 | B_CS2);
	graphic_write_cmd(LCD_SET_ADD, B_CS1 | B_CS2);
	graphic_write_cmd(LCD_SET_PAGE, B_CS1 | B_CS2);
	
	graphic_clear_screen();
	select_controller(0);
}

void graphic_ctrl_bit_set(byte x) //Super tested
{
	byte bits = *(GPIO_ODR_LOW);
	bits &= ~B_SELECT;
	*(GPIO_ODR_LOW) = bits | (x);
}

void graphic_ctrl_bit_clear(byte x) //Super tested
{
	byte bits = *(GPIO_ODR_LOW);
	*(GPIO_ODR_LOW) = bits & (~x & ~B_SELECT);
}

void select_controller(byte controller) //Über tested
{
	switch(controller)
	{
		case B_CS1:
			graphic_ctrl_bit_set(B_CS1);
			graphic_ctrl_bit_clear(B_CS2);
			break;
        case B_CS2:
			graphic_ctrl_bit_clear(B_CS1);
			graphic_ctrl_bit_set(B_CS2);
			break;
		case (B_CS1 | B_CS2):
			graphic_ctrl_bit_set(B_CS1);
			graphic_ctrl_bit_set(B_CS2);
			break;
		case 0:
			graphic_ctrl_bit_clear(B_CS1);
			graphic_ctrl_bit_clear(B_CS2);
			break;
	}
}

void graphic_wait_ready(void)
{
	graphic_ctrl_bit_clear(B_E);
	*(GPIO_MODER) = 0x00005555;
	graphic_ctrl_bit_clear(B_RS);
	graphic_ctrl_bit_set(B_RW);
	delay_500ns();
	while(true)
	{
		graphic_ctrl_bit_set(B_E);
		delay_500ns();
		graphic_ctrl_bit_clear(B_E);
		if(!(*(GPIO_IDR_HIGH) & LCD_BUSY ))
		{
			break;
		}
		delay_500ns();	
	}
	graphic_ctrl_bit_set(B_E);
	*(GPIO_MODER) = 0x55555555;
}

byte graphic_read(byte din_mamma)
{
	graphic_ctrl_bit_clear(B_E);
	*(GPIO_MODER) = 0x00005555;
	graphic_ctrl_bit_set(B_RS);
	graphic_ctrl_bit_set(B_RW);
	select_controller(din_mamma);
	delay_500ns();
	graphic_ctrl_bit_set(B_E);
	delay_500ns();
	byte value = *(GPIO_IDR_HIGH);
	graphic_ctrl_bit_clear(B_E);
	*(GPIO_MODER) = 0x55555555;
	
	if (din_mamma == B_CS1 || din_mamma == B_CS2){
		select_controller(din_mamma);
		graphic_wait_ready();
	}
	
	return value;
}

byte graphic_read_data(byte controller)
{
	graphic_read(controller); //Dummy read
	return graphic_read(controller);
}

void graphic_write(byte value, byte controller)
{
	*(GPIO_ODR_HIGH) = value;
	select_controller(controller);
	delay_250ns();
	graphic_ctrl_bit_set(B_E);
	delay_500ns();
	graphic_ctrl_bit_clear(B_E);
	
	if (controller & B_CS1)
	{
		select_controller(B_CS1);
		graphic_wait_ready();
	}
	if (controller & B_CS2)
	{
		select_controller(B_CS2);
		graphic_wait_ready();
	}
	graphic_wait_ready();
	*(GPIO_ODR_HIGH) = 0;
	graphic_ctrl_bit_set(B_E);
	select_controller(0);
}

void graphic_write_cmd(byte cmd, byte controller)
{
	graphic_ctrl_bit_clear(B_E);
	select_controller(controller); //Can be both
	graphic_ctrl_bit_clear(B_RS);
	graphic_ctrl_bit_clear(B_RW);
	delay_250ns();
	graphic_write(cmd, controller);
}

void graphic_write_data(byte data, byte controller)
{
	graphic_ctrl_bit_clear(B_E);
	select_controller(controller); //Can be both
	graphic_ctrl_bit_set(B_RS);
	graphic_ctrl_bit_clear(B_RW);
	delay_250ns();
	graphic_write(data, controller);
}

void graphic_clear_screen(void)
{
	int page;
	for (page = 0; page < 8; page++)
	{
		graphic_write_cmd(LCD_SET_PAGE | page, B_CS1 | B_CS2);
		
		int addr;
		for (addr = 0; addr < 64; addr++)
		{
			graphic_write_cmd(LCD_SET_ADD | addr, B_CS1 | B_CS2);
			graphic_write_data(0x00000000, B_CS1 | B_CS2);
			
			/*
			|x|x|x|x|x|x|x|x|x|x| <- addr
			|x|||||||||||||||
			|x|||||||||
			|x||||||
			|x|||||
			|x|0||||
			|x|||||
			|1|00|||
			
			|||||
			||||
			 */
		}
		
	}
}

void put_pixel(byte x, byte y, byte set)
{
	if (x > 127 || x < 0 || y > 63 || y < 0)
		return;
		
	//34 / 8 = vilken page = 4
	//34 % 8 = vilken bit  = 2 0x00000010
		
	byte page = y / 8;
	byte pagebit;
	byte controller = B_CS1;
	byte physic_x = x;
	
	switch(y % 8)
	{
		case 0:
			pagebit = 1;
			break;
		case 1:
			pagebit = 2;
			break;
		case 2:
			pagebit = 4;
			break;
		case 3:
			pagebit = 8;
			break;
		case 4:
			pagebit = 0x10;
			break;
		case 5:
			pagebit = 0x20;
			break;
		case 6:
			pagebit = 0x40;
			break;
		case 7:
			pagebit = 0x80;
			break;
	}
	if (!set)
		pagebit = ~pagebit; //00001000 clear -> xxxxxxx & 11110111
		
	if (x > 63)
	{
		controller = B_CS2;
		physic_x = x - 64;
	}
	
	graphic_write_cmd(LCD_SET_ADD | physic_x, controller);
	graphic_write_cmd(LCD_SET_PAGE | page, controller);
	byte currentPixels = graphic_read_data(controller);
	
	graphic_write_cmd(LCD_SET_ADD | physic_x, controller);
	
	if(!set)
		pagebit &= currentPixels;
	else
		pagebit |= currentPixels;
		
	graphic_write_data(pagebit, controller);
}