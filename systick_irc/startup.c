#include "boolean.h"
#ifdef SIMULATOR
#define DELAY_COUNT 100
#else
#define DELAY_COUNT 1000000
#endif


/*
 * 	startup.c
 *
 */

typedef unsigned volatile char vbyte; 
typedef unsigned volatile long u_long;
 /*
#define GPIO_MODER (unsigned volatile long*) 0x40021000
#define GPIO_OTYPER (unsigned volatile short*) 0x40021004
#define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008
#define GPIO_PUPDR (unsigned volatile long*) 0x4002100C
#define GPIO_IDR_LOW (vbyte*) 0x40021010
#define GPIO_IDR_HIGH (vbyte*) 0x40021011
#define GPIO_ODR_LOW (vbyte*) 0x40021014
#define GPIO_ODR_HIGH (vbyte*) 0x40021015
*/
#define STK_CTRL (unsigned volatile long*) 0xE000E010
#define STK_LOAD (unsigned volatile long*) 0xE000E014
#define STK_VAl (unsigned volatile long*) 0xE000E018
#define STK_CALIB (unsigned volatile long*) 0xE000E01C

typedef struct GPIO_T 
{
    u_long MODER;
    u_long OTYPER;
    u_long OSPEEDR;
    u_long PUPDR;
    union 
    {
        u_long IDR;
        struct
        {
            vbyte IDR_LOW;
            vbyte IDR_HIGH;
        };
    };
    union 
    {
        u_long ODR;
        struct
        {
            vbyte ODR_LOW;
            vbyte ODR_HIGH;
        };
    };
} GPIO, *P_GPIO;

#define GPIO_D (*((volatile GPIO*) 0x40020C00))
 
void init_app(void);

void delay250(int);

void delay(int);
 
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void startup ( void )
{
asm volatile(
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void init_app(void)
{
//    *(GPIO_MODER) = 0x55555555;
}

void delay250(int value)
{
    // initiera avbrottsvektor
 //   *(STK_CTRL) &= FFFEFFFF;
    
}

void delay(int value)
{
    // initiera avbrottsvektor
//    *(STK_CTRL) &= FFFEFFFF;
    
}

void main(void)
{
//    init_app();
//    while(true)
//    {
 //       *(GPIO_ODR_HIGH) = 0x0000;
 //       delay(DELAY_COUNT);
 //       *(GPIO_ODR_HIGH) = 0x0000;
 //       delay(DELAY_COUNT);
 //   }
}

