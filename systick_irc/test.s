   1              		.syntax unified
   2              		.arch armv6-m
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 0
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.syntax unified
  15              		.file	"startup.c"
  16              		.text
  17              	.Ltext0:
  18              		.cfi_sections	.debug_frame
  19              		.section	.start_section,"ax",%progbits
  20              		.align	2
  21              		.global	startup
  22              		.code	16
  23              		.thumb_func
  25              	startup:
  26              	.LFB0:
  27              		.file 1 "/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c"
   1:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #include "boolean.h"
   2:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #ifdef SIMULATOR
   3:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define DELAY_COUNT 100
   4:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #else
   5:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define DELAY_COUNT 1000000
   6:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #endif
   7:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
   8:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
   9:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** /*
  10:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  * 	startup.c
  11:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  *
  12:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  */
  13:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  14:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** typedef unsigned volatile char vbyte; 
  15:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** typedef unsigned volatile long u_long;
  16:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  /*
  17:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_MODER (unsigned volatile long*) 0x40021000
  18:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_OTYPER (unsigned volatile short*) 0x40021004
  19:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_OSPEEDR (unsigned volatile long*) 0x40021008
  20:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_PUPDR (unsigned volatile long*) 0x4002100C
  21:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_IDR_LOW (vbyte*) 0x40021010
  22:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_IDR_HIGH (vbyte*) 0x40021011
  23:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_ODR_LOW (vbyte*) 0x40021014
  24:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_ODR_HIGH (vbyte*) 0x40021015
  25:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** */
  26:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define STK_CTRL (unsigned volatile long*) 0xE000E010
  27:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define STK_LOAD (unsigned volatile long*) 0xE000E014
  28:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define STK_VAl (unsigned volatile long*) 0xE000E018
  29:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define STK_CALIB (unsigned volatile long*) 0xE000E01C
  30:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  31:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** typedef struct GPIO_T 
  32:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
  33:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     u_long MODER;
  34:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     u_long OTYPER;
  35:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     u_long OSPEEDR;
  36:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     u_long PUPDR;
  37:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     union 
  38:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     {
  39:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         u_long IDR;
  40:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         struct
  41:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         {
  42:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****             vbyte IDR_LOW;
  43:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****             vbyte IDR_HIGH;
  44:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         };
  45:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     };
  46:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     union 
  47:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     {
  48:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         u_long ODR;
  49:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         struct
  50:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         {
  51:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****             vbyte ODR_LOW;
  52:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****             vbyte ODR_HIGH;
  53:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****         };
  54:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     };
  55:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** } GPIO, *P_GPIO;
  56:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  57:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** #define GPIO_D (*((volatile GPIO*) 0x40020C00))
  58:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  
  59:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void init_app(void);
  60:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  61:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void delay250(int);
  62:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  63:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void delay(int);
  64:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  
  65:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );
  66:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  67:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void startup ( void )
  68:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
  28              		.loc 1 68 0
  29              		.cfi_startproc
  30              		@ Naked Function: prologue and epilogue provided by programmer.
  31              		@ args = 0, pretend = 0, frame = 0
  32              		@ frame_needed = 1, uses_anonymous_args = 0
  69:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** asm volatile(
  33              		.loc 1 69 0
  34              		.syntax divided
  35              	@ 69 "/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c" 1
  36 0000 0248     		 LDR R0,=0x2001C000
  37 0002 8546     	 MOV SP,R0
  38 0004 FFF7FEFF 	 BL main
  39 0008 FEE7     	.L1: B .L1
  40              	
  41              	@ 0 "" 2
  70:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 	" LDR R0,=0x2001C000\n"		/* set stack */
  71:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 	" MOV SP,R0\n"
  72:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 	" BL main\n"				/* call main */
  73:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 	".L1: B .L1\n"				/* never return */
  74:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 	) ;
  75:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** }
  42              		.loc 1 75 0
  43              		.thumb
  44              		.syntax unified
  45 000a C046     		nop
  46              		.cfi_endproc
  47              	.LFE0:
  49              		.text
  50              		.align	2
  51              		.global	init_app
  52              		.code	16
  53              		.thumb_func
  55              	init_app:
  56              	.LFB1:
  76:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  77:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void init_app(void)
  78:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
  57              		.loc 1 78 0
  58              		.cfi_startproc
  59              		@ args = 0, pretend = 0, frame = 0
  60              		@ frame_needed = 1, uses_anonymous_args = 0
  61 0000 80B5     		push	{r7, lr}
  62              		.cfi_def_cfa_offset 8
  63              		.cfi_offset 7, -8
  64              		.cfi_offset 14, -4
  65 0002 00AF     		add	r7, sp, #0
  66              		.cfi_def_cfa_register 7
  79:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** //    *(GPIO_MODER) = 0x55555555;
  80:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** }
  67              		.loc 1 80 0
  68 0004 C046     		nop
  69 0006 BD46     		mov	sp, r7
  70              		@ sp needed
  71 0008 80BD     		pop	{r7, pc}
  72              		.cfi_endproc
  73              	.LFE1:
  75 000a C046     		.align	2
  76              		.global	delay250
  77              		.code	16
  78              		.thumb_func
  80              	delay250:
  81              	.LFB2:
  81:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  82:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void delay250(int value)
  83:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
  82              		.loc 1 83 0
  83              		.cfi_startproc
  84              		@ args = 0, pretend = 0, frame = 8
  85              		@ frame_needed = 1, uses_anonymous_args = 0
  86 000c 80B5     		push	{r7, lr}
  87              		.cfi_def_cfa_offset 8
  88              		.cfi_offset 7, -8
  89              		.cfi_offset 14, -4
  90 000e 82B0     		sub	sp, sp, #8
  91              		.cfi_def_cfa_offset 16
  92 0010 00AF     		add	r7, sp, #0
  93              		.cfi_def_cfa_register 7
  94 0012 7860     		str	r0, [r7, #4]
  84:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     // initiera avbrottsvektor
  85:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //   *(STK_CTRL) &= FFFEFFFF;
  86:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     
  87:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** }
  95              		.loc 1 87 0
  96 0014 C046     		nop
  97 0016 BD46     		mov	sp, r7
  98 0018 02B0     		add	sp, sp, #8
  99              		@ sp needed
 100 001a 80BD     		pop	{r7, pc}
 101              		.cfi_endproc
 102              	.LFE2:
 104              		.align	2
 105              		.global	delay
 106              		.code	16
 107              		.thumb_func
 109              	delay:
 110              	.LFB3:
  88:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  89:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void delay(int value)
  90:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
 111              		.loc 1 90 0
 112              		.cfi_startproc
 113              		@ args = 0, pretend = 0, frame = 8
 114              		@ frame_needed = 1, uses_anonymous_args = 0
 115 001c 80B5     		push	{r7, lr}
 116              		.cfi_def_cfa_offset 8
 117              		.cfi_offset 7, -8
 118              		.cfi_offset 14, -4
 119 001e 82B0     		sub	sp, sp, #8
 120              		.cfi_def_cfa_offset 16
 121 0020 00AF     		add	r7, sp, #0
 122              		.cfi_def_cfa_register 7
 123 0022 7860     		str	r0, [r7, #4]
  91:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     // initiera avbrottsvektor
  92:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** //    *(STK_CTRL) &= FFFEFFFF;
  93:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****     
  94:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** }
 124              		.loc 1 94 0
 125 0024 C046     		nop
 126 0026 BD46     		mov	sp, r7
 127 0028 02B0     		add	sp, sp, #8
 128              		@ sp needed
 129 002a 80BD     		pop	{r7, pc}
 130              		.cfi_endproc
 131              	.LFE3:
 133              		.align	2
 134              		.global	main
 135              		.code	16
 136              		.thumb_func
 138              	main:
 139              	.LFB4:
  95:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** 
  96:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** void main(void)
  97:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** {
 140              		.loc 1 97 0
 141              		.cfi_startproc
 142              		@ args = 0, pretend = 0, frame = 0
 143              		@ frame_needed = 1, uses_anonymous_args = 0
 144 002c 80B5     		push	{r7, lr}
 145              		.cfi_def_cfa_offset 8
 146              		.cfi_offset 7, -8
 147              		.cfi_offset 14, -4
 148 002e 00AF     		add	r7, sp, #0
 149              		.cfi_def_cfa_register 7
  98:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** //    init_app();
  99:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** //    while(true)
 100:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** //    {
 101:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //       *(GPIO_ODR_HIGH) = 0x0000;
 102:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //       delay(DELAY_COUNT);
 103:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //       *(GPIO_ODR_HIGH) = 0x0000;
 104:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //       delay(DELAY_COUNT);
 105:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c ****  //   }
 106:/Users/aleksandarbabunovic/MaskinorienteradProgrammering/Lab2/mop/systick_irc/startup.c **** }
 150              		.loc 1 106 0
 151 0030 C046     		nop
 152 0032 BD46     		mov	sp, r7
 153              		@ sp needed
 154 0034 80BD     		pop	{r7, pc}
 155              		.cfi_endproc
 156              	.LFE4:
 158              	.Letext0:
