/*
 * 	startup.c
 *
 */
#include <Windows.h>


unsigned char* display = (unsigned char*)0x40020C00; //GPIO D 0-7
unsigned char* keyboard = (unsigned char*)0x40020C01; //GPIO D 8-15

char keyValues[4][4] = {{'1','2','3','A'},{'4','5','6','B'},{'7','8','9','C'},{'*','0','#','D'}};

void app_init(void);
unsigned char keyb(void);
void out7seg(unsigned char c);
void activateRow(int i);
int readColumn(void);

 
void startup(void) __attribute__((naked)) __attribute__((section (".start_section")) );

void startup ( void )
{
asm volatile(
	" LDR R0,=0x2001C000\n"		/* set stack */
	" MOV SP,R0\n"
	" BL main\n"				/* call main */
	".L1: B .L1\n"				/* never return */
	) ;
}

void main(void)
{
	app_init();
	printf("MAWDHJWIODJAWD");
	while(1){
		//out7seg(keyb());
		unsigned char inp = keyb();
		if(inp != 0xFF){
			*display = 0xFF;
		}else{
			*display = 0x00;
		}
		Sleep(5);
	}
}

void app_init(){
	*keyboard = 0x5555;
	*display = 0x5555;
}

unsigned char keyb(void){
	int i;
	for(i = 0; i < 4; i++){
		activateRow(i);
		unsigned char column = readColumn();
		if(column != -1){
			return keyValues[i][column];
		}
	}
	return 0xFF;
}

void activateRow(int i){
	unsigned char bits;
	switch(i){
		case 0: //Set bits to 00010000
			bits = 0x10;
			break;
		case 1: //Set bits to 00100000
			bits = 0x20;
			break;
		case 2: //Set bits to 01000000
			bits = 0x40;
			break;
		case 3: //Set bits to 10000000
			bits = 0x80;
			break;
	}
	*keyboard = bits;
}

int readColumn(void){
	unsigned char bits = *keyboard;
	int i;
	for(i = 0; i < 4; i++){
		if(bits % 2 == 1){ //If odd
			return i;
		}
		bits = bits>>1;
	}
	return -1;
}
